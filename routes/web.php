<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { # backward historical compatibility
    return redirect('/en/welcome');
});
Route::get('/en/welcome', function () {
    return view('welcome', ['lang'=>'en']);
});
Route::get('/es/welcome', function () {
    return view('es_welcome', ['lang'=>'es']);
});


Route::get('/about', function () { # backward historical compatibility
    return redirect('/en/about');
});
Route::get('/en/about', function () {
    return view('about', ['lang'=>'en']);
});
Route::get('/es/about', function () {
    return view('es_about', ['lang'=>'es']);
});


Route::get('/awareness', function () { # backward historical compatibility
    return redirect('/en/awareness');
});
Route::get('/en/awareness', function () {
    return view('awareness', ['lang'=>'en']);
});
Route::get('/es/awareness', function () {
    return view('es_awareness', ['lang'=>'es']);
});


Route::get('/artworks', function () { # backward historical compatibility
    return redirect('/en/artworks');
});
Route::get('/en/artworks', function () {
    return view('artworks', ['lang'=>'en']);
});
Route::get('/es/artworks', function () {
    return view('es_artworks', ['lang'=>'es']);
});


Route::get('/contact', function () { # backward historical compatibility
    return redirect('/en/contact');
});
Route::get('/en/contact', function () {
    return view('contact', ['lang'=>'en']);
});
Route::get('/es/contact', function () {
    return view('es_contact', ['lang'=>'es']);
});


Route::get('/awareness/dossier/d/mind-reading', function () { # backward historical compatibility
    return redirect('/en/mind_reading_dossier-part-1');
});
Route::get('/en/mind-reading-dossier-part-1', function () {
    return view('mind_reading_dossier_1', ['lang'=>'en']);
});
Route::get('/es/mind-reading-dossier-part-1', function () {
    return view('es_mind_reading_dossier_1', ['lang'=>'es']);
});


Route::get('/en/artworks/metropia-the-main-idea', function () {
    return view('metropia_the_main_idea', ['lang'=>'en']);
});
Route::get('/es/artworks/metropia-the-main-idea', function () {
    return view('es_metropia_the_main_idea', ['lang'=>'es']);
});


Route::get('/en/artworks/metropia-am-i-hearing-things', function () {
    return view('metropia_am_i_hearing_things', ['lang'=>'en']);
});
Route::get('/es/artworks/metropia-am-i-hearing-things', function () {
    return view('es_metropia_am_i_hearing_things', ['lang'=>'es']);
});


Route::get('/en/artworks/metropia-it-is-hard-to-talk-about-it', function () {
    return view('metropia_it_is_hard_to_talk_about_it', ['lang'=>'en']);
});
Route::get('/es/artworks/metropia-it-is-hard-to-talk-about-it', function () {
    return view('es_metropia_it_is_hard_to_talk_about_it', ['lang'=>'es']);
});