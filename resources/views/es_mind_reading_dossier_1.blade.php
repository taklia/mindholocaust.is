<!DOCTYPE html>
<html lang="en">

@include('html_header');

<body>
@include('header');
<!-- es_mind_reading_dossier.blade.php -->

<!-- START Content -->

<!-- Section Header START -->
<div class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-sm-4">
				<h1>conocimiento</h1>
				<p style="color: #BFBFEF ">
					reconocer la situación				</p>
			</div>
			<div class="col-lg-8 col-sm-8 navigation">

								
			</div>
		</div>
	</div>
</div>
<!-- Section Header END -->

<div class="container" 
	style="margin-bottom: 50px">

	<div class="row">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
		
			<article class="dossier" 
				title="The Mind Reading Dossier"
				id="awareness-the-mind-reading-dossier">
				<header>
    				<h3>
					
					Historia Contemporánea de la Identificación del Pensamiento
					<br /><span class='reduxtext'>
					Un dossier sobre la Lectura
					de la mente, Parte I (2006-2015)
					</span>
									</h3>
					<p>
					<span style="font-style: italic;">
					Massimo Opposto
					</span>
					<br />
					<span style="font-size: smaller;">
					Septiembre 2015
					</span>
					<br />
					<a 
						target="_blank"
						href="/pdf/Mind-Reading_Dossier_-_es.pdf">
					Download as PDF
					</a>
					</p>
				</header>
				
				
				<section>
					<article>
						<header>
							<h5 style="margin-top: 36px;">
			    				Indice 
			    			</h5>
						</header>
					</article>
					<section>
						<article>
						<p>
							<ul style="list-style-type: none;">
								<li style="padding-left: 15px;">
									<a href="#abstract">
									Abstract
									</a>
								</li>
								<li style="padding-left: 15px;">
									<a href="#thoughts-identification">
									1. 
					Identificación de pensamientos
													</a> 
								</li>
								<li style="padding-left: 15px;">
									<a href="#brain-atlas">
									2. 
					The Brain Atlas (El Atlas del Cerebro).
				 
									</a>
								</li>
								<li style="padding-left: 15px;">
									<a href="#brain-initiative">
									3. 
					The BRAIN initiative (La iniciativa BRAIN)
				 
									</a>
								</li>
								<li style="padding-left: 15px;">
									<a href="#conclusions">
									
					Conclusiones
				 
									</a>
								</li>
								<li style="padding-left: 15px;">
									<a href="#references">
									
					Referencias
				 
									</a>
								</li>
							</ul>
						</p>
						</article>
					</section>
				</section>
				
				<section 
					title="Abstract" 
					id="abstract">
					<article>
						<header>
							<h5 style="margin-top: 36px; 
								margin-bottom: 24px;">
			    				Abstract 
			    			</h5>
		    			</header>
		    			<section>
			    			<article>
			    				<p>
			    					
					La neurociencia moderna es capaz de leer la mente de las
					personas: el procedimiento es llamado "identificación de
					pensamientos".
							    				</p>
			    				<p>
			    					
					Este proceso implica escáneres y sofisticados algoritmos
					computacionales.
							    				</p>
			    				<p>
			    					
					La investigación se lleva a cabo en centros de investigación
					avanzada tales como universidades y laboratorios corporativos con
					la ayuda de las grandes corporaciones IT, y es principalmente
					financiada por el gobierno y los militares (DARPA sobre todo).
							    				</p>
			    				<p>
			    					
					Para cumplir el objetivo de tener un conocimiento preciso
					acerca del funcionamiento de la mente, la administración de Obama
					ha lanzado una importante iniciativa.
							    				</p>
			    				<p>
			    					
					Los informes oficiales acerca de los problemas éticos que puede
					conllevar esta investigación están negando la existencia de
					problemas relacionados con el proceso de la "identificación de
					pensamientos" así como la posibilidad misma de este proceso.
							    				</p>
			    			</article>
		    			</section>
		    		</article>
		    	</section>
				
				<section 
					title="Thoughts Identification" 
					id="thoughts-identification">
					<article>
						<header>
							<h5 style="margin-top: 36px; margin-bottom: 24px;">
								1.
			    				
					Identificación de pensamientos
				 
			    				<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_thoughts_identification">
										?
									</a>
								</sup>
			    			</h5>
		    			</header>
						<section>
							<article>
								<p>
									1.1.
									
					Desde 2006, la neurociencia contemporánea afirma que es posible
					detectar mentiras y actitudes encubiertas utilizando escáneres
					mentales, como el IRMf.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_1">
										[1]
									</a>
									</sup>
								</p>
							</article>
						</section>
						<section>
							<article>
								<p>
									1.2. 
									
					En febrero de 2007, un equipo de científicos del Instituto Max
					Planck para las Ciencias Cognitivas y del Cerebro, en Alemania,
					"leyó" las intenciones de los participantes a partir de su
					actividad cerebral. Esto fue posible gracias a una nueva
					combinación de imágenes por resonancia magnética funcional y
					sofisticados algoritmos computacionales.
				 
									<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_2">
										[2]
									</a>
									</sup>
								</p>
							</article>
						</section>
						<section>
							<article>
								<p>
									1.3. 
									
					Existen pruebas de que el resultado de una decisión es
					codificado (y por lo tanto puede ser detectado) en la actividad
					cerebral hasta 10 segundos antes de que llegue a ser consciente.
				 
									<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_3">
										[3]
									</a>
									</sup>
								</p>
							</article>
						</section>
						<section>
							<article>
								<p>
									1.4. 
									
					A principios de 2008, ya era posible detectar pensamientos
					relativos a objetos familiares, a través del uso de escáneres
					mentales junto con algoritmos computacionales.
				 
									<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_4">
										[4]
									</a>
									</sup>
									<br />
									1.4.1.
									
					El pensamiento acerca de objetos particulares ha sido
					esquematizado en patrones de activación cerebral específicos por un
					grupo de investigadores de la Carnegie Mellon University, en
					Pittsburgh, Estados Unidos.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_4">
										[4]
									</a>
									</sup>
									<br />
									1.4.2.
									
					Los mismos investigadores aseguran que hay puntos en común en
					los modos en que los cerebros de diferentes personas representan el
					mismo pensamiento.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_4">
										[4]
									</a>
									</sup>
								</p>
							</article>
						</section>
						<section>
							<article>
								<p>
									1.5.
									
					Desde 2009, neurocientíficos de los Estados Unidos están
					catalogando los patrones cerebrales para cotejar frases e
					intenciones.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_5">
										[5]
									</a>
									</sup>
									<br />
									1.5.1.
									
					John-Dylan Haynes, del Instituto Max Planck, explica que "ahora
					comprendemos que cada pensamiento está asociado con un patrón de
					actividad cerebral y es posible programar un ordenador para
					reconocer el patrón asociado a un pensamiento en particular."
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_6">
										[6]
									</a>
									</sup>
								</p>
							</article>
						</section>
						<section>
							<article>
								<p>
									1.6.
									
					Los Laboratorios Intel de Pittsburgh trabajan en colaboración
					con el Grupo de Investigación de Análisis de Imágenes Cerebrales de
					la Carnegie Mellon University para identificar los patrones
					cerebrales del pensamiento a través de la tecnología IRMf.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_7">
										[7]
									</a>
									</sup>
									<br />
									1.6.1.
									
					Los Laboratorios Intel de Pittsburgh forman parte de la
					división de investigación de Intel, que fue creada en el año 2000,
					bajo el liderazgo de David L. Tennenhouse. Tennenhouse pretendía
					modelar su nueva organización de investigación basándose en DARPA,
					donde peviamente había sido director de la Oficina de Tecnología de
					la Información.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_8">
										[8]
									</a>
									</sup>
								</p>
							</article>
						</section>				
						<section>
							<article>
								<p>
									1.7.
									
					En 2010, el presupuesto de DARPA para el año fiscal incluía 4
					milliones de dólares para poner en marcha un programa denominado
					Silent Talk ("Conversación silenciosa"). El objetivo era permitir
					una comunicación de usuario a usuario en el campo de batalla sin
					necesidad de un discurso vocalizado, a través del análisis de
					signos neuronales.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_9">
										[9]
									</a>
									</sup>
									<br />
									1.7.1.
									
					Antes de ser vocalizado, el discurso existe en la mente como
					signos neuronales específicamente verbales.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_10">
										[10]
									</a>
									</sup>
									<br />
									1.7.2.
									
					El objetivo de DARPA era desarrollar una tecnología que
					detectara estas señales "pre-discursivas", que las analizara, y
					transmitiera luego el enunciado al locutor deseado.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_9">
										[9]
									</a>
									</sup>
								</p>
							</article>
						</section>
						<section>
							<article>
								<p>
									1.8
									
					En 2011, gracias al "método de aprendizaje zero-shot",
					utilizando conocimiento semántico extraído de un amplio corpus de
					textos y múltiples fuentes humanas, se ha mostrado que las imágenes
					de la actividad cerebral no son necesarias para cada palabra a fin
					de ser reconocidas: es posible predecir palabras (es decir,
					identificar pensamientos) a partir de las Imágenes por resonancia
					magnética funcional (IRMf) de su actividad neuronal, incluso sin
					necesidad de modelos para dichas palabras.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_11">
										[11]
									</a>
									</sup>
								</p>
							</article>
						</section>
						<section>
							<article>
								<p>
									1.9.
									
					El 22 de septiembre, un grupo de investigadores de la Universidad de Berkelay publicó un artículo sobre "Reconstruir las experiencias visuales de la actividad cerebral evocada por las películas naturales".
					Utilizando imágenes de resonancia magnética funcional (fMRI) y modelos computacionales, los investigadores de UC Berkeley han logrado decodificar y reconstruir las experiencias visuales dinámicas de las personas, en este caso, viendo trailers de películas de Hollywood.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_11_1">
										[12]
									</a>
									</sup>
									<br />
									1.9.1.
									
					Un estudiante del mismo grupo tuvo los mismos resultados exitosos aplicando la misma técnica a la experiencia audio, en enero de 2012, el día 31.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_11_2">
										[13]
									</a>
								</p>
							</article>
						</section>
					</article>
				</section>
					
				<section
					title="Thoughts Identification" 
					id="brain-atlas"><!-- The sub-section (Brain Atlas) START -->
					<article><!-- The sub-article (Brain Atlas) START -->
						<header>
							<h5 style="margin-top: 36px;">
								2.
			    				
					The Brain Atlas (El Atlas del Cerebro).
				 
			    				<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_aibs">
										?
									</a>
								</sup>
			    			</h5>
						</header>
						<section>
							<article>
								<p>
									2.1
									
					En 2003, Paul G. Allen, cofundador de Microsoft en 1975, fundó
					el Allen Institute for Brain Science (AIBS), financiándolo con 41
					millones de dólares (hasta la fecha: 500 millones de dólares)
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_12">
										[14]
									</a>
									</sup>
								</p>
							</article>
						</section>
						<section>
							<article>
								<p>
									2.2
									
					El proyecto inaugural del Allan Institute es el Atlas Allen del
					cerebro del ratón, completado el 26 de septiembre de 2006.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_13">
										[15]
									</a>
									</sup>
									<br />
									2.2.1.
									
					El Atlas Allen es un mapa de expresión génica del cerebro del
					ratón (y del cerebro humano, puesto que las personas y los ratones
					comparten el 90% de los genes que se expresan en las funciones
					cerebrales), que, además de técnicas de escaneo funcional, permite
					a los investigadores relacionar la expresión génica, los tipos de
					célula y las funciones secuenciales en la relación de
					comportamientos (o fenotipos).
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_14">
										[16]
									</a>
									</sup>
								</p>
							</article>
						</section>
						<section>
							<article>
								<p>
									2.3.
									
					El 24 de mayo de 2010, el Instituto Allen anunció que estaba
					expandiendo su Atlas del cerebro del ratón al del humano con el
					lanzamiento del Atlas Allen del cerebro humano.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_15">
										[17]
									</a>
									</sup>
								</p>
							</article>
						</section>
					</article><!-- The sub-article (ATLAS) END -->
				</section><!-- The sub-section (ATLAS) END -->
				<section
					title="Thoughts Identification" 
					id="brain-initiative"><!-- The sub-section (BRAIN) START -->
					<article><!-- The sub-article (BRAIN) START -->
						<header>
							<h5 style="margin-top: 36px;">
								3.
			    				
					The BRAIN initiative (La iniciativa BRAIN)
				 
			    				<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_brain_initiative">
										?
									</a>
								</sup>
			    			</h5>
						</header>
						<section>
							<article>
								<p>
									3.1.
									
					El 2 de abril de 2013, Barack Obama, presidente de Estados
					Unidos, dio a conocer la Iniciativa BRAIN (Brain Research through
					Advancing Innovative Neurotechnologies; en español: Investigación
					del cerebro a través del avance de las neurotecnologías
					innovadoras): una investigación colaborativa, público-privada, con
					la meta de respaldar el desarrollo y aplicación de tecnologías
					innovadoras capaces de crear una comprensión dinámica de la
					actividad cerebral.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_16">
										[18]
									</a>
									</sup>
									<br />
									3.1.1.
									
					En otras palabras, el objetivo es producir el primer mapa de la
					actividad cerebral para explorar cada señal enviada por cada célula
					y monitorizar cómo los datos resultantes transcurren a través de
					redes neuronales y son finalmente traducidos en pensamientos,
					sentimientos y acciones: es decir, tener un modelo computacional
					del cerebro humano.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_17">
										[19]
									</a>
									</sup>
									<br />
									3.1.2.
									
					La iniciativa BRAIN ha sido desarrollada por la Office of
					Science and Technology Policy (OSTP; en español: Oficina de la
					política científica y tecnológica) de la Casa Blanca, con
					inversiones para el año fiscal de 2014 de aproximadamente 100
					millones de dólares: 50 millones de la Defense Advanced Research
					Projects Agency (DARPA; en español: Agencia de Proyectos de
					Investigación Avanzados de Defensa), 40 millones del National
					Institutes of Health (NIH) y 20 millones de la National Science
					Foundation (NSF).
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_18">
										[20]
									</a>
									</sup>
									<br />
									3.1.3.
									
					Los socios del sector privado han establecido también
					importantes compromisos para dar soporte a la Iniciativa BRAIN,
					incluyendo: 50 millones de dólares provenientes del Allen Institute
					for Brain Science y 30 millones de dólares del Howard Hughes
					Medical Institute.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_17">
										[19]
									</a>
									</sup>
									<br />
									3.1.4.
									
					El 30 de septiembre de 2014, la administración Obama anunció
					que dos agencias federales más estaban participando en la
					Iniciativa BRAIN: la Food and Drug Administration (FDA) y la
					Intelligence Advanced Research Projects Activity (IARPA). El
					presupuesto del presidente para el año 2015 proponía doblar la
					inversión Federal en la Iniciativa BRAIN de 100 millones de dólares
					a 200 millones de dólares.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_18">
										[20]
									</a>
									</sup>
								</p>
							</article>
						</section>
						<section>
							<article>
								<p>
									3.2.
									
					En 2015, un grupo de compañías, fundaciones, organizaciones de
					defensa del paciente, universidades e instituciones de
					investigación privada están realizando inversiones y anunciando
					contratos para aunar más de 270 millones de dólares en esfuerzos de
					investigación y desarrollo con los objetivos de la Iniciativa
					BRAIN.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_19">
										[21]
									</a>
									</sup>
								</p>
							</article>
						</section>
						<section>
							<article>
								<p>
									3.3.
									
					El 17 de enero de 2013, se llevó a cabo en el California
					Institute of Technology un encuentro entre tres agencias del
					gobierno (DARPA, NIH y NSF), además de neurocientíficos,
					nanocientíficos y representantes de Google, Microsoft y Qualcomm.
					De acuerdo con el sumario del encuentro, este fue mantenido con el
					fin de determinar si existían servicios computacionales capaces de
					capturar y analizar la vasta cantidad de información que
					conllevaría el proyecto. Los científicos y tecnólogos concluyeron
					que era posible hacerlo.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_20">
										[22]
									</a>
									</sup>
								</p>
							</article>
						</section>
						<section>
							<article>
								<p>
									3.4.
									
					El 4 de abril de 2013, Qualcomm Inc. anunció que tomaba parte
					en la
					Iniciativa BRAIN y reveló que había estado trabajando sin
					hacerlo
					público en las fronteras de la neurociencia desde 2009 en
					la &#8220; Brain
					Corp.&#8221; 
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_21">
										[23]
									</a>
									</sup>
									
					una aventura independiente que Qualcomm ha mantenido
					generalmente oculta.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_22">
										[24]
									</a>
									</sup>
									<br />
									3.4.1.
									
					Fundada en 2009, la Brain Corp. tiene la intención de
					desarrollar sistemas informáticos y softwares radicalmente
					diferentes, basándose en algoritmos que emularan los procesos de la
					"neurona de impulsos" propios del cerebro humano.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_22">
										[24]
									</a>
									</sup>
									<br />
									3.4.2.
									
					En 2010, DARPA proveyó un importe no revelado a la Brain Corp.
					"para diseñar un sistema nervioso artificial para VANTs" (vehículos
					aéreos no tripulados).
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_23">
										[25]
									</a>
									</sup>
									<br />
									3.4.3.
									
					El 13 de febrero de 2012, Todd Hylton se unió a la Brain Corp.
					en cuanto que alto ejecutivo después de haber presentado su
					dimisión en DARPA, donde había trabajado cerca de cinco años en el
					puesto de gestor del programa.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_24">
										[26]
									</a>
									</sup>
								</p>
							</article>
						</section>
						<section>
							<article>
								<p>
									3.5.
									
					El 30 de septiembre de 2014, se anunció que los ingenieros de
					Google estaban ideando herramientas y desarrollando
					infraestructuras para analizar a escala de petabytes conjuntos de
					datos generados por la Iniciativa Brain y la comunidad
					neurocientífica, a fin de comprender mejor las connexiones
					computacionales y la bases neuronal del conocimiento humano. Google
					está trabajando muy estrechamente con el Allen Institute for Brain
					Science con el objeto de desarrollar soluciones computacionales
					expansibles a la comprensión científica del cerebro.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_19">
										[21]
									</a>
									</sup>
								</p>
							</article>
						</section>
						<section>
							<article>
								<p>
									3.6.
									
					El 21 de junio de 2012, en el número 74 de "Neuron", una de las
					más influyentes y reputadas revistas en el ámbito de la
					neurociencia, se señalaron algunas consideraciones éticas acerca de
					la Iniciativa BRAIN (ex. Brain Activity Map Project): entre ellas,
					problemas relativos al "control de la mente".
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_25">
										[27]
									</a>
									</sup>
									<br />
									3.6.1.
									
					El 2 de abril de 2013, la Oficina del Secretario de Premsa
					informó de que DARPA contrataría un amplio conjunto de expertos
					para explorar los problemas éticos, legales y sociales planteados
					por los avances de la neurotecnología.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_16">
										[18]
									</a>
									</sup>
									<br />
									3.6.2.
									
					En marzo de 2015, la Comisión Presidencial para el Estudio de
					Problemas Bioéticos publicó el segundo volumen de la "Respuesta en
					dos partes de la Comisión de Bioética a la petición del presidente
					Obama acerca de la Iniciativa BRAIN".
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_26">
										[28]
									</a>
									</sup>
									<br />
									3.6.2.1
									
					En el informe de la comisión, el problema del "control mental"
					y este concepto mismo, son replanteados en términos de
					"modificadores neuronales" para referirse a un más amplio
					despliegue de mecanismos de cambio del sistema cerebral y nervioso,
					ignorado en su peculiaridad.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_26">
										[28]
									</a>
									</sup>
									<br />
									3.6.2.2
									
					En el informe se afirma que "proteger la privacidad mental es
					una preocupación que, con vistas al futuro, los neurocientíficos y
					los legisladores pueden tener que evaluar a medida que la
					tecnología continúe progresando".
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_26">
										[28]
									</a>
									</sup>
									<br />
									3.6.2.3
									
					El informe de la Comisión niega que los logros de los
					neurocientíficos puedan conducir a la "lectura de la mente". Se
					asegura que "hoy por hoy, y en el futuro predecible, la
					neurociencia no nos permite leer mentes. La tecnología continúa
					siendo extremadamente limitada y no puede revelar los verdaderos
					deseos íntimos, los estados psicológicos o las motivaciones que
					pueden ser designadas con el término de lectura mental".
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_26">
										[28]
									</a>
									</sup>
								</p>
							</article>
						</section>
					</article><!-- The sub-article (BRAIN) END -->
				</section><!-- The sub-section (BRAIN) END -->
				<section
				title="Thoughts Identification" 
					id="conclusions"><!-- The sub-section (Conclusion) START -->
					<article><!-- The sub-article (Conclusion) START -->
					
						<header>
							<h5 style="margin-top: 36px;">
								
					Conclusiones
											</h5>
						</header>
						<section>
							<article>
								<p>
								
					Es absurdo negar los problemas de integridad y
					privacidad causados
					por  
            		<a href="#awareness-the-mind-reading-dossier"
            			 title="the mind reading dossier">
            		los
					logros de la
					neurociencia computacional. </a> 
												</p>
								<p>
								
					El hecho de que para negar este problema se
					recurra precisamente
					al Comité Presidencial de Bio-ética de Estados
					Unidos, ampliamente
					financiado y controlado por DARPA, es
					dramático.
												</p>
								<p>
								
					Los gobiernos mundiales están profundamente influenciados por
					el complejo militar, industrial y científico que está quebrantando
					la integridad de la privacidad de la mente.
												</p>
								<p>
								
					De hecho, este poderoso complejo financiero concibe los asuntos
					internos como una cuestión de conflicto estratégico-militar.
												</p>
								<p>
								
					Este marco político contemporáneo está caracterizado por una
					 &#8220;Guerra
					de cuarta generación&#8221; 
					o Fourth Generation Warfare (4GW)
												<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_4GW">
										?
									</a>
								</sup>
								
					llevada a cabo en el frente interno de los estados, esto es,
					entre los ciudadanos, con el fin de ganar los corazones y las
					mentes de sus propias poblaciones.
												</p>
								<p>
								
					A través de la historia contemporánea la guerra psicológica ha
					llegado a ser altamente sofisticada, dependiendo de los logros de
					la investigación científica –entre ellos, la de la tecnología de
					lectura mental–.
												</p>
								<p>
								
					Este escenario subraya que los partidarios de los derechos
					civiles y de la justicia social deben defender la privacidad del
					pensamiento de ser amenzada del abuso de la tecnología de lectura
					mental.
												</p>
							</article>
						</section>
					</article><!-- The sub-article (Conclusion) END -->
				</section><!-- The sub-section (Conclusion) END -->
				
				
				<footer 
					id="references">
					<h5
						style="margin-top: 36px;">
					References
					</h5>
    				
    				<!-- Reference #0 START-->
    					<div 
    						style="display: none;"
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_thoughts_identification">
    						<div data-tag="HTMLPopoverTitle">
    							Thoughts Identification
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Thought identification refers to the 
	    							empirically verified use of technology to, 
	    							in some sense, read people's minds. 
	    							Advances in research have made this 
	    							possible by using human neuroimaging 
	    							to decode a person's conscious experience 
	    							based on non-invasive measurements of an 
	    							individual's brain activity.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
	    							From Wikipedia, the free encyclopedia
    							</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="https://en.wikipedia.org/wiki/Thought_identification"
										title='Thought identification'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #0 END-->
    				<!-- Reference #1 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_1">
    						<div
    							data-tag="HTMLPopoverTitle">
    							Reference #1
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
	    							Prof. Dr. John-Dylan Haynes <br />
	    							(Max Planck Institute for Cognitive and Brain Sciences)
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Decoding mental states from brain 
	    							activity in humans.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
	    							in Nature Reviews Neuroscience #7, <br />
	    							pag. 523-534,<br />
	    							July 2006.
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/John-Dylan_Haynes_-_Decoding_mental_states_from_brain_activity_in_humans.pdf"
										title='Decoding mental states from brain activity in humans'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://www.nature.com/nrn/journal/v7/n7/abs/nrn1931.html"
										title='Decoding mental states from brain activity in humans'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #1 END -->
    				<!-- Reference #2 START -->
    					<div 
    						
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_2">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #2
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
	    							Prof. Dr. John-Dylan Haynes <br />
	    							(Max Planck Institute for Cognitive and Brain Sciences)
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Revealing secret intentions in 
	    							the brain.
									Scientists decode concealed intentions 
									from human brain activity.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
	    							in Max Planck Institute 
	    							for Human Cognitive and Brain Sciences
	    							website. <br />
	    							February 08, 2007
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/John-Dylan_Haynes_-_Research_news_-_2007_-_Revealing_Secret_Intention.pdf"
										title='Revealing secret intentions in the brain.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://www.mpg.de/550068/pressRelease20070206"
										title='Revealing secret intentions in the brain.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #2 END -->
    				<!-- Reference #3 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_3">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #3
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
	    							Chun Siong Soon, Marcel Brass, 
	    							Hans-Jochen Heinze, John-Dylan Haynes.
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Unconscious determinants of free 
	    							decisions in the human brain.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
	    							in Nature Neuroscience. <br />
	    							April 13, 2008
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/John-Dylan_Haynes_and_others_-_NatureNeuroScience_-_Unconscious_determinants_of_free_decisions_in_the_human_brain.pdf"
										title='Unconscious determinants of free decisions in the human brain.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://www.nature.com/neuro/journal/v11/n5/full/nn.2112.html"
										title='Unconscious determinants of free decisions in the human brain.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #3 END -->
    				<!-- Reference #4 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_4">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #4
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
	    							Carnegie Mellon University Press.
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Carnegie Mellon Study Identifies 
	    							Where Thoughts Of Familiar Objects 
	    							Occur Inside the Human Brain.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
	    							in Carnegie Mellon University website. <br />
	    							Jannuary 3, 2008
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Carnegie_Mellon_Study_Identifies_Where_Thoughts_Of_Familiar_Object_Occur_Inside_the_Human_Brain.pdf"
										title='Carnegie Mellon Study Identifies Where Thoughts Of Familiar Objects Occur Inside the Human Brain.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="https://www.cmu.edu/news/archive/2008/January/jan3_justmitchell.shtml"
										title='Carnegie Mellon Study Identifies Where Thoughts Of Familiar Objects Occur Inside the Human Brain.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #4 END -->
    				<!-- Reference #5 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_5">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #5
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
	    							Michael_Johnson
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Watch what you think.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								International Herald Tribune.<br />
	    							march 3, 2009. <br />
	    							hosted at the Center for Cognitive Brain Imaging of
	    							the Carnegie Mellon University
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Michael_Johnson_-_Watch_what_you_think_-_InternationalHeraldTribune.pdf"
										title='Watch what you think.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://www.ccbi.cmu.edu/news/InternationalHeraldTribune.pdf"
										title='Watch what you think.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #4 END -->
    				<!-- Reference #6 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_6">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #6
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
	    							John-Dylan Haynes quoted in
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Watch what you think.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
	    							by Sharon Begley on Newsweek, <br />
	    							Jennuary 12, 2008. <br />
	    							(Also in John-Dylan Haynes. Brain reading. 
	    							Oxford University Press. 2012)
	    							
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/John-Dylan_Haynes_-_Mind_Reading_Is_Now_Possible_-_Newsweek.pdf"
										title='Begley: Mind Reading Is Now Possible.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://www.newsweek.com/begley-mind-reading-now-possible-86567"
										title='Begley: Mind Reading Is Now Possible.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #4 END -->
					<!-- Reference #7 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_7">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #7
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
	    							Marcel Just and Tom Mitchell
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Thought Reading Demonstration.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
	    							in Carnegie Mellon University youtube channel. <br />
	    							Jennuary 8, 2009
	    							
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/video/Marcel_Just_and_Tom_Mitchell_-_Thought_Reading_Demonstration_-_CMU.webm"
										title='Thought Reading Demonstration.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://www.youtube.com/watch?v=JVLu5_hvr8s"
										title='Watch what you think.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #7 END -->
					<!-- Reference #8 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_8">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #8
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
	    							
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Intel Research Lablets.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								from Wikipedia, the free encyclopedia
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Intel_Research_Lablets_-_Wikipedia_the_free_encyclopedia.webm"
										title='Thought Reading Demonstration.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="https://en.wikipedia.org/wiki/Intel_Research_Lablets"
										title='Watch what you think.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #8 END -->
    				<!-- Reference #9 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_9">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #9
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
	    							The Defense Advanced Research Projects Agency (DARPA).
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Fiscal Year 2010 Budget Estimates.&#8221;
	    							
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								pag. 12, <br />
    								May 2009. 
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Department_of_Defense_Fiscal_Year_2010_Budget_Estimates.pdf"
										title='Thought Reading Demonstration.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://www.darpa.mil/attachments/%282G7%29%20Global%20Nav%20-%20About%20Us%20-%20Budget%20-%20Budget%20Entries%20-%20FY2010%20%28Approved%29.pdf"
										title='Watch what you think.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #9 END -->
    				<!-- Reference #10 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_10">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #10
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
	    							Schmidek and Sweet.
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Operative Neurosurgical Techniques.&#8221;
	    							
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								pag. 1372, <br />
    								Ed. Elsevier Inc. <br />
    								2012 
    							</div>
    							
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Schmidek_Sweet_-_Operative_Neurosurgical_Techniques.pdf"
										title='Thought Reading Demonstration.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="https://books.google.es/books?id=4sKQSY-zdvgC&pg=PA1372&lpg=PA1372&dq=speech+exists+as+word-specific+neural+signals+in+the+mind&source=bl&ots=eXw21MDrlX&sig=-NvyQAhD8sj-1WwIe232176Hm0w&hl=en&sa=X&ved=0CCgQ6AEwAWoVChMIl9Td_OWUyAIVx1AUCh36vQRn#v=onepage&q&f=false"
										title='Operative Neurosurgical Techniques.'
										target="_blank">
										external link
									</a>
								</div>
								
    						</div>
    					</div>
    				<!-- Reference #10 END -->
    				<!-- Reference #11 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_11">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #11
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
	    							Mark M. Palatucci.
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Thought Recognition: Predicting and Decoding.
	    							Brain Activity Using the Zero-Shot Learning Model.&#8221;
	    							
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								pag. iv, <br />
    								Carnegie Mellon University <br />
    								Dissertations, paper 65.  <br />
    								March 25, 2011.
    							</div>
    							
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Thought_Recognition_Predicting_and_Decoding_Brain_Activity_Using_the_zero_shot_learning_model.pdf"
										title='Thought Recognition: Predicting and Decoding. Brain Activity Using the Zero-Shot Learning Model.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://repository.cmu.edu/cgi/viewcontent.cgi?article=1058&context=dissertations"
										title='Thought Recognition: Predicting and Decoding. Brain Activity Using the Zero-Shot Learning Model.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #11 END -->
    				<!-- Reference #11_1 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_11_1">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #12
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
	    							Jack L. Gallant and al.
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Reconstructing visual experiences from brain activity evoked by natural movies.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								UC Berkeley<br />
    								US National Library of Medicine National Institutes of Health<br />
    								September 22, 2011.
    							</div>
    							
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Jack_L._Gallant_-_Reconstructing_visual_experiences_from_brain_activity_evoked_by_natural_movies.pdf"
										title='Jack L. Gallan: Reconstructing visual experiences from brain activity evoked by natural movies'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3326357/pdf/nihms319652.pdf"
										title='Jack L. Gallan: Reconstructing visual experiences from brain activity evoked by natural movies.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #11_1 END -->
    				<!-- Reference #11_2 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_11_2">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #13
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
	    							Brian N. Pasley and al.
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Reconstructing Speech from Human Auditory Cortex.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								UC Berkeley<br />
    								PLOS Biology<br />
    								January the 31st, 2012.
    							</div>
    							
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Brian_N._Pasley_-_Reconstructing_Speech_from_Human_Auditory_Cortex.pdf"
										title='Brian N. Pasley: Reconstructing Speech from Human Auditory Cortex'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://journals.plos.org/plosbiology/article/file?id=10.1371/journal.pbio.1001251&type=printable"
										title='Brian N. Pasley: Reconstructing Speech from Human Auditory Cortex.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #11_1 END -->
    				<!-- Thoughts Identification END -->
    				<!-- Reference The Brain Atlas START -->
    					<div 
    						style="display: none;"
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_aibs">
    						<div data-tag="HTMLPopoverTitle">
    							The Brain Atlas
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;The Allen Mouse and Human Brain 
	    							Atlases are projects within the Allen 
	    							Institute for Brain Science which seek 
	    							to combine genomics with neuroanatomy by 
	    							creating gene expression maps for the 
	    							mouse and human brain.&#8221;
	    							
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								From Wikipedia, the free encyclopedia
    							</div>
    							<!-- 
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Thought_Recognition_Predicting_and_Decoding_Brain_Activity_Using_the_zero_shot_learning_model.pdf"
										title='Thought Recognition: Predicting and Decoding. Brain Activity Using the Zero-Shot Learning Model.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
							 	-->
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="https://en.wikipedia.org/wiki/Allen_Brain_Atlas"
										title='Allen Brain Atlas'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference The Brain Atlas END -->
    				<!-- Reference #12 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_12">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #14
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Allen Insitute for Brain Science, Founders&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								Allen Institute for Brain Science website.
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Allen_Institute_for_Brain_Science_-_Founders.pdf"
										title='Allen Insitute for Brain Science, Founders.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://www.alleninstitute.org/our-institute/founders/"
										title='Allen Insitute for Brain Science, Founders.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #12 END -->
					<!-- Reference #13 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_13">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #15
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Allen Institute for Brain Science 
	    							completes brain atlas&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								Allen Institute for Brain Science website. <br />
    								September 26, 2006.
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Allen_Institute_for_Brain_Science_-_The_Allen_Institute_for_Brain_Science_Completes_the_Brain_Atlas.pdf"
										title='Allen Institute for Brain Science completes brain atlas.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://www.alleninstitute.org/news-events/press/press-release/allen-institute-brain-science-completes-brain-atlas"
										title='Allen Institute for Brain Science completes brain atlas.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #13 END -->
    				<!-- Reference #14 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_14">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #16
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Allen Brain Atlas, Contributions to neuroscience.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								From Wikipedia, the free encyclopedia.
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Allen_Brain_Atlas_-_Wikipedia_the_free_encyclopedia.pdf"
										title='Allen Brain Atlas, Contributions to neuroscience.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="https://en.wikipedia.org/wiki/Allen_Brain_Atlas#Contributions_to_neuroscience"
										title='Allen Brain Atlas, Contributions to neuroscience.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #14 END -->
    				<!-- Reference #15 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_15">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #17
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Allen Institute for Brain Science launches 
	    							Allen Human Brain Atlas with first data set charting genes at 
	    							work in the adult human brain.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								Allen Institute for Brain Science website. <br />
    								May 24, 2010
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Allen_Institute_for_Brain_Science_-_Allen_Institute_for_Brain_Science_launches_Allen_Human_Brain_Atlas_with_first_data_set_charting_genes_at_work_in_the_adult_human_brain.pdf"
										title='Allen Institute for Brain Science launches Allen Human Brain Atlas.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://alleninstitute.org/news-events/press/press-release/allen-institute-brain-science-launches-allen-human-brain-atlas-first-data-set-charting-genes-work-ad"
										title='Allen Institute for Brain Science launches Allen Human Brain Atlas.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #15 END -->
    				<!-- Reference Brain Initiative START -->
    					<div 
    						style="display: none;"
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_brain_initiative">
    						<div data-tag="HTMLPopoverTitle">
    							The Brain Initiative
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;The White House BRAIN Initiative 
	    							(Brain Research through Advancing Innovative 
	    							Neurotechnologies), is a collaborative, 
	    							public-private research initiative announced 
	    							by the Obama administration on April 2, 2013,
	    							with the goal of supporting the development 
	    							and application of innovative technologies 
	    							that can create a dynamic understanding of 
	    							brain function.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								From Wikipedia, the free encyclopedia.
    							</div>
    							<!--  
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Allen_Institute_for_Brain_Science_-_Allen_Institute_for_Brain_Science_launches_Allen_Human_Brain_Atlas_with_first_data_set_charting_genes_at_work_in_the_adult_human_brain.pdf"
										title='Allen Institute for Brain Science launches Allen Human Brain Atlas.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								-->
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://alleninstitute.org/news-events/press/press-release/allen-institute-brain-science-launches-allen-human-brain-atlas-first-data-set-charting-genes-work-ad"
										title='Allen Institute for Brain Science launches Allen Human Brain Atlas.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference Brain Initiative END -->
    				<!-- Reference #16 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_16">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #18
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							The White House, Office of the Press Secretary.
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Fact Sheet: BRAIN Initiative.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								The White House website <br />
    								April 02, 2013
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/The_White_House_Office_of_the_Press_Secretary_-_Fact_Sheet_BRAIN_Initiative.pdf"
										title='Fact Sheet: BRAIN Initiative.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="https://www.whitehouse.gov/the-press-office/2013/04/02/fact-sheet-brain-initiative"
										title='Fact Sheet: BRAIN Initiative.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #16 END -->
    				<!-- Reference #17 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_17">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #19
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							Maia Szalavitz
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Brain Map: President Obama Proposes 
	    							First Detailed Guide of Human Brain Function.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								Time <br />
    								February 19, 2013
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Maia_Szalavitz_-_Brain_Map_President_Obama_Proposes_First_Detailed_Guide_of_Human_Brain_Function.pdf"
										title='Brain Map: President Obama Proposes First Detailed Guide of Human Brain Function.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://healthland.time.com/2013/02/19/brain-map-president-obama-proposes-first-detailed-guide-of-human-brain-function/"
										title='Brain Map: President Obama Proposes First Detailed Guide of Human Brain Function.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #17 END -->
    				<!-- Reference #18 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_18">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #20
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							Francis Collins (Director of the National Institutes of Health) 
    							and Arati Prabhakar (Director of Defense Advanced Research Projects Agency).
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;BRAIN Initiative Challenges Researchers 
	    							to Unlock Mysteries of Human Mind.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								White House Blog <br />
    								April 2, 2013
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Francis_Collins_and_Arati_Prabhakar_-_BRAIN_Initiative_Challenges_Researchers_to_Unlock_Mysteries_of_Human_Mind.pdf"
										title='BRAIN Initiative Challenges Researchers to Unlock Mysteries of Human Mind.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="https://www.whitehouse.gov/blog/2013/04/02/brain-initiative-challenges-researchers-unlock-mysteries-human-mind"
										title='BRAIN Initiative Challenges Researchers to Unlock Mysteries of Human Mind.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #18 END -->
    				<!-- Reference #19 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_19">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #21
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Fact Sheet: Over $300 Million in 
	    							Support of the President’s BRAIN Initiative.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								The White House website <br />
    								April 2, 2013
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Fact_Sheet_Over_300_Million_in_Support_of_the_President_s_BRAIN_Initiative.pdf"
										title='Fact Sheet: Over $300 Million in Support of the President’s BRAIN Initiative.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="https://www.whitehouse.gov/sites/default/files/microsites/ostp/brain_fact_sheet_9_30_2014_final.pdf"
										title='Fact Sheet: Over $300 Million in Support of the President’s BRAIN Initiative.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #19 END -->
    				<!-- Reference #20 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_20">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #22
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							John Markoff
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Obama Seeking to Boost Study of Human Brain.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								New York Times <br />
    								February 17, 2013
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/John_Markoff_-_Obama_Seeking_to_Boost_Study_of_Human_Brain.pdf"
										title='Obama Seeking to Boost Study of Human Brain.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://www.nytimes.com/2013/02/18/science/project-seeks-to-build-map-of-human-brain.html"
										title='Obama Seeking to Boost Study of Human Brain.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #20 END -->
    				<!-- Reference #21 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_21">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #23
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							Michael Copeland (Qualcomm Sr. Manager, Marketing)
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;The Government’s New BRAIN Initiative: 
	    							The Next Manhattan Project?&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								Qualcomm website <br />
    								April 22, 2013.
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/The_Government_s_New_BRAIN_Initiative_The_Next_Manhattan_Project.pdf"
										title='Obama Seeking to Boost Study of Human Brain.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="https://www.qualcomm.com/news/spark/2013/04/22/governments-new-brain-initiative-next-manhattan-project"
										title='Obama Seeking to Boost Study of Human Brain.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #21 END -->
    				<!-- Reference #22 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_22">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #24
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							Michael Copeland (Qualcomm Sr. Manager, Marketing)
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Qualcomm Started Computing Like a Neuron Years Ago&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								Qualcomm website <br />
    								March 4, 2013
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Bruce_V_Bigelow_-_With_Brain_Corp._Qualcomm_Started_Computing_Like_a_Neuron_Years_Ago.pdf"
										title='Qualcomm Started Computing Like a Neuron Years Ago'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://www.xconomy.com/san-diego/2013/04/03/qualcomm-started-its-brain-initiative-years-before-president-obama/"
										title='Qualcomm Started Computing Like a Neuron Years Ago'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #22 END -->
    				<!-- Reference #23 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_23">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #25
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;DARPA funds Brain Corporation.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								Brain Corporation website <br />
    								November 1, 2010. 
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/DARPA_funds_Brain_Corporation.pdf"
										title='DARPA funds Brain Corporation.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://www.braincorporation.com/darpa-funds-brain-corporation/"
										title='DARPA funds Brain Corporation.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #23 END -->
    				<!-- Reference #24 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_24">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #26
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Dr. Todd Hylton joins Brain Corporation.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								Brain Corporation website <br />
    								February 12, 2013. 
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Dr_Todd_Hylton_joins_Brain_Corporation.pdf"
										title='Dr. Todd Hylton joins Brain Corporation.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://www.braincorporation.com/dr-todd-hylton-joins-brain-corporation/"
										title='Dr. Todd Hylton joins Brain Corporation.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #24 END -->
    				<!-- Reference #25 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_25">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #27
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							A. Paul Alivisatos, Miyoung Chun, George M. Church, 
    							Ralph J. Greenspan, Michael L. Roukes, and Rafael Yuste
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;The Brain Activity Map Project and 
	    							the Challenge of Functional Connectomics.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								Neuron #74, 
    								pag. 973, <br />
    								Cell press. Elsevier Inc. <br />
    								June 21, 2012.
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/The_Brain_Activity_Map_Project_and_Challenge_of_Functional_Connectocomics.pdf"
										title='The Brain Activity Map Project and the Challenge of Functional Connectomics.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://arep.med.harvard.edu/pdf/Alivisatos_BAM_12.pdf"
										title='The Brain Activity Map Project and the Challenge of Functional Connectomics.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #25 END -->
    				<!-- Reference #26 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_26">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #28
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							Presidential Commission for the study of Bioethical Issues.
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Gray Matters: Topics at the Intersection 
	    							of Neuroscience, Ethics, and Society.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								Presidential Commission for the study of 
    								Bioethical Issues website. <br />
    								Pag 28. <br />
    								March 2015<br />
    								
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/The_Brain_Activity_Map_Project_and_Challenge_of_Functional_Connectocomics"
										title='Gray Matters: Topics at the Intersection of Neuroscience, Ethics, and Society.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://bioethics.gov/sites/default/files/GrayMatter_V2_508.pdf"
										title='Gray Matters: Topics at the Intersection of Neuroscience, Ethics, and Society.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #26 END -->
    				<!-- Reference #4GW START -->
    					<div 
    						style="display: none;"
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_4GW">
    						<div data-tag="HTMLPopoverTitle">
    							 Fourth Generation Warfare
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Fourth-generation warfare (4GW) 
	    							is conflict characterized by a blurring 
	    							of the lines between war and politics, 
	    							combatants and civilians.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								From Wikipedia, the free encyclopedia
    								
    							</div>
    							<!--
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/The_Brain_Activity_Map_Project_and_Challenge_of_Functional_Connectocomics"
										title='Gray Matters: Topics at the Intersection of Neuroscience, Ethics, and Society.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								-->
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="https://en.wikipedia.org/wiki/Fourth-generation_warfare"
										title='Fourth-generation warfare.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #26 END -->

  				</footer>
			</article>
		
		</div>
		<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12" >
		
			
		
		</div>
	</div>
	
	
</div>      
<!-- END Content -->

<!-- footer START -->
@include('es_footer');
<!-- footer END -->
<!-- html footer START -->
@include('html_footer');
<!-- html footer END -->

    </body>
</html>