<article class="media">
	<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
		<a 
			class="pull-left thumb" 
			href="https://mindholocaust.is.the.nextholocaust.com/news/a-film-about-synthetic-telepathy-and-a-dystopic-world-of-secret-experiments/"> 
			<img 
				style="width: 100%; border-radius: 25px; margin-bottom: 12px;"
				src="https://mindholocaust.is.the.nextholocaust.com/news/wp-content/uploads/2018/06/distortedheader.jpg"
				alt="A film about synthetic telepathy and a dystopic world of secret experiments" 
			/>
		</a>
	</div>
	<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
		<p style="color: #797979; font-style: italic;">
			Article published on 
			<time datetime="June 28, 2018" pubdate="pubdate">
				June 28, 2018
			</time>
		</p>
		<div class="media-body">
			<a 
				href="https://mindholocaust.is.the.nextholocaust.com/news/a-film-about-synthetic-telepathy-and-a-dystopic-world-of-secret-experiments/" class=" p-head">
				A film about synthetic telepathy and a dystopic world of secret experiments
			</a>
			<p>
				<p>A movie about synthetic telepathy is out since June the 22nd, 2018. 
					Produced by Mind’s Eye Pictures and starring John Cusack 
					and Christina Ricci, Distorted is about a capitalistic dystopia 
					that involves subliminal neuro-advertising messages, AI, 
					mental heath treatments and a global conspiracy. In the film, 
					Ricci plays a young bipolar woman who moves into a 
					state-of-the-art […]
				</p>
			</p>
			<p>
				<a href="https://mindholocaust.is.the.nextholocaust.com/news/a-film-about-synthetic-telepathy-and-a-dystopic-world-of-secret-experiments/">
					<i class="fa fa-link"></i>
					Read more</a>
			</p>
		</div>
	</div>
</article>