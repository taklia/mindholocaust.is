<!DOCTYPE html>
<html lang="es">

@include('html_header');

<body>
@include('es_header')
<!-- es_awarness.blade.php -->

<!-- START Content -->

<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-sm-4">
				<h1>
					conocimiento
				</h1>
				<p style="color: #BFBFEF ">
					reconocer la situación
				</p>
			</div>
			<div class="col-lg-8 col-sm-8 navigation">
				<a href="/">MindHolocaust</a> &nbsp; &gt; &nbsp; 
				conocimiento
			</div>
		</div>
	</div>
</div>


<div class="container">

	<!-- Dossier Mind-Reading START -->
	<div class="row" style="margin-bottom: 60px;">
		<div class="col-lg-3 col-md-6 col-sm-6  text-right">
			<img style="width: 263px; margin-bottom: 10px; margin-top: 60px;"
				alt="" src="/images/dossier.png">
		</div>
		<div class="col-lg-9 col-md-6 col-sm-6">
			<h3>
					Historia de la Identificación del Pensamiento
					<br /><span class='reduxtext'>
					un dossier sobre la Lectura
					de la mente, Parte I (2006-2015)
					</span>
			</h3>
			<p class="f-text">
					MindHolocaust se enorgullece de presentar y publicar el dossier
					sobre la "lectura de la mente": una corta pero exhaustiva historia cronológica
					sobre la tecnología de lectura de la mente.
			</p>
			<p class="f-text">
					El dossier da respuestas a la pregunta "¿Existe la
					tecnología
					de
					lectura mental?" con una precisión extrema.
			</p>
			<p class="f-text">
					Cada enunciado del dossier está bien documentado y bien
					probado.
			</p>	
			<p class="f-text">
					Toda la documentación se proporciona con el dossier
					como enlaces
					web y documentos PDF.
			</p>
			<p class="f-text">
					Todos los documentos del dossier son documentos oficiales:
					publicaciones científicas, comunicados de prensa publicados por
					las
					universidades o por las instituciones
					governativas o agencias
					federales.
			</p>
			<a class="btn btn-purchase"
				href="/es/awareness/dossier/d/mind-reading">
					Leer y disfrutar del dossier
			</a>
		</div>
	</div>

	<!-- Dossier Mind-Reading END -->
	<div class="row" style="margin-bottom: 60px;">
		<div class="col-lg-3 col-md-6 col-sm-6  text-right">
			<a href="/videos/watch/v/Science_Bytes_-_Decoding_Our_Senses">
					<img style="width: 263px; margin-bottom: 10px; margin-top: 60px;"
					alt="" src="/images/thumbnail-Science_Bytes_-_Decoding_Our_Senses.png" />
			</a>
		</div>
		<div class="col-lg-9 col-md-6 col-sm-6">
			<h3>
					Descodificando nuestros sentidos
			</h3>
			<p>
					Nuestros sentidos, nuestras experiencias audiovisuales, pueden ser 
					decodificados y reconstruidos usando fMRI y los algoritmos 
					informáticos científicos demandados a finales de 2011 a partir 
					de 2012.
			</p>
			<p>
					Un corto documental sobre la descondificaciòn experiencias 
					audiovisuales producido en la serie "Bytes de la Ciencia", 
					financiado por Alfred P. Sloan Foundation.
			</p>
			<a class="btn btn-purchase"
				href="/videos/watch/v/Science_Bytes_-_Decoding_Our_Senses">
					Míra el video
			</a>
		</div>
	</div>

	<div class="row" style="margin-bottom: 60px;">
		<div class="col-lg-3 col-md-6 col-sm-6  text-right">
			<a href="/videos/watch/v/CBS_Reading_Your_Mind">
					<img style="width: 263px; margin-bottom: 10px; margin-top: 60px;"
					alt="" src="/images/thumbnail-CBS_Reading_Your_Mind.png" />
			</a>
		</div>
		<div class="col-lg-9 col-md-6 col-sm-6">
			<h3>
					La
					lectura de tu mente
			</h3>
			<p>
					el documental de la CBS acerca de
					la técnica de la neurociencia
					llamada
					&#8220;identificación
					del pensamiento &#8221;
			</p>
			<p>
					La neurociencia ha aprendido mucho acerca de la
					actividad del
					cerebro y su relación con ciertos pensamientos. 
					 <br /> 
					Como
					informa Lesley Stahl, que ahora puede ser posible, en un nivel
					básico, de leer la mente de una persona.
			</p>
			<a class="btn btn-purchase"
					href="/videos/watch/v/CBS_Reading_Your_Mind">
					Míra el video
			</a>
		</div>
	</div>

	<div class="row" style="margin-bottom: 60px;">
		<div class="col-lg-3 col-md-6 col-sm-6  text-right">
			<a href="/videos/watch/v/John-Dylan_Haynes_-_Mind_reading_with_brain_scanners">
				<img style="width: 263px; margin-bottom: 10px; margin-top: 60px;"
					alt="" src="/images/thumbnail-John-Dylan_Haynes_-_Mind_reading_with_brain_scanners.png" />
			</a>
		</div>
		<div class="col-lg-9 col-md-6 col-sm-6">
			<h3>
					John-Dylan Haynes: Lectura de la mente con escáneres
					cerebrales
			</h3>
			<p>
					Un TEDx Talk sobre Mind Reading
			</p>
			<p>
					La investigación de John-Dylan Haynes se centra en los
					mecanismos neurales que subyacen a los procesos cognitivos humanos.
					Sus intereses especiales son los fundamentos técnicos y éticos de
					decodificación del estado mental, así como la neurociencia de la
					conciencia, las intenciones y el libre albedrío.
			</p>
			<a class="btn btn-purchase"
					href="/videos/watch/v/John-Dylan_Haynes_-_Mind_reading_with_brain_scanners">
					Míra el video
			</a>
		</div>
	</div>

</div>
<!-- END Content -->

<!-- Footer START -->
@include('es_footer');
@include('html_footer');
<!-- Footer END -->
    </body>
</html>