<!DOCTYPE html>
<html lang="es">

@include('html_header');

<body>
@include('es_header')
<!-- es_artworks.blade.php -->

<!-- START Content -->

<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-sm-4">
				<h1>
					Obras de arte
				</h1>
				<p style="color: #BFBFEF ">
					Un imaginario sobre la tecnología telepática.
				</p>
			</div>
			<div class="col-lg-8 col-sm-8 navigation">
				<a href="/es/welcome">MindHolocaust</a> &nbsp; &gt; &nbsp; 
					obras de arte			
			</div>
		</div>
	</div>
</div>


<div class="container">

	
	<div class="row" style="margin-bottom: 60px;">
		<div class="col-lg-3 col-md-6 col-sm-6  text-right">
			<a href="/artworks/video/v/metropia_-_the_main_idea">
		
			<img style="width: 263px; margin-bottom: 10px; margin-top: 60px;"
				alt="" src="/images/thumbnail-metropia-video-01-review.jpg" />
				
			</a>
		</div>
		<div class="col-lg-9 col-md-6 col-sm-6">
			<h3>Metropia: La idea principal.</h3>
			<p>
					Metropia es una película sobre una distopía impulsada por
					las empresas en un mundo en el cual las corporaciones
					pueden leer
					los pensamientos de la gente y entregar
					información a la mente de la
					gente...
				</p>
			<p>
					Este videoclip es un extracto a partir de una reseña
					sobre Metropia: aquí se explica la idea principal de la película.
				</p>

			<a class="btn btn-purchase"
				href="/artworks/video/v/metropia_-_the_main_idea">
				
					Míralo
								</a>
		</div>
	</div>
	
	
	<div class="row" style="margin-bottom: 60px;">
		<div class="col-lg-3 col-md-6 col-sm-6  text-right">
			<a href="/artworks/video/v/metropia_-_am_i_hearing_things">
		
			<img style="width: 263px; margin-bottom: 10px; margin-top: 60px;"
				alt="" src="/images/thumbnail-metropia-video-02-inneryou.jpg" />
				
			</a>
		</div>
		<div class="col-lg-9 col-md-6 col-sm-6">
			<h3>Metropia: ¿Estoy escuchando cosas?</h3>
			<p>
					Metropia es una película sobre una distopía impulsada por
					las empresas en un mundo en el cual las corporaciones
					pueden leer
					los pensamientos de la gente y entregar
					información a la mente de la
					gente...
				</p>
			<p>
					Este videoclip trata acerca de un hombre que está
					escuchando la voz de un extraño dentro de
					su propia cabeza. El
					extraño escucha los
					pensamientos de la voz interior del hombre...
				</p>

			<a class="btn btn-purchase"
				href="/artworks/video/v/metropia_-_am_i_hearing_things">
				
					Míralo
								</a>
		</div>
	</div>
	
	
	<div class="row" style="margin-bottom: 60px;">
		<div class="col-lg-3 col-md-6 col-sm-6  text-right">
			<a href="/artworks/video/v/metropia_-_hard_to_talk_about">
		
			<img style="width: 263px; margin-bottom: 10px; margin-top: 60px;"
				alt="" src="/images/thumbnail-metropia-video-02-hardtotalk.jpg" />
				
			</a>
		</div>
		<div class="col-lg-9 col-md-6 col-sm-6">
			<h3>Metropia: Es difícil hablar de esto.</h3>
			<p>
					Metropia es una película sobre una distopía impulsada por
					las empresas en un mundo en el cual las corporaciones
					pueden leer
					los pensamientos de la gente y entregar
					información a la mente de la
					gente...
				</p>
			<p>
					Este videoclip muestra cuán violento puede ser hablar
					de este fenómeno.
				</p>

			<a class="btn btn-purchase"
				href="/artworks/video/v/metropia_-_hard_to_talk_about">
				
					Míralo
								</a>
		</div>
	</div>
	
		
</div>

<!-- END Content -->

<!-- Footer START -->
@include('es_footer');
@include('html_footer');
<!-- Footer END -->

    </body>
</html>