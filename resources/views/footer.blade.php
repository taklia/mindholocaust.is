<!--footer start-->
<footer class="footer">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-sm-3">
				<h1>
					contact info
				</h1>
				<address>
 					<!-- <p>Matteo Mazzanti</p> -->
 					<p>NextHolocaust</p>
					<p>
					Address
				:No. 9, Mariedalsvagen, #1302</p>
					<p>21754, Malmö, Sweden </p>
					<p>
					Map
				: <a href="https://www.google.se/maps/place/Mariedalsv%C3%A4gen+9,+217+54+Malm%C3%B6/@55.6017824,12.9800826,17z/data=!3m1!4b1!4m5!3m4!1s0x4653a6abb4e10b91:0x9e71085088afc7f8!8m2!3d55.6017794!4d12.9822766"
					 	target="_blank">
						Open Google Map</a>
					</p>
					<p>
					Phone
				: +46 72681516</p>
					<p>
						Email : <a href="/contact-form">info@nextholocaust.com</a>
					</p>
				</address>
			</div>
			<div class="col-lg-5 col-sm-5">
				<h1>
					privacy policy
				</h1>
				<div class="tweet-box">
					<span style="font-weight: bold;">
					
					We do care about your online privacy too:
									</span>
					<br />
					<ul>
						<li>
							
					We don't allow 
				
				<a href="http://en.wikipedia.org/wiki/Criticism_of_Google#Tracking"
					target="_blank"
					title="Criticism of Google"> 
				
					Google tracking your navigation
				
				</a> 
				
					using Google Analytics nor with Google's
					other software.
										</li>
						<li>
							
					We don't allow 
            	
				<a href="http://en.wikipedia.org/wiki/Criticism_of_Facebook#Tracking_cookies"
					target="_blank"
					title="Criticism of Facebook">
				
					facebook/Google+ (and similar)
				
				</a> 
				
					to track your navigation
					using their tracking icons
					on our pages.
										</li>
						<li>
							
					In general, 
				 <span style="text-decoration: underline;"> 
					we don't give data about any of
					our users to any third party</span>.
										</li>
						<li>
							
					This web-site run on a Free Software  &amp; 
					civil
					rights enthusiasts hosting service.
										</li>
					</ul>
					<p>
						
					Read more about our 
				 <a href="/privacy_policy"> 
					privacy policy here</a>.
									</p>
				</div>
			</div>
			<div class="col-lg-3 col-sm-3 col-lg-offset-1 copyrights">
				<h1>2021 - MindHolocaust</h1>
				
					
        <div style="margin: 0px 0px 10px 4px;">
          <a 
            rel="license" 
            href="http://creativecommons.org/licenses/by-sa/4.0/"
            target="_blank">
            <img 
              alt="Creative Commons License" 
              style="border-width:0" 
              src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" />
          </a>
        </div>

        <p>	
              This work		
          is licensed under a 
          <a 
            rel="license" 
            href="http://creativecommons.org/licenses/by-sa/4.0/"
            target="_blank">
            Creative Commons Attribution  
              -Share-Alike			4.0
              International</a>.
          <br />

        </p>
	
	
        <p>
          
            Permissions beyond the scope of this
                license may be available at 
          <a 
            href="http://nextholocaust.com/contact" 
            target="_blank">
            http://nextholocaust.com/contact		</a>.
            
        </p>
	
						
        <div style="margin-top:20px;">
          
        produced by
                  <a href="http://taklia.com"
            target="_blank"
            style="font-weight: bold;">
          taklia.com
          </a>
        </div>
				
			</div>
		</div>
	</div>
</footer>
<!--footer end--> 
