<!DOCTYPE html>
<html lang="en">

@include('html_header');

<body>
@include('header');
<!-- contact.blade.php -->

<!-- START Content -->

<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-sm-4">
				<h1>
					contact
				</h1>
				<p style="color: #BFBFEF">
					Feel free to contact us.
				</p>
			</div>
			<div class="col-lg-8 col-sm-8 navigation">
				<a href="/">MindHolocaust</a> &nbsp; &gt; &nbsp; 
					contact
			</div>
		</div>
	</div>
</div>


<div class="container">
	<div class="row">
		<div class="col-lg-7 col-sm-7 address">
			<h4>
					Send a Message
				</h4>
			<div class="contact-form">
				<form 
					role="form" 
					name="contactForm"
					action="/contact"
					method="post">
					<div class="form-group">
						<label for="name">
						
					Name
				</label> 
						<input type="text"
							class="form-control" 
							name="formContactName" 
							placeholder="Winston Smith" />
						<div class="formError-text"></div>
					</div>
					<div class="form-group">
						<label for="email">
						
					Email
				</label> 
						<input type="text"
							class="form-control" 
							name="formContactEmail" 
							placeholder="email@domain.org" />
						<div class="formError-text"></div>
					</div>
					<div class="form-group">
						<label for="phone">
						
					Phone
										</label> 
						<input type="text"
							class="form-control" 
							name="formContactPhone" 
							placeholder="0037 45658878451" />
						<div class="formError-text"></div>
					</div>
					<div class="form-group">
						<label for="phone">
						
					Message
										</label>
						<textarea 
							class="form-control" 
							name="formContactMessage" 
							rows="5" 
							placeholder="Your message here."></textarea>
						<div class="formError-text"></div>
					</div>
					<button type="submit" class="btn btn-danger">
					
					Send
									</button>
				</form>

			</div>
		</div>
		
		<div class="col-lg-5 col-md-5 col-sm-5 address">

			<div class="f-box-static"
				style="padding-bottom: 20px; 
					margin-bottom: 40px; 
					min-height: 0; 
					border: solid 1px #BDBDBD;
					color: #7e7e7e;">
				<h4>
					Note on  <br /> 
					Privacy  &amp; 
					Security
								</h4>
				<p>
					
					Our servers are “privacy friendly”.
									<br /> 
					
					We DO NOT use google services NOR any other spying service.
				 
					<br /> 
					
					Your emails are read only by us.
				 
					<br /> 
					<b>
					
					Your digital privacy, at least, is preserved from our part.
				 
						</b>
				</p>
			</div>


			<h4>
					Other ways to contact us.
				</h4>
			<p>
				<b>
					phone
				</b> 
				<br /> 
				<span class="muted"> 
					+46 72 868 1516 
				</span> 
				<br />  <br />
				<b>
					email address
				</b> <br /> 
				<span class="muted">
					info@nextholocaust.com 
				</span> 
				<br /> <br />
				<b>
					physical address
				</b> <br /> 
				<span
					class="muted"> 
					NextHolocaust <br />
					Kronborgsvägen, N. 13B<br /> 
					21742, Malmö, Sverige 
				</span>
			</p>
		</div>		

	</div>
</div>



<!-- Modal -->
<div 
	class="modal fade" 
	id="modalFormContact" 
	tabindex="-1" 
	role="dialog"
	aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">
					
					Mail sent.
								</h4>
			</div>
			<div class="modal-body">
					Your email will be attended as soon as possible.
				</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<!-- 
				<button type="button" class="btn btn-primary">Save changes</button>
				 -->
			</div>
		</div>
	</div>
</div>
<!-- END Content -->

<!-- footer START -->
@include('footer');
<!-- footer END -->
<!-- html footer START -->
@include('html_footer');
<!-- html footer END -->


    </body>
</html>