<!DOCTYPE html>
<html lang="es">

@include('html_header');

<body>
@include('es_header')
<!-- es_welcome.blade.php -->

<!-- START Content -->

<!-- revolution slider start -->
<div class="fullwidthbanner-container main-slider">
	<div class="fullwidthabnner">
		<ul id="revolutionul" style="display: none;">
			<!-- 1st slide -->
						<li data-transition="fade" 
				data-slotamount="8" 
				data-masterspeed="300"
				data-delay="15000" 
				data-thumb="">
				<!-- Image background on first slide -->
				<img
					src="/images/brain_lens_1900x590.jpg"
				alt="">
				
				
				<div class="caption lft slide_subtitle dark-text" 
					data-x="790"
					data-y="60"
					data-speed="400" 
					data-start="800"
					data-easing="easeOutExpo">
					¿los
					pensamientos son
            	 <br /> 
					absolutamente privados?
				 
				</div>	
				
				<div class="caption lfr slide_subtitle dark-almost-text" 
					data-x="790"
					data-y="180" 
					data-speed="400" 
					data-start="1500"
					data-easing="easeOutExpo">
					¿Es
					posible leer la mente
            	 <br /> 
					gracias a la tecnología?
				 
				</div>
				
				
				
				<div class="caption lfr slide_title" 
					data-x="26" 
					data-y="70"
					data-speed="400" 
					data-start="3400" 
					data-easing="easeOutExpo">
					
					la tecnología
					para leer 
				 <br /> 
					la mente
					esta aquí.
				 
				</div>
				

				<div class="caption lfr slide_subtitle darker-text" 
					data-x="26"
					data-y="200" 
					data-speed="400" 
					data-start="4000"
					data-easing="easeOutExpo">
					
					La integridad de la privacidad 
				 <br /> 
					de tu mente está quebrada.
				 
				</div>
				
				<div class="caption lfr slide_desc bolder-text darkish-text" 
					data-x="26" 
					data-y="340"
					data-speed="400" 
					data-start="5400" 
					data-easing="easeOutExpo">
					
					La neurociencia moderna puede hacer eso.
									<br />
					
				 <a href="https://en.wikipedia.org/wiki/Thought_identification"
					class="blue-link" target="_blank"> 
					"Identificación del pensamiento" 
				 </a> 
					es el nombre
				 <br /> 
					de dicha técnica.
									
				</div> 
				
				<a class="caption lfr btn yellow slide_btn"
					href="/awareness" 
					data-x="26"
					data-y="450" 
					data-speed="400" 
					data-start="6000"
					data-easing="easeOutExpo"> 
					Sigue leyendo 
				</a>

			</li>
			
			
			<!-- 2nd slider -->
			<li data-transition="fade" 
				data-slotamount="8" 
				data-masterspeed="300"
				data-delay="15000" 
				data-thumb="">
				<!-- Image background on first slide -->
				<img
					src="/images/neuroslave_1900x590_01.jpg"
				alt="">
				
				<!-- 
				<div class="caption lfl slide_item_left" 
					data-x="140" 
					data-y="100"
					data-speed="400" 
					data-start="1000" 
					data-easing="easeOutBack">
					<img
						src="/images/waves_to_the_brain_voice_to_skull_360x326_2.png"
						alt="Image 1">
				</div>
				-->
				
				<div class="caption lft slide_subtitle dark-text" 
					data-x="26"
					data-y="60"
					data-speed="400" 
					data-start="800"
					data-easing="easeOutExpo">
					¿tu mente está en línea?				</div>	
				
				<div class="caption lfr slide_subtitle dark-almost-text" 
					data-x="26"
					data-y="160" 
					data-speed="400" 
					data-start="2000"
					data-easing="easeOutExpo">
					¿tu
					pensamiento puede ser 
				 <br /> 
					espiado de forma remota?
								</div>
				
				
				
				<div class="caption lfr slide_title" 
					data-x="740" 
					data-y="70"
					data-speed="400" 
					data-start="2500" 
					data-easing="easeOutExpo">
					Una
					nueva tecnología 
            	 <br /> está
					disponible
									<!-- and is changing our reality.  -->
				</div>
				

				<div class="caption lfr slide_subtitle darker-text" 
					data-x="740"
					data-y="200" 
					data-speed="400" 
					data-start="3500"
					data-easing="easeOutExpo">
					<!-- 
					Everything has changed.  
					<br />
					-->
					Sé
					consciente de ello. 
            	 <br /> Prepárate.
								</div>
				
				<div class="caption lfr slide_desc bolder-text" 
					data-x="740" 
					data-y="290"
					data-speed="400" 
					data-start="5000" 
					data-easing="easeOutExpo">
					Descubre lo que está pasando.					<br />
					No te asustes: no estás solo.					<br />
					Comparte tus conocimientos y habilidades.					<br />
					Ayuda y consigue ayuda.				</div> 
				
				<a class="caption lfr btn yellow slide_btn"
					href="#welcome" 
					data-x="740"
					data-y="450" 
					data-speed="400" 
					data-start="5500"
					data-easing="easeOutExpo"> 
					Sigue leyendo 
				</a>

			</li>

			
			
			<!-- 3rd Slide START -->
			<li data-transition="fade" data-slotamount="7" data-masterspeed="300"
				data-delay="25000" data-thumb=""><img
				src="/images/subliminal_audio_message_1900x590_01.jpg"
				alt="">
				<div 
					class="caption lfr slide_title slide_item_left yellow-text"
					data-x="670" 
					data-y="125" 
					data-speed="400" 
					data-start="500"
					data-easing="easeOutExpo">
					
					mensajes audio subliminales
								</div>
				<div class="caption lfr slide_subtitle slide_item_left slide_title bolder-text yellow-text" 
					data-x="670"
					data-y="180" 
					data-speed="400" 
					data-start="1000"
					data-easing="easeOutExpo">
					
					reconocerlos
								</div>
				
				<div 
					class="caption lfr slide_desc slide_item_left bolder-text" 
					data-x="670"
					data-y="240" 
					data-speed="400" 
					data-start="2000"
					data-easing="easeOutExpo">
					
					Si prestas atención reconocerás que estás 
					 <br /> 
					rodeado
					de mensajes de audio subliminales.
								</div>
				
				<div 
					class="caption lfr slide_desc slide_item_left bolder-text" 
					data-x="670"
					data-y="310" 
					data-speed="400" 
					data-start="3000"
					data-easing="easeOutExpo">
					
					Mensajes subliminales de audio se utilizan para reemplazar 
					 <br /> 
					a tu voz interior, tus pensamientos originales, 
					 <br /> 
					para sugerir lo que piensas, como si fuera pensado por ti.
								</div>
				
				<a class="caption lft slide_btn btn red slide_item_left"
					href="#welcome"  
					data-x="670" 
					data-y="450" 
					data-speed="400"
					data-start="4000" 
					data-easing="easeOutExpo"> 
					Sigue leyendo 
				</a>
			</li>
			<!-- 3rd Slide END -->
			
			
			<!-- 4nd slide  -->
			<li 
				data-transition="fade" 
				data-slotamount="8" 
				data-masterspeed="700"
				data-delay="15000" 
				data-thumb="">
				<!-- THE MAIN IMAGE IN THE Second SLIDE --> 
				<img
					src="/images/mind_reading_mind_programming_1900x590low_2.jpg"
				alt="">
				
				<div class="caption lft slide_subtitle dark-almost-text" 
					data-x="10"
					data-y="60"
					data-speed="400" 
					data-start="1000"
					data-easing="easeOutExpo">
					El
					terrible descubrimiento
            	 <br /> de
					la vida actual:
								</div>				
				
				<div class="caption lft slide_title" 
					data-x="10" 
					data-y="150"
					data-speed="400" 
					data-start="1800" 
					data-easing="easeOutExpo">
					Los
					pensamientos pueden ser 
            	 <br /> leídos
					y "sugeridos".
								</div>

				<div class="caption lft slide_desc bolder-text dark-almost-text" 
					data-x="10"
					data-y="282" 
					data-speed="400" 
					data-start="2500"
					data-easing="easeOutExpo">
					"Los poderes fácticos" tienen la tecnología...				</div> 
				<div class="caption lft slide_desc bolder-text dark-almost-text" 
					data-x="10"
					data-y="306" 
					data-speed="400" 
					data-start="2800"
					data-easing="easeOutExpo">
					...y la están usando.				</div> 
				
				<a class="caption lft slide_btn btn red slide_item_left"
					href="#welcome" 
					data-x="10" 
					data-y="400" 
					data-speed="400"
					data-start="7000" 
					data-easing="easeOutExpo"> 
					Sigue leyendo 
				</a>
				<div 
					class="caption lft start" 
					data-x="640" 
					data-y="55"
					data-speed="400" 
					data-start="4000" 
					data-easing="easeOutBack">
					
					<img
						src="/images/human_body_hacked_02.png"
						alt="man">
					
				</div>
				<div class="caption lft slide_subtitle dark-almost-text" 
					style="text-align: right"
					data-x="718" 
					data-y="15"
					data-speed="500" 
					data-start="5000" 
					data-easing="easeOutExpo">
					Tu
					mente puede 
            	 <br /> ser
					hackeada
								</div>
				<div class="caption lft slide_item_right" 
					data-x="870" 
					data-y="50"
					data-speed="500" 
					data-start="5000" 
					data-easing="easeOutBack">
					<img
						src="/images/pointer_left_up_down_01.png"
						id="rev-hint2" alt="txt img">
				</div>
			</li>

			
			
			
			
			<!-- 5rd slide  -->
			<li data-transition="fade" data-slotamount="7" data-masterspeed="300"
				data-delay="25000" data-thumb=""><img
				src="/images/holocaust_memorial_1900x590_02.jpg"
				alt="">
				<div 
					class="caption lfr slide_title slide_item_left yellow-txt"
					data-x="670" 
					data-y="125" 
					data-speed="400" 
					data-start="500"
					data-easing="easeOutExpo">
					El Holocausto de la Mente				</div>
				<div class="caption lfr slide_subtitle slide_item_left slide_title bolder-text" 
					data-x="670"
					data-y="180" 
					data-speed="400" 
					data-start="1000"
					data-easing="easeOutExpo">
					es el proximo holocausto				</div>
				
				<div 
					class="caption lfr slide_desc slide_item_left bolder-text" 
					data-x="670"
					data-y="240" 
					data-speed="400" 
					data-start="2000"
					data-easing="easeOutExpo">
					
					La telepatía tecnológica
					 <br /> 
					y otras técnicas de acondicionamiento subliminal
					 <br /> 
					se utilizan
					para esclavizar
					a la población,
					 <br /> 
					para torturar y neutralizar
					individuos libres,
					 <br /> 
					con el fin de construir el totalitarismo más terrible
					 <br /> 
					y
					aniquilar a la humanidad
					tal y como la conocemos.
									
				</div>
				
				<a class="caption lft slide_btn btn red slide_item_left"
					href="#welcome"  
					data-x="670" 
					data-y="450" 
					data-speed="400"
					data-start="2500" 
					data-easing="easeOutExpo"> 
					Sigue leyendo 
				</a>
				
			</li>

		</ul>
		<div class="tp-bannertimer tp-top"></div>
	</div>
</div>
<!-- revolution slider end -->
<!-- END revolutionSlider -->



<!-- START revolutionSlider -->


<!--container start-->
<div class="container">
	<div class="row">
		<!--feature start-->
		<div class="text-center feature-head">
			<h1 style="text-transform: none;"
				id="welcome">
				Bienvenido a MindHolocaust				<span style="text-transform: uppercase;"></span>
			</h1>
			<p class="dark-almost-text">
				Observatorio contra las
					tecnologías de acondicionamiento de la
					mente			</p>
		</div>
	</div>
	
	<div class="row" style="margin-bottom: 36px;">
		<div class="col-lg-4 col-sm-4">
			<section id="awareness-in-short">
				<div class="f-box">
					<i class="fa fa-search"></i>
					<h2>investigación					</h2>
					<p class="dark-almost-text">
					
					acerca de las prácticas militares
									</p>
				</div>

				<p class="f-text">
					
					El observatorio mindholocaust está llevando a cabo una
					investigación sobre las prácticas militares de acondicionamiento de
					la mente.
								</p>
				<p class="f-text">
					
					El objetivo de esta investigación es reconocer y revelar nuevas tecnologías y las consiguientes técnicas derivadas utilizadas específicamente para reducir y limitar el potencial de la mente en una automatización enredada.
								</p>
				<!--
				<p class="f-text">
					
					Entre estas prácticas, se aplican dos paradigmas principales:
								</p>
				<p class="f-text">
					
					1) que se pueda definir un modelo funcional de la mente
									<br />
					
					2) que las técnicas psicologícas de acondicionamiento operante se pueden
					utilizar con el fin de atrofiar y dar forma al corpus del cerebro
					para reducir nuestra capacidad de adquirir y mantener percepciones
					significativas del mundo.
								</p>
				-->
				<p class="f-text">
					
					Actualmente la investigación se centra en tres nuevas prácticas
					que se pueden utilizar en conjunto para cumplir con el objetivo:
								</p>
				<p class="f-text">
					
					1) "leer la mente" o "identificación del pensamiento": la
					capacidad de conocer los pensamientos de alguien, gracias a los
					logros recientes de la neurociencia.
									<br />
					
					2) espionaje del cerebro a distancia: la capacidad de llevar a
					cabo de leer la mente de distancia remota, una especie de "tener la
					mente en línea".
									<br />
					
					3) mensajes subliminales de audio: la capacidad de influir en
					el pensamiento de alguien usando estimulaciones audio apenas
					perceptibles.
								</p>
				<p class="f-text">
					
					 <a href="/awareness/dossier/d/mind-reading" target="_blank"> 
					Ya se ha ado el primer dossier sobre la lectura de la mente: una breve pero exhaustiva y bien documentada historia cronológica de los logros recientes de la neurociencia.
					 </a> 
								</p>
				<p class="f-text">
					<a href="/awareness/dossier/d/mind-reading" target="_blank"><i class="fa fa-link"></i>
					Disfrútalo					</a>
				</p>
				
			</section>
		</div>
	
		<div class="col-lg-4 col-sm-4">
			<section id="awareness-in-short">
				<div class="f-box">
					<i class="fa fa-comments-o"></i>
					<h2>conocimiento					</h2>
					<p class="dark-almost-text">
						reconocer la situación					</p>
				</div>

				<p class="f-text">
					Es importante estar al tanto de la
					situación: conocer los
					fenómenos y sus consecuencias.				</p>

				<p class="f-text">
					En resumen: un poder oculto está
					utilizando la tecnología
					telepática
					y otras técnicas de
					acondicionamiento de la mente con el
					objetivo de dominar a los
					individuos y a la sociedad.				</p>
					
				<p class="f-text">
					Con esta tecnología la voz interior de la
					mente puede ser
					"escuchada" por una tercera persona.				</p>
				
				<p class="f-text">
					Mensajes auditivos también se pueden enviar al sujeto.				</p>
					
				<p class="f-text">
					Este tratamiento puede manipular profundamente el comportamiento de una persona. De todos modos, los efectos pueden atenuarse o neutralizarse, si se conocen.				</p>

				<p class="f-text">
					La tecnología utilizada es casi
					intangible: es difícil de
					detectar, su uso es difícil de
					ser identificado.				</p>

				<p class="f-text">
					Once identified, it is difficult to communicate: it is difficult to talk about this phenomenon with other people.				</p>

				<p class="f-text">
					Estas dos características (el que sea
					difícil de identificar y
					el que sus abusos son dificilmente
					divulgables) han ralentizado su
					denuncia pública.				</p>

				<p class="f-text">
					El capítulo "conocimiento" de
					Mindholocaust pretende denunciar
					públicamente este fenómeno
					y describirlo con detalle.				</p>

				<p class="f-text">
					<a href="/awareness" target="_blank"><i class="fa fa-link"></i>
					Sigue leyendo					</a>
				</p>

			</section>
		</div>
		<div class="col-lg-4 col-sm-4">
			<section id="fellowship-in-short">
				<div class="f-box">
					<i class="fa fa-life-ring"></i>
					<h2>comunidad</h2>
					<p class="dark-almost-text">recibe y da apoyo mutuo</p>
				</div>

				<p class="f-text">
					Si oyes voces y crees que son reales,
					entonces estás en el lugar
					correcto y aquí encontrarás personas
					que piensan lo mismo.				</p>

				<p class="f-text">
					Si percibes que tus pensamientos son sugeridos por mensajes de audio subliminales, a veces una voz apenas perceptible, entonces puedes ser víctima de la telepatía tecnológica.				</p>

				<p class="f-text">
					Si puedes escuchar mensajes subliminales en contextos sociales, radio, televisión o a través de cualquier otro "ruido blanco", puedes estar entonces consciente del control ejercido sobre las masas a través del uso de técnicas de acondicionamiento subliminal con medios tecnológicos.				</p>

				<p class="f-text">
					No es una enfermedad. Es el mundo en el
					que vivimos. Esto está
					sucediendo debido a los poderes ocultos. Existen				</p>

				<p class="f-text">
					Cuando alguien lo descubre, por lo general la primera reacción
					es el miedo y el pánico. Pero podemos ayudarte a sobreponerte.
					Nosotros ya hemos pasado por ello, y parece que ahora es tu turno.
					Puedes contar con nuestra ayuda.				</p>

				<p class="f-text">
					Por lo tanto, no dudes en contactar con
					nosotros. Un día, es
					posible que ayudes a alguien que está
					en tu situación actual.				</p>

				<p class="f-text">
					<a href="/contact"><i class="fa fa-link"></i>
					Contáctanos					</a>
				</p>
			</section>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4 col-sm-4">
			<section id="actions-in-short">
				<div class="f-box">
					<i class="fa fa-sun-o"></i>
					<h2>acciones</h2>
					<p class="dark-almost-text">
					cómo resistir y responder					</p>
				</div>

				<p class="f-text">
					A medida que tomas conciencia de
					la tecnología telepática, ¿qué
					hacer?				</p>

				<p class="f-text">
					
					En primer lugar, necesitas mantener la calma. El primer objetivo
					es
					ganar el choque tomado debido a la conciencia de lo que está
					sucediendo. ¿Cómo?
	             <a href="#techniques_to_avoid_panic"> 
					Algunas técnicas para evitar el pánico 
	             </a> 
					se explican a continuación. Ayúdate a evitar un programa
					público de
					tratamiento mental.
								</p>

				<p class="f-text">
					
					La conmoción debida a la conciencia puede estimular conductas
					autodestructivas y el deseo de suicidarse. Es bueno evitar
					reacciones reflejas. A continuación hay 
	              <a href="#instinctive_reactions_to_be_avoided"> 
					una lista de reacciones instintivas que es mejor evitar
	             </a> .
								</p>

				<p class="f-text">
					
					Tomar conciencia generalmente provoca angustia. Este
					estado
					emocional puede durar años. Es bueno hacer un esfuerzo para
					reducir
					el impacto de la angustia.
					A continuación se presentan algunas 
	             <a href="#how_to_minimize_the_anguish"> 
					técnicas para reducir la angustia
	             </a> 
					al mínimo.
								</p>

				<p class="f-text">
					
					Una vez que la angustia se ha atenuado, entonces es
					posible dirigir
					la respuesta al mundo exterior. 
	           	 <a href="#actions_to_counter_the_threat"> 
					Una lista de medidas para combatir la amenaza de la MindHolocaust 
	           	 </a> 
					se presentan a continuación.
								</p>
				
				<p class="f-text">
					<a href="#take-action"><i class="fa fa-link"></i> 
					Actúa					</a>
				</p>
			</section>
		</div>
		<div class="col-lg-4 col-sm-4">
			<section>
				<div class="f-box">
					<i class="fa fa-map-marker"></i>
					<h2>lugares</h2>
					<p class="dark-almost-text">
					reunirse y relajarse					</p>
				</div>

				<p class="f-text">
					
					¿Te gustaría conocer a otras personas que
					luchan contra el abuso
					de la tecnología telepática?				</p>

				<p class="f-text">
					
					A continuación presentamos
        		 <a href="#meet_activists"> 
					una lista de los lugares donde se puede encontrar
					activistas de
					MindHolocaust</a>.
					Puedes solicitar una reunión a través
					del
				 <a href="/contact"> 
					formulario de contacto </a> .
								</p>

				<p class="f-text">
					
					Es también posible usar la
            	
        		<a href="/meetup" 
					title="meetup"
					target="_blank">
				
					aplicación "Meetup"
				 </a> 
					con el fin de organizar reuniones en tu ciudad.
								</p>

				<p class="f-text"> 
					
            	
        		<a href="/channels" 
					title="channels" 
					target="_blank">
				
					Algunos canales de comunicación en línea
					
				</a> 
				
					están disponibles para la comunidad.
								</p>

				<p class="f-text">
					
					Entre los proyectos en construcción mencionamos
					los más
					importantes.				</p>

				<p class="f-text">
					
					La bienal de MindHolocaust, sobre el abuso de la tecnología
					telepática. Es posible presentar la propuesta de las
					obras/investigaciones a través del
				
				<a href="/biennal" 
					title="biennal" 
					target="_blank">
				
					formulario de sumisión</a>.
				 
					</p>

				<p class="f-text">
					
					El laboratorio "Mind Lab", para practicar las técnicas y
					tecnologías de resistencia y respuesta al fenómeno. 
				</p>

				<p class="f-text">
					
					Una estancia segura en la naturaleza, para tener la mente
					clara
					y para relajarse. 
				</p>

				<p class="f-text">
					<a href="#take-part"><i class="fa fa-link"></i> 
					Participa 
					</a>
				</p>

			</section>
		</div>
		<!--feature end-->
	</div>
</div><!-- END revolutionSlider -->


<!-- START newsletter -->
<div class="container">
	<div class="row">
            <!--quote start -->
            <div class="quote newsletter"> 
            
                <div class="col-lg-9 col-sm-9">
	                <div class="quote-info">
    	            	<div style="margin-top: 4px;">
        	                <span class="title">newsletter</span>
                   		</div>
                   		
		                <input type="text" 
		                	name="subscribeNewsletterEmail" 
		                	data-tag="subscribeNewsletterEmail"
		                	placeholder="tu-correo@ejemplo.com" />
		                	
		                <div 
		                	style="color: #a8a8a8; margin-top: 4px;">
		                La página de cancelación de la suscripción está		                <a href="http://ml.nextholocaust.com/?p=unsubscribe" 
		                style="color:  #f77b6f;" 
		                target="_blank">
		                aquí		                </a>
		                </div>
		                
		                <div class="subscribeMessages">
        	                <span
        	                	data-tag="subscribeMessageSuccess" 
        	                	style="color: #41CAC0;">
							
	             <b> 
					Tu suscripción ha sido enviada! 
            	 </b> 
            	 <br /> 
					Esperamos tu confirmación...
					Revisa tu correo electrónico (consulta
					también la carpeta de spam).
				        	                </span>
        	                <span
        	                	data-tag="subscribeMessageError" 
        	                	style="color: #ec6459;">
        	                Error: la dirección de correo electrónico no es válida..        	                </span>
                   		</div>
					</div>
                </div>
                
                
                <div class="col-lg-3 col-sm-3">
                    <a class="btn btn-danger subscribe-btn" 
                    	href="javascript:void(0)"
                    	data-tag="subscribeButton">
                    	Suscríbete                    </a>
                </div>
                
            </div>
            <!--quote end-->
        </div>
</div>
<!-- END newsletter -->


<!-- START byte science decoding_our_sense -->
<div class="container" style="margin-bottom: 48px;">

	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xm-12">
			<h2 class="r-work">
					Descodificando nuestros sentidos
				</h2>
			<h3 class="r-work-subtitle">
					Nuestros sentidos, nuestras experiencias audiovisuales, pueden ser decodificados y reconstruidos usando fMRI y los algoritmos informáticos científicos demandados a finales de 2011 a partir de 2012.
				</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-12 col-xm-12">

		<video class="video-clip" 
			controls="controls"
			poster="/images/Science_Bytes_-_Decoding_Our_Senses2.png">
			<!-- 
			<source 
				src="//video/Science_Bytes_-_Decoding_Our_Senses.ogv" 
				type="video/ogg" 
				media="all" />
			 -->
			<source 
				src="/video/Science_Bytes_-_Decoding_Our_Senses.webm" 
				type="video/webm" 
				media="all" />
			<source 
				src="/video/Science_Bytes_-_Decoding_Our_Senses.mp4" 
				type="video/mp4" 
				media="all" />
		</video>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-12 col-xm-12">	
		
			<p>
					Un corto documental sobre la descondificaciòn experiencias audiovisuales producido en 
					la serie "Bytes de la Ciencia", financiado por Alfred P. Sloan Foundation.
				</p>
			<p style="font-style: italic;">
				
					Copyright © 2009 por CBS. Todos Los Derechos Reservados.
								<br />
				
					Se cree que el uso de un videoclip parcial de escala
					reducida y de baja resolución se califica como uso justo.
							</p>
			
			<p style="margin-top:36px;">
				<a href="https://vimeo.com/42863899"
				target="_blank">
				<i class="fa fa-vimeo"></i> &nbsp;
					Miralo en Vimeo				</a>
				<br />
				<a href="/video/Science_Bytes_-_Decoding_Our_Senses.mp4"
				target="_blank">
				<i class="fa fa-download"></i> &nbsp;
					Download
				</a>
			</p>
			<br />
			<p>
				<a class="btn morevideo-btn" href="/videos">
				más vídeos				</a>
			</p>
			
		</div>	
	</div>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xm-12">
		
		<br /><br />
		
		</div>
	</div>
	
</div>
<!-- END cbyte science decoding_our_sense -->


<!-- START facebook-building8 -->
<!--parallax start-->
<section class="parallax2">
	<div class="container fbb8-container" style="">
		<div class="row fbb8-line"  style="margin-top: 5%;">
			<div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-4 fbb8-fb-image fbb8-fb-image-fb" 
				style="margin-top: 0%;">

				<div style="width: 70%; margin-right: auto; margin-left: auto; margin-top: 3%;">
					<img src="/images/facebook-brain-chip.png" class="img-responsive">
				</div>


			</div>
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-4 fbb8-title" 
				style="">

				<h1 class="r-work">
					<span>
					Control mental y redes sociales					</span>
				</h1>

				<div class="fbb8-subtitle">
					<h3 class="r-work-subtitle">
						<span>
						La anterior directora de DARPA Regina Douglas inicia el proyecto de Facebook sobre tecnologías de lectura mental.</span>
					</h3>
				</div>


			</div>
			<div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-4 fbb8-fb-image fbb8-fb-image-dp" 
				style="margin-top: 0%;">

				<img src="/images/darpa-logo-618x339.png" class="img-responsive">

			</div>
		</div><!-- row end -->
		<div class="row fbb8-line"  style="margin-top: 5%;">
			<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-xs-5 fbb8-fb-image fbb8-fb-image-rd" 
				style="">

				<img src="/images/douglas-fbb8.jpg" class="img-responsive">

			</div>
			<div class="col-xl-8 col-lg-8 col-md-6 col-sm-6 col-xs-7 fbb8-paragraph">

				<p class="">
					<span>
					
				<a 
					href="https://news.mindholocaust.is/facebook-hiring-neuroscientists-to-build-mind-reading-network-a-social-one/" 
					target="_blank">
				"building 8"
				</a>
				
				es el nombre del proyecto, que beneficiará "de cientos de personas y cientos de millones de dólares", dijo Mark Zuckerberg en un tweet. Es probablemente el imprinting de DARPA para la próxima generación "social".					</span>
				</p>

				<!--
				<p class="">
					<span>
					DARPA es la agencia de servicios secretos militar de los Estados Unidos para proyectos de investigación avanzada, como la bomba atómica, el GPS, el Internet, las bombas inteligentes, la guerra psicológica, la ciencia social de 2da generación, etc.					</span>
				</p>
				-->

			</div>
		</div><!-- row end -->

		
		<div class="row fbb8-line"  style="margin-top: 5%; margin-bottom: 6%;">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 fbb8-link" 
				style="margin-top: 0%;">

				<p class="">
					<span>
					<a class="btn fbb8-btn" 
						href="https://news.mindholocaust.is/facebook-unveils-building-8-brain-activity-decoding-project/"
						target="_blank">
					Lee mas...
					</a>
					</span>
				</p>

			</div>
		</div><!-- row end -->
	</div>
</section>
<!--parallax end--><!-- END facebook-building8 -->


<!-- START awarness -->

<div class="container" 
	id="awareness"
	style="margin-bottom: 50px">

	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<section class="panel tab">
			
				<header class="panel-heading tab-dossier">
					<ul class="nav nav-tabs nav-justified ">
						<li class="active only">
							
							<div style="float:left;">
								<i class="fa fa-5x fa-comments-o"
									style="vertical-align: middle; color: #475168 ;">
								</i>
							</div>
							<div style="float:left; margin-left: 10px;">
								<p class="dark-almost-text" 
									style="color: #475168 ; margin-bottom: 0px;">
									
					en profundidad:
									
								</p>
								<h1 class="r-work" 
									style="margin-bottom: 0px; margin-top: 0px;">
									
					conocimiento
												</h1>
								<p class="dark-almost-text" 
									style="color: #475168 ;">
									
					reconocer la situación
				 					
								</p>
							</div>
							<div style="clear: both;"></div>
						
						</li>
						<li class="none"></li>
						<li class="none"></li>
					</ul>
				</header>
				<div class="panel-body">

 						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"
 							style="height: 172px">
							<div class="element item view view-tenth" 
								data-zlname="reverse-effect">
									<img src="/images/dossier.png" 
										alt="dossier mind reading" 
										style=" 
											border-radius: 4px; 
											width: 100%;
											margin-bottom: 12px;"
									/>
									<div class="mask">
											<a data-zl-popup="link" 
												href="/es/mind-reading-dossier-part-1">
													<i class="fa fa-link"></i>
											</a>
									</div>
							</div>
						</div>

						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
							<h3 class="r-work-subtitle-bigger">
								<a href="/es/mind-reading-dossier-part-1">
								
									Historia de la Identificación del Pensamiento
									<br /><span class='reduxtext'>
									un dossier sobre la Lectura
									de la mente, Parte I (2006-2015)
									</span>
								</a>		
							</h3>
							<p class="f-text">
								
					MindHolocaust se enorgullece de presentar y ar el dossier
					sobre la "lectura de la mente": una corta pero exhaustiva historia cronológica
					sobre la tecnología de lectura de la mente.
							</p>
							<p class="f-text">
								
					El dossier da respuestas a la pregunta "¿Existe la
					tecnología
					de
					lectura mental?" con una precisión extrema.
							</p>
							<p class="f-text">
								
					Cada enunciado del dossier está bien documentado y bien
					probado.
							</p>	
							<p class="f-text">
								
					Toda la documentación se proporciona con el dossier
					como enlaces
					web y documentos PDF.
				 
							</p>
							<p class="f-text">
								
					Todos los documentos del dossier son documentos oficiales:
					aciones científicas, comunicados de prensa ados por
					las
					universidades o por las instituciones
					governativas o agencias
					federales.
				 
							</p>
							<p class="f-text">
								<a href="/es/mind-reading-dossier-part-1">
									<i class="fa fa-link"></i>
									
					Leer y disfrutar del dossier
				 
								</a>
							 </p>
						</div>
				</div>
			</section>
		</div>
	</div>

</div>
<!-- END awareness -->

<!-- START artworks video -->
<!-- Educationsl VIDEO -->
<div class="property gray-bg">
	<div class="container educationals">
		<div class="row">
			<div class="col-lg-12 col-sm-12">
				<h2 class="r-work">Metropia: una distopía plausible</h2>
				<h3 class="r-work-subtitle">
					Cómo se presenta la tecnología telepática.
				</h3>
				
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-3 col-sm-3">
				<div class="quote-info">
					<p>
						Metropia es una película sobre una distopía
						impulsada por las
						empresas en un mundo en el cual las
						corporaciones pueden leer los
						pensamientos de la
						gente y dar información a la mente de las
						personas...
					</p>
					<p>
						<a 
							class="p-head"
							style="font-size: 12px; font-weight: 300"
							href="http://en.wikipedia.org/wiki/Metropia_%28film%29"
							target="_blank"><i class="fa fa-link"></i>  &nbsp;
							Leer más sobre Metropia						</a>
						<a href="/artworks" class="btn btn-danger mediateca-btn">
							más obras de arte
						</a>
					</p>
				</div>
			</div>

			<div class="col-lg-3 col-sm-3">
				<div class="video-thumb element view-tenth item" data-zlname="reverse-effect">
					<img src="/images/thumbnail-metropia-video-01-review.jpg" 
    					alt="" />
						<div class="mask">
							<a data-zl-popup="link" 
								href="/es/artworks/metropia-the-main-idea">
									<i class="fa fa-play"></i>
							</a>
						</div>
				</div>
				<div class="title-bottom">
					<a class=" p-head" 
						href="/es/artworks/metropia-the-main-idea">
						Metropia: La idea principal.					</a>
					<p>
						Este videoclip es un extracto a partir de una reseña
						sobre Metropia: aquí se explica la idea principal de la película.
					</p> 
				</div>
			</div>

			<div class="col-lg-3 col-sm-3">
				<div class="video-thumb element view-tenth item" data-zlname="reverse-effect">
					<img src="/images/thumbnail-metropia-video-02-inneryou.jpg" 
    					alt="" />
						<div class="mask">
							<a data-zl-popup="link" 
								href="/es/artworks/metropia-am-i-hearing-things">
									<i class="fa fa-play"></i>
							</a>
						</div>
				</div>
				<div class="title-bottom">
					<a class=" p-head" 
						href="/es/artworks/metropia-am-i-hearing-things">
						Metropia: ¿Estoy escuchando cosas?					</a>
					<p>
						Este videoclip trata acerca de un hombre que está
						escuchando la voz de un extraño dentro de
						su propia cabeza. El
						extraño escucha los
						pensamientos de la voz interior del hombre...
					</p> 
				</div>
			</div>

			<div class="col-lg-3 col-sm-3">
				<div class="video-thumb element view-tenth item" data-zlname="reverse-effect">
					<img src="/images/thumbnail-metropia-video-02-hardtotalk.jpg" 
    					alt="" />
						<div class="mask">
							<a data-zl-popup="link" 
								href="/es/artworks/metropia-it-is-hard-to-talk-about-it">
									<i class="fa fa-play"></i>
							</a>
						</div>
				</div>
				<div class="title-bottom">
					<a class=" p-head" 
						href="/es/artworks/metropia-it-is-hard-to-talk-about-it">
						Metropia: Es difícil hablar de esto.</a>
					<p>
						Este videoclip muestra cuán violento puede ser hablar
						de este fenómeno.
					</p> 
				</div>
			</div>
			
		</div>
	</div>
</div>
<!-- END artworks -->


<!-- START cbs_documentary -->
<!-- /application/modules/default/views/scripts/partials/cbs_documentary.phtml -->

<div class="container" style="margin-bottom: 48px;">

	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xm-12">
			<h2 class="r-work">
					La
					lectura de tu mente
				</h2>
			<h3 class="r-work-subtitle">
					el documental de la CBS acerca de
					la técnica de la neurociencia
					llamada
					&#8220;identificación
					del pensamiento &#8221;
				</h3>
		</div>
	</div>


	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-12 col-xm-12">

		<video class="video-clip" 
			controls="controls"
			poster="/images/CBS_Reading_Your_Mind.png">
			<!-- 
			<source 
				src="//video/CBS_Reading_Your_Mind.ogv" 
				type="video/ogg" 
				media="all" />
			 -->
			<source 
				src="/video/CBS_Reading_Your_Mind.webm" 
				type="video/webm" 
				media="all" />
			<source 
				src="/video/CBS_Reading_Your_Mind.mp4" 
				type="video/mp4" 
				media="all" />
		</video>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-12 col-xm-12">	
		
			<p>
					La neurociencia ha aprendido mucho acerca de la
					actividad del
					cerebro y su relación con ciertos pensamientos. 
					 <br /> 
					Como
					informa Lesley Stahl, que ahora puede ser posible, en un nivel
					básico, de leer la mente de una persona.
				</p>
			<p style="font-style: italic;">
				
					Copyright © 2009 por CBS. Todos Los Derechos Reservados.
								<br />
				
					Se cree que el uso de un videoclip parcial de escala
					reducida y de baja resolución se califica como uso justo.
							</p>
			
			<p style="margin-top:36px;">
				<a href="https://www.youtube.com/watch?v=8jc8URRxPIg"
				target="_blank">
				<i class="fa fa-youtube"></i> &nbsp;
					Miralo en Youtube				</a>
				<br />
				<a href="//video/CBS_Reading_Your_Mind.mp4"
				target="_blank">
				<i class="fa fa-download"></i> &nbsp;
					Download
				</a>
			</p>
			<br /> <br />
			<p>
				<a class="btn morevideo-btn" href="/es/videos">
				más vídeos				</a>
			</p>
			
		</div>	
	</div>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xm-12">
		<hr />
		</div>
	</div>
	
</div><!-- END cbs_documentary -->


<!-- START targeted-individuals-map -->
<div class="property gray-bg">
	<div class="container educationals">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<h2 class="r-work">Mapa de los individuos perseguidos</h2>
				<h3 class="r-work-subtitle">
				Censo global de individuos perseguidos.				</h3>
			
				<div class="title-bottom">
					<a class=" p-head" href="http://targeted-individuals-map.mindholocaust.is" 
					target="_blank" >
					<i class="fa fa-link"></i>
						Abrir el mapa del censo de los individuos perseguidos					
					</a>
					<p>
						<i>
						Tenga en cuenta, al hacer clic en el enlace se abrirá una página web que es traqueada por Google.						</i>
					</p>
					<p>	
						El mapa de personas perseguidas es un censo mundial de personas perseguidas mostrados en un mapa mundial.					</p> 
					<p>	
						El sitio web de la mapa-de-individuos-perseguidos ofrece una visión global sobre la comunidad de personas que se marcaron a sí mismos en este censo como Individuos Perseguidos por alguna forma de Mind Control.					</p> 
				</div>

			</div><!-- ends DIV class: column -->

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<a href="http://targeted-individuals-map.mindholocaust.is"
					target="_blank">
				<img style="width: 100%; border-radius: 4px; margin-top: 10px;" 
						src="/images/targeted-individuals-map.png" 
						alt="targeted individuals map" />
				</a>
			</div>

			
		</div><!-- ends DIV class: row -->
	</div><!-- ends DIV class: container -->
</div><!-- ends DIV class: property --><!-- END targeted-individuals-map -->



<!-- START articles & events -->


<div class="container" style="margin-bottom: 50px">
	<div class="row">
	
		<!-- LATEST ARTICLES -->
		<div class="col-lg-9 col-sm-9">
			<h1 class="r-work">
			Últimos Artículos			</h1>


			<article class="media">
				
				<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">

				<a class="pull-left thumb" href="https://mindholocaust.is.the.nextholocaust.com/news/a-film-about-synthetic-telepathy-and-a-dystopic-world-of-secret-experiments/"> 
					<img style="width: 100%; border-radius: 25px; margin-bottom: 12px;"
					src="https://mindholocaust.is.the.nextholocaust.com/news/wp-content/uploads/2018/06/distortedheader.jpg"
					alt="A film about synthetic telepathy and a dystopic world of secret experiments" />
				</a>

				</div>
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">

				<p style="color: #797979; font-style: italic;">
					Artículo ado el 
					<time datetime="June 28, 2018" pubdate="pubdate">
					June 28, 2018					</time>
				</p>
				<div class="media-body">
					<a href="https://mindholocaust.is.the.nextholocaust.com/news/a-film-about-synthetic-telepathy-and-a-dystopic-world-of-secret-experiments/" class=" p-head">
						A film about synthetic telepathy and a dystopic world of secret experiments					</a>
					<p>
						<p>  A movie about synthetic telepathy is out since June the 22nd, 2018. Produced by Mind’s Eye Pictures and starring John Cusack and Christina Ricci, Distorted is about a capitalistic dystopia that involves subliminal neuro-advertising messages, AI, mental heath treatments and a global conspiracy. In the film, Ricci plays a young bipolar woman who moves into a state-of-the-art […]</p>
					</p>
					<p>

						<a href="https://mindholocaust.is.the.nextholocaust.com/news/a-film-about-synthetic-telepathy-and-a-dystopic-world-of-secret-experiments/">
						<i class="fa fa-link"></i>
						Sigue leyendo</a>
					</p>
				</div>

				</div>
			</article>
			<hr>


			<article class="media">
				
				<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">

				<a class="pull-left thumb" href="https://mindholocaust.is.the.nextholocaust.com/news/a-brain-controlled-film/"> 
					<img style="width: 100%; border-radius: 25px; margin-bottom: 12px;"
					src="https://mindholocaust.is.the.nextholocaust.com/news/wp-content/uploads/2018/05/film_frame.png"
					alt="A brain controlled film" />
				</a>

				</div>
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">

				<p style="color: #797979; font-style: italic;">
					Artículo ado el 
					<time datetime="May 30, 2018" pubdate="pubdate">
					May 30, 2018					</time>
				</p>
				<div class="media-body">
					<a href="https://mindholocaust.is.the.nextholocaust.com/news/a-brain-controlled-film/" class=" p-head">
						A brain controlled film					</a>
					<p>
						<p>  A brain-controlled movie is out in June 2018! which picks up reactions from the viewer via an electroencephalogram (EEG) headset and switches the storyline depending on their response. [1] The film, called ‘The Moment’, has been developed by the University of Nottingham’s computer science PhD researcher and creative director Richard Ramchurn. It is being officially launched at Sheffield Doc/Fest on June […]</p>
					</p>
					<p>

						<a href="https://mindholocaust.is.the.nextholocaust.com/news/a-brain-controlled-film/">
						<i class="fa fa-link"></i>
						Sigue leyendo</a>
					</p>
				</div>

				</div>
			</article>
			<hr>


			<article class="media">
				
				<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">

				<a class="pull-left thumb" href="https://mindholocaust.is.the.nextholocaust.com/news/alterego-the-headset-that-listen-to-your-internal-verbalization/"> 
					<img style="width: 100%; border-radius: 25px; margin-bottom: 12px;"
					src="https://mindholocaust.is.the.nextholocaust.com/news/wp-content/uploads/2018/04/Second_self.png"
					alt="AlterEgo, the headset that listen to your internal verbalization" />
				</a>

				</div>
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">

				<p style="color: #797979; font-style: italic;">
					Artículo ado el 
					<time datetime="April 8, 2018" pubdate="pubdate">
					April 8, 2018					</time>
				</p>
				<div class="media-body">
					<a href="https://mindholocaust.is.the.nextholocaust.com/news/alterego-the-headset-that-listen-to-your-internal-verbalization/" class=" p-head">
						AlterEgo, the headset that listen to your internal verbalization					</a>
					<p>
						<p>Thanks to AlterEgo wearable gadget, neuromuscular signals from a user will be fed into a neural network (AI) that is trained by researchers “to identify [a fundamental set of] subvocalized words from neuromuscular signals, but it can be customized to a particular user,” the MIT News Office reported. This successful direct approach to the problem, […]</p>
					</p>
					<p>

						<a href="https://mindholocaust.is.the.nextholocaust.com/news/alterego-the-headset-that-listen-to-your-internal-verbalization/">
						<i class="fa fa-link"></i>
						Sigue leyendo</a>
					</p>
				</div>

				</div>
			</article>
			<hr>


			<article class="media">
				
				<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">

				<a class="pull-left thumb" href="https://mindholocaust.is.the.nextholocaust.com/news/trans-cranial-stimulation-can-really-improve-our-memory-darpa-funded-research-on-epilepsy-has-found/"> 
					<img style="width: 100%; border-radius: 25px; margin-bottom: 12px;"
					src="https://mindholocaust.is.the.nextholocaust.com/news/wp-content/uploads/2018/02/Screenshot-from-2018-02-15-19-36-35.png"
					alt="Trans-cranial stimulation can really improve our memory, DARPA funded research on epilepsy has found." />
				</a>

				</div>
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">

				<p style="color: #797979; font-style: italic;">
					Artículo ado el 
					<time datetime="February 7, 2018" pubdate="pubdate">
					February 7, 2018					</time>
				</p>
				<div class="media-body">
					<a href="https://mindholocaust.is.the.nextholocaust.com/news/trans-cranial-stimulation-can-really-improve-our-memory-darpa-funded-research-on-epilepsy-has-found/" class=" p-head">
						Trans-cranial stimulation can really improve our memory, DARPA funded research on epilepsy has found.					</a>
					<p>
						<p>A little electrical brain stimulation (trans-craneal) can boost our memory. The trans-craneal stimulation is a brain-stimulation technique in which electrical current is applied via electrodes implanted on or directly in the brain parenchyma. It can be a bit invasive. The key is to deliver a tiny pulse of electricity to exactly the right place at exactly the […]</p>
					</p>
					<p>

						<a href="https://mindholocaust.is.the.nextholocaust.com/news/trans-cranial-stimulation-can-really-improve-our-memory-darpa-funded-research-on-epilepsy-has-found/">
						<i class="fa fa-link"></i>
						Sigue leyendo</a>
					</p>
				</div>

				</div>
			</article>
			<hr>


			<article class="media">
				
				<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">

				<a class="pull-left thumb" href="https://mindholocaust.is.the.nextholocaust.com/news/decoding-thoughts-and-speech-neuroprosthesis-at-the-amazing-brain-symposium-lunds-universitet-350-anniversary-science-week-about-neurocience/"> 
					<img style="width: 100%; border-radius: 25px; margin-bottom: 12px;"
					src="https://mindholocaust.is.the.nextholocaust.com/news/wp-content/uploads/2017/09/The-Amazing-Brain-Graphinc-02.png"
					alt="Decoding thoughts and  speech neuroprosthesis at “The Amazing Brain Symposium”" />
				</a>

				</div>
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">

				<p style="color: #797979; font-style: italic;">
					Artículo ado el 
					<time datetime="September 9, 2017" pubdate="pubdate">
					September 9, 2017					</time>
				</p>
				<div class="media-body">
					<a href="https://mindholocaust.is.the.nextholocaust.com/news/decoding-thoughts-and-speech-neuroprosthesis-at-the-amazing-brain-symposium-lunds-universitet-350-anniversary-science-week-about-neurocience/" class=" p-head">
						Decoding thoughts and  speech neuroprosthesis at “The Amazing Brain Symposium”					</a>
					<p>
						<p>During the science week that has occurred on September the 4th-10th, 2017,at Lunds Universitet 350 anniversary, of great interest for all neuroscience lovers, Robert T. Knight, Professor of Neuroscience and Psychology at the University of California, Berkeley, USA had one lecture about “Decoding Thought from the Human Brain”: Recording neural signals directly from the human […]</p>
					</p>
					<p>

						<a href="https://mindholocaust.is.the.nextholocaust.com/news/decoding-thoughts-and-speech-neuroprosthesis-at-the-amazing-brain-symposium-lunds-universitet-350-anniversary-science-week-about-neurocience/">
						<i class="fa fa-link"></i>
						Sigue leyendo</a>
					</p>
				</div>

				</div>
			</article>
			<hr>


			<article class="media">
				
				<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">

				<a class="pull-left thumb" href="https://mindholocaust.is.the.nextholocaust.com/news/empathy-through-technology-meetup-in-malmo/"> 
					<img style="width: 100%; border-radius: 25px; margin-bottom: 12px;"
					src="https://mindholocaust.is.the.nextholocaust.com/news/wp-content/uploads/2017/08/Screenshot-from-2017-08-07-23-04-11.png"
					alt="Empathy Through Technology meetup in Malmö" />
				</a>

				</div>
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">

				<p style="color: #797979; font-style: italic;">
					Artículo ado el 
					<time datetime="August 7, 2017" pubdate="pubdate">
					August 7, 2017					</time>
				</p>
				<div class="media-body">
					<a href="https://mindholocaust.is.the.nextholocaust.com/news/empathy-through-technology-meetup-in-malmo/" class=" p-head">
						Empathy Through Technology meetup in Malmö					</a>
					<p>
						<p>On July 2017 the 31st a promising group was founded on the meetup platform about BCI. As the meetup page announce: Empathy Through Technology Meetup supports in connecting professionals with various backgrounds; can be arts – > technology; with various skills, talents but having the same interest. On the 7th of August the group counts […]</p>
					</p>
					<p>

						<a href="https://mindholocaust.is.the.nextholocaust.com/news/empathy-through-technology-meetup-in-malmo/">
						<i class="fa fa-link"></i>
						Sigue leyendo</a>
					</p>
				</div>

				</div>
			</article>
			<hr>


			<article class="media">
				
				<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">

				<a class="pull-left thumb" href="https://mindholocaust.is.the.nextholocaust.com/news/new-human-rights-against-mind-data-hacking-mind-reading-and-mind-manipulation-through-technology/"> 
					<img style="width: 100%; border-radius: 25px; margin-bottom: 12px;"
					src="https://mindholocaust.is.the.nextholocaust.com/news/wp-content/uploads/2017/04/KN_neurolaw.jpg"
					alt="New human rights against mind-data-hacking (mind-reading and mind-manipulation through technology) proposed." />
				</a>

				</div>
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">

				<p style="color: #797979; font-style: italic;">
					Artículo ado el 
					<time datetime="April 26, 2017" pubdate="pubdate">
					April 26, 2017					</time>
				</p>
				<div class="media-body">
					<a href="https://mindholocaust.is.the.nextholocaust.com/news/new-human-rights-against-mind-data-hacking-mind-reading-and-mind-manipulation-through-technology/" class=" p-head">
						New human rights against mind-data-hacking (mind-reading and mind-manipulation through technology) proposed.					</a>
					<p>
						<p>New human rights that would protect people from having their thoughts and other brain information stolen, abused or hacked have been proposed by researchers, as a response to advances in neurotechnology that can read or alter brain activity. [1][2] Four new human rights for the freedom of the mind have been proposed in the open access journal Life […]</p>
					</p>
					<p>

						<a href="https://mindholocaust.is.the.nextholocaust.com/news/new-human-rights-against-mind-data-hacking-mind-reading-and-mind-manipulation-through-technology/">
						<i class="fa fa-link"></i>
						Sigue leyendo</a>
					</p>
				</div>

				</div>
			</article>
			<hr>


			<article class="media">
				
				<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">

				<a class="pull-left thumb" href="https://mindholocaust.is.the.nextholocaust.com/news/facebook-unveils-building-8-brain-activity-decoding-project/"> 
					<img style="width: 100%; border-radius: 25px; margin-bottom: 12px;"
					src="https://mindholocaust.is.the.nextholocaust.com/news/wp-content/uploads/2017/04/Dugan-at-F8.png"
					alt="Facebook unveils Building 8: it’s about brain activity decoding" />
				</a>

				</div>
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">

				<p style="color: #797979; font-style: italic;">
					Artículo ado el 
					<time datetime="April 19, 2017" pubdate="pubdate">
					April 19, 2017					</time>
				</p>
				<div class="media-body">
					<a href="https://mindholocaust.is.the.nextholocaust.com/news/facebook-unveils-building-8-brain-activity-decoding-project/" class=" p-head">
						Facebook unveils Building 8: it’s about brain activity decoding					</a>
					<p>
						<p>It was spotted already in Jan 14th , 2017, by the Russians. RT published an article that was leaving no doubt: “Facebook’s job ads suggest ‘mind reading’ social networks could soon be a reality. Three months later during the 7th F8 conference, Facebook annual event for engineers, it was announced on stage to the  world. [1] [2] […]</p>
					</p>
					<p>

						<a href="https://mindholocaust.is.the.nextholocaust.com/news/facebook-unveils-building-8-brain-activity-decoding-project/">
						<i class="fa fa-link"></i>
						Sigue leyendo</a>
					</p>
				</div>

				</div>
			</article>
			<hr>


			<article class="media">
				
				<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">

				<a class="pull-left thumb" href="https://mindholocaust.is.the.nextholocaust.com/news/first-brain-controlled-drone-race/"> 
					<img style="width: 100%; border-radius: 25px; margin-bottom: 12px;"
					src="https://mindholocaust.is.the.nextholocaust.com/news/wp-content/uploads/2018/08/brain-drone-race_02.png"
					alt="First brain-controlled drone race took place in Gainesville (FL, USA)" />
				</a>

				</div>
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">

				<p style="color: #797979; font-style: italic;">
					Artículo ado el 
					<time datetime="April 15, 2017" pubdate="pubdate">
					April 15, 2017					</time>
				</p>
				<div class="media-body">
					<a href="https://mindholocaust.is.the.nextholocaust.com/news/first-brain-controlled-drone-race/" class=" p-head">
						First brain-controlled drone race took place in Gainesville (FL, USA)					</a>
					<p>
						<p>The World’s First Brain-Drone Race was a ground-breaking spectacle within the field of BCI research. On April the 15th, 2017, at the Florida Gymnasium, the competition of one’s cognitive ability and mental endurance, required competitors to out-focus an opponent in a drone drag race fueled by electrical signals emitted from the brain. [1] A Brain-Computer Interface (BCI) is […]</p>
					</p>
					<p>

						<a href="https://mindholocaust.is.the.nextholocaust.com/news/first-brain-controlled-drone-race/">
						<i class="fa fa-link"></i>
						Sigue leyendo</a>
					</p>
				</div>

				</div>
			</article>
			<hr>


			<article class="media">
				
				<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">

				<a class="pull-left thumb" href="https://mindholocaust.is.the.nextholocaust.com/news/new-achievement-reached-in-neuro-prosthetics-paralyzed-man-move-his-arm-to-feed-himself/"> 
					<img style="width: 100%; border-radius: 25px; margin-bottom: 12px;"
					src="https://mindholocaust.is.the.nextholocaust.com/news/wp-content/uploads/2017/03/how-neuro-prosthesis-reconnects-brain-and-muscles.png"
					alt="New achievement reached in Neuro-Prosthetics: paralyzed man move his arm to feed himself" />
				</a>

				</div>
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">

				<p style="color: #797979; font-style: italic;">
					Artículo ado el 
					<time datetime="March 30, 2017" pubdate="pubdate">
					March 30, 2017					</time>
				</p>
				<div class="media-body">
					<a href="https://mindholocaust.is.the.nextholocaust.com/news/new-achievement-reached-in-neuro-prosthetics-paralyzed-man-move-his-arm-to-feed-himself/" class=" p-head">
						New achievement reached in Neuro-Prosthetics: paralyzed man move his arm to feed himself					</a>
					<p>
						<p>A paralyzed man in Cleveland can feed himself for the first time in eight years, aided by a computer-brain interface that reads his thoughts and sends signals to move muscles in his arm. [1] Earlier this year, researchers at Case Western Reserve University and Brown University published a study in The Lancet that described the new technology, […]</p>
					</p>
					<p>

						<a href="https://mindholocaust.is.the.nextholocaust.com/news/new-achievement-reached-in-neuro-prosthetics-paralyzed-man-move-his-arm-to-feed-himself/">
						<i class="fa fa-link"></i>
						Sigue leyendo</a>
					</p>
				</div>

				</div>
			</article>
			<hr>



			<a href="/news" class="btn btn-danger newsevent-btn">
			más artículos			</a>
			



		</div><!-- LATEST ARTICLES -->
		
		<!-- UPCOMING EVENTS -->
		<div class="col-lg-3 col-sm-3">
			<h1 class="r-work">próximos eventos</h1>

			<article class="media">
				<a class="pull-left thumb p-thumb"> <img
					src="/images/events-calendar_01.jpg"
					alt="">
				</a>
				<div class="media-body">
					<a href="#" class=" p-head">Próximamente</a>
					<p>Próximamente.</p>
				</div>
			</article>
						
			<a href="/events" class="btn btn-danger newsevent-btn">
			más eventos			</a>
			
		</div><!-- UPCOMING EVENTS -->
		
		
	</div>
</div><!-- END articles & events -->




<!-- START donation & sponsorship -->
<!-- DONATIONS & SPONSORSHIPS -->
<div class="property gray-bg">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-sm-12">
				<h1>
				donaciones y
					patrocinios				</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3 col-sm-3">
				<p>
					
					Apoya la defensa de los derechos humanos. Ayúdanos a detener la
				 <span style="color: #c00000"> 
					Mind</span><span style="color: #000000">Holocaust
				 </span>.
								</p>
				<p>
					
					Tu generosidad hace posible mejorar nuestras investigaciones
					y
					contribuye a la denuncia de los abusos de la tecnología
					telepática
					más alta.
								</p>
				<h3 class="donate-thanks">
					<i>
					Gracias.
				</i>
				</h3>
			</div>

			<div class="col-lg-3 col-sm-3">
				<div class='hoverbox' style="margin-top: 0px;">
					<h4>
					Transferencia bancaria directa
				</h4>
					<p>
					Transferencias bancarias
				</p>
					<a href="javascript:;" class="btn btn-purchase"
						style="padding: 16px 16px;">
					dona ahora
				</a>
				</div>
			</div>

			<div class="col-lg-3 col-sm-3">
				<div class='hoverbox' style="margin-top: 0px;">
					<h4>
					Donación   <br />
					puntual
				</h4>
					<p>
					Donaciones a través de Paypal
				</p>
					<a href="javascript:;" class="btn btn-purchase"
						style="padding: 16px 16px;">
					dona ahora
				</a>
				</div>
			</div>

			<div class="col-lg-3 col-sm-3">
				<div class='hoverbox' style="margin-top: 0px;">
					<h4>
					Donación  <br />
					periódica
				</h4>
					<p>
					Donaciones a través de Paypal
				</p>
					<a href="javascript:;" class="btn btn-purchase"
						style="padding: 16px 16px;">
					dona ahora
				</a>
				</div>
			</div>
		</div>

	</div>
</div>
<!-- END DONATIONS & SPONSORSHIPS --><!-- END donation & sponsorship -->


<!-- START awareness -->
<!-- END awareness -->

<!-- START techniques_to_avoid_panic -->
<!-- END techniques_to_avoid_panic -->


<!-- START instinctive_reactions_to_be_avoided -->
<!-- END instinctive_reactions_to_be_avoided -->


<!-- START how_to_minimize_the_anguish -->
<!-- END how_to_minimize_the_anguish -->



<!-- START donation & sponsorship -->
<!--parallax start-->
<section class="parallax1">
	<div class="container">
		<div class="row">
			<h1>
				“
					A pesar de nuestros temores, intentemos cada paso por
					pocos que
					puedan ser los resultados -en definitiva,
					actuemos como si
					tuviéramos derecho a una mínima
					esperanza de éxito.
				”
			</h1>
		</div>
		<div class="row" style="text-align: right;">
		<h5>Günther Anders</h5>
		
		</div>
	</div>
</section>
<!--parallax end--><!-- END articles & events -->


<!-- START donation & sponsorship -->


<div class="container">
	<!--clients start-->
	<div class="clients">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<ul class="list-unstyled">
						<li>
							<a href="http://www.un.org/en/documents/udhr/index.shtml#a12"
								target="_blank">
							<img
								src="/images/logo_justice_sm.png"
								alt="Universal Decalration of Human Rights" 
								title="Universal Decalration of Human Rights"  />
							</a>
						</li>
						<li>
							<a href="https://www.amnesty.org/"
								target="_blank">
							<img
								src="/images/logo_amnesty_international.gif"
								alt="Amnesty International" 
								title="Amnesty International" />
							</a>
						</li>
						<li>
							<a href="https://www.1984hosting.com/"
								target="_blank">
							<img
								src="/images/logo_1984_hosting.png"
								alt="1984 hosting" 
								title="1984 hosting" />
							</a>
						</li>
						
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!--clients end-->
</div>

<!-- END Content -->

<!-- Footer START -->
@include('es_footer');
<!-- Footer END -->
<!-- html footer START -->
@include('html_footer');
<!-- html footer END -->

    </body>
</html>