<!DOCTYPE html>
<html lang="en">

@include('html_header');

<body>
@include('header');
<!-- es_mind_reading_dossier.blade.php -->

<!-- START Content -->

<!-- Section Header START -->
<div class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-sm-4">
				<h1>awareness</h1>
				<p style="color: #BFBFEF ">
					acknowledge the situation				</p>
			</div>
			<div class="col-lg-8 col-sm-8 navigation">

								
			</div>
		</div>
	</div>
</div>
<!-- Section Header END -->

<div class="container" 
	style="margin-bottom: 50px">

	<div class="row">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
		
			<article class="dossier" 
				title="The Mind Reading Dossier"
				id="awareness-the-mind-reading-dossier">
				<header>
    				<h3>
					
					History of Thought Identification 
					<br /><span class='reduxtext'>
					A dossier about Mind
					Reading, Part I (2006-2015)
					</span>
									</h3>
					<p>
					<span style="font-style: italic;">
					Massimo Opposto
					</span>
					<br />
					<span style="font-size: smaller;">
					September 2015
					</span>
					<br />
					<a 
						target="_blank"
						href="/pdf/Mind-Reading_Dossier_-_en.pdf">
					Download as PDF
					</a>
					</p>
				</header>
				
				
				<section>
					<article>
						<header>
							<h5 style="margin-top: 36px;">
			    				Index 
			    			</h5>
						</header>
					</article>
					<section>
						<article>
						<p>
							<ul style="list-style-type: none;">
								<li style="padding-left: 15px;">
									<a href="#abstract">
									Abstract
									</a>
								</li>
								<li style="padding-left: 15px;">
									<a href="#thoughts-identification">
									1. 
					Thoughts Identification
													</a> 
								</li>
								<li style="padding-left: 15px;">
									<a href="#brain-atlas">
									2. 
					The Brain Atlas.
				 
									</a>
								</li>
								<li style="padding-left: 15px;">
									<a href="#brain-initiative">
									3. 
					The BRAIN initiative
				 
									</a>
								</li>
								<li style="padding-left: 15px;">
									<a href="#conclusions">
									
					Conclusions
				 
									</a>
								</li>
								<li style="padding-left: 15px;">
									<a href="#references">
									
					References
				 
									</a>
								</li>
							</ul>
						</p>
						</article>
					</section>
				</section>
				
				<section 
					title="Abstract" 
					id="abstract">
					<article>
						<header>
							<h5 style="margin-top: 36px; 
								margin-bottom: 24px;">
			    				Abstract 
			    			</h5>
		    			</header>
		    			<section>
			    			<article>
			    				<p>
			    					
					The modern neuroscience is able to read people's mind: the
					process is called "thought identification".
							    				</p>
			    				<p>
			    					
					The process involves brain scanners and
					sophisticated computer
					algorithms.
							    				</p>
			    				<p>
			    					
					The research takes place in advanced reasearch facilities
					like
					Universities and corporation's labs,
					with the help of the biggest IT
					corporations
					and is mainly financed by the government and the
					militaries
					(DARPA above all).
							    				</p>
			    				<p>
			    					
					To fulfill the goal of having an accurate knowledge
					about the
					functioning of the mind, a huge initiative
					has been launched by the
					Obama's administration.
							    				</p>
			    				<p>
			    					
					The official reports about ethical issues that can
					be brought by
					this research are denying the existence
					of issues about the "thought
					identification" process
					and the possibility of process itself.
							    				</p>
			    			</article>
		    			</section>
		    		</article>
		    	</section>
				
				<section 
					title="Thoughts Identification" 
					id="thoughts-identification">
					<article>
						<header>
							<h5 style="margin-top: 36px; margin-bottom: 24px;">
								1.
			    				
					Thoughts Identification
				 
			    				<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_thoughts_identification">
										?
									</a>
								</sup>
			    			</h5>
		    			</header>
						<section>
							<article>
								<p>
									1.1.
									
					Since 2006, the contemporary neuroscience claims that it is
					possilbe to detect lies and covert attitudes using mind
					scanners,
					like fMRI.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_1">
										[1]
									</a>
									</sup>
								</p>
							</article>
						</section>
						<section>
							<article>
								<p>
									1.2. 
									
					In February 2007 a team of scientist at the Max Planck
					Institute for Human Cognitive and Brain Sciences, in
					Germany, "read"
					participants’ intentions out of
					their brain activity. This was made
					possible by a
					new combination of functional magnetic resonance
					imaging and sophisticated computer algorithms.
				 
									<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_2">
										[2]
									</a>
									</sup>
								</p>
							</article>
						</section>
						<section>
							<article>
								<p>
									1.3. 
									
					There are proofs that the outcome of a decision is encoded
					(and
					so detected) in brain activity up to 10 seconds
					before it enters
					awareness.
				 
									<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_3">
										[3]
									</a>
									</sup>
								</p>
							</article>
						</section>
						<section>
							<article>
								<p>
									1.4. 
									
					At the beginning of 2008, it was already possible to detect
					thoughts of familiar objects, by the use of
					mind scanners in
					conjunctions with a computer's algorithm.
				 
									<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_4">
										[4]
									</a>
									</sup>
									<br />
									1.4.1.
									
					The thought of specific objects have been mapped
					to specific
					brain activation patterns by a group of researcher
					at the Carnegie
					Mellon University, Pittsburgh, Unites States.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_4">
										[4]
									</a>
									</sup>
									<br />
									1.4.2.
									
					The same researchers claimed that there is a commonality
					in how
					different people's brains represent the same thought.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_4">
										[4]
									</a>
									</sup>
								</p>
							</article>
						</section>
						<section>
							<article>
								<p>
									1.5.
									
					Since 2009, neuroscientists in the United States are
					cataloguing brain patterns to match up with actual words,
					sentences
					and intentions.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_5">
										[5]
									</a>
									</sup>
									<br />
									1.5.1.
									
					John-Dylan Haynes, of the Max Planck Institute, explains,
					"The
					new realization is that every thought is associated
					with a pattern
					of brain activity and you can train a
					computer to recognize the
					pattern associated with a
					particular thought."
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_6">
										[6]
									</a>
									</sup>
								</p>
							</article>
						</section>
						<section>
							<article>
								<p>
									1.6.
									
					The Intel Lab at Pittsburg works in partnership with the
					Brain
					Image Analysis Research Group at Carnegie Mellon
					University to
					identify the brain's thought patterns
					through fMRI technology.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_7">
										[7]
									</a>
									</sup>
									<br />
									1.6.1.
									
					The Intel Lab at Pittsburg is part of Intel Research division,
					that was created in 2000, under the leadership of
					David L.
					Tennenhouse. Tennenhouse aimed to model
					his new research
					organization based on DARPA, where
					he had previously been director
					of the Information
					Technology Office.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_8">
										[8]
									</a>
									</sup>
								</p>
							</article>
						</section>				
						<section>
							<article>
								<p>
									1.7.
									
					In 2010 the DARPA’s budget for the fiscal year
					included $4
					million to start up a program
					called Silent Talk. The goal was to
					“allow
					user-to-user communication on the battlefield
					without the use
					of vocalized speech through
					analysis of neural signals”.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_9">
										[9]
									</a>
									</sup>
									<br />
									1.7.1.
									
					Before being vocalized, speech exists as word-specific
					neural
					signals in the mind.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_10">
										[10]
									</a>
									</sup>
									<br />
									1.7.2.
									
					Darpa wants to develop technology that would detect
					these
					signals of “pre-speech,” analyze them, and
					then transmit the
					statement to an intended interlocutor.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_9">
										[9]
									</a>
									</sup>
								</p>
							</article>
						</section>
						<section>
							<article>
								<p>
									1.8
									
					In 2011, due to the "zero-shot learning method", by
					utilizing
					semantic knowledge mined from large
					text corpora and crowd-sourced
					humans, it has
					been shown that training images of brain activity
					are
					not required for every word, in order to be
					recognized: it is
					possible to predict words
					(identify thoughts) that people are
					thinking about,
					from functional magnetic resonance images (fMRI)
					of
					their neural activity, even without training
					examples for those
					words.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_11">
										[11]
									</a>
									</sup>
								</p>
							</article>
						</section>
						<section>
							<article>
								<p>
									1.9.
									
					On September 2011, the 22nd a group of researcher from Berkelay University published an article about "Reconstructing visual experiences from brain activity evoked by natural movies".
					Using functional Magnetic Resonance Imaging (fMRI) and computational models, UC Berkeley researchers have succeeded in decoding and reconstructing people’s dynamic visual experiences – in this case, watching Hollywood movie trailers.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_11_1">
										[12]
									</a>
									</sup>
									<br />
									1.9.1.
									
					A student from the same group had the same succesfull results applying the same technique to the audio experience, on Jannuary 2012, the 31th.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_11_2">
										[13]
									</a>
								</p>
							</article>
						</section>
					</article>
				</section>
					
				<section
					title="Thoughts Identification" 
					id="brain-atlas"><!-- The sub-section (Brain Atlas) START -->
					<article><!-- The sub-article (Brain Atlas) START -->
						<header>
							<h5 style="margin-top: 36px;">
								2.
			    				
					The Brain Atlas.
				 
			    				<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_aibs">
										?
									</a>
								</sup>
			    			</h5>
						</header>
						<section>
							<article>
								<p>
									2.1
									
					In 2003 Paul G. Allen, co-founder of Microsoft in 1975,
					founded
					the Allen Institute for Brain Science (AIBS),
					supporting it with 41$
					million (to date: $500 million).
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_12">
										[14]
									</a>
									</sup>
								</p>
							</article>
						</section>
						<section>
							<article>
								<p>
									2.2
									
					The inaugural project of the Allen Institute is the
					Allen Mouse
					Brain Atlas, compelted the 26th September 2006:
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_13">
										[15]
									</a>
									</sup>
									<br />
									2.2.1.
									
					The Allen Mouse Brain Atlas is a gene expression map for the
					mouse (and human brain, as
					people and mice share the 90% of brain
					genes) that, as well
					as functional imaging techniques, permits
					researchers
					to correlate between gene expression, cell types,
					and
					pathway function in relation to behaviors (or phenotypes).
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_14">
										[16]
									</a>
									</sup>
								</p>
							</article>
						</section>
						<section>
							<article>
								<p>
									2.3.
									
					On May 24, 2010, the Allen Institute announced it
					was expanding
					its Atlas from the mouse into the
					human brain with the launch of the
					Allen Human Brain Atlas.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_15">
										[17]
									</a>
									</sup>
								</p>
							</article>
						</section>
					</article><!-- The sub-article (ATLAS) END -->
				</section><!-- The sub-section (ATLAS) END -->
				<section
					title="Thoughts Identification" 
					id="brain-initiative"><!-- The sub-section (BRAIN) START -->
					<article><!-- The sub-article (BRAIN) START -->
						<header>
							<h5 style="margin-top: 36px;">
								3.
			    				
					The BRAIN initiative
				 
			    				<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_brain_initiative">
										?
									</a>
								</sup>
			    			</h5>
						</header>
						<section>
							<article>
								<p>
									3.1.
									
					On April 2, 2013, the US president Barack Obama unveiled
					the
					“BRAIN” Initiative (Brain Research through
					Advancing Innovative
					Neurotechnologies): a collaborative,
					public-private research, with
					the goal of supporting
					the development and application of innovative
					technologies
					that can create a dynamic understanding of brain
					function.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_16">
										[18]
									</a>
									</sup>
									<br />
									3.1.1.
									
					In other words, the aim is to produce the first map of
					brain
					function to explore every signal sent by every
					cell and track how
					the resulting data flows through
					neural networks and is ultimately
					translated into thoughts,
					feelings and actions: to have a
					computational model of the
					human brain.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_17">
										[19]
									</a>
									</sup>
									<br />
									3.1.2.
									
					The BRAIN Initiative has been developed by
					the White House
					Office of Science and Technology Policy (OSTP),
					with proposed
					initial expenditures for fiscal year 2014 of
					approximately $100
					million: $50 million from
					the Defense Advanced Research Projects
					Agency (DARPA),
					$40 million from the National Institutes of Health
					(NIH),
					and $20 million from the National Science Foundation (NSF).
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_18">
										[20]
									</a>
									</sup>
									<br />
									3.1.3.
									
					Private sector partners also have made important commitments
					to
					support the BRAIN Initiative, including: $60 millions
					from the Allen
					Institute for Brain Science and $30 millions
					from the Howard Hughes
					Medical Institute.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_17">
										[19]
									</a>
									</sup>
									<br />
									3.1.4.
									
					On September 30, 2014, the Obama administration announced
					two
					more federal agencies were partecipating
					the BRAIN Initiative: the
					Food and Drug Administration (FDA)
					and Intelligence Advanced
					Research Projects Activity (IARPA).
					The President’s FY15 Budget
					proposed to double the Federal
					investment in the BRAIN Initiative
					form $100 millions
					to $200 millions.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_18">
										[20]
									</a>
									</sup>
								</p>
							</article>
						</section>
						<section>
							<article>
								<p>
									3.2.
									
					In 2015, a number of companies, foundations,
					patient advocacy
					organizations, universities,
					and private research institutions are
					making
					investments and announced commitments to align
					more than $270
					million in research and development
					efforts with the goals of the
					BRAIN Initiative.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_19">
										[21]
									</a>
									</sup>
								</p>
							</article>
						</section>
						<section>
							<article>
								<p>
									3.3.
									
					On January, 17, 2013, a meeting was held at the California
					Institute of Technology was attended by the three government
					agencies DARPA, NIH and NSF, as well as neuroscientists,
					nanoscientists and representatives from Google, Microsoft
					and
					Qualcomm. According to a summary of the meeting,
					it was held to
					determine whether computing facilities
					existed to capture and
					analyze the vast amounts of data
					that would come from the project.
					The scientists and
					technologists concluded that they did.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_20">
										[22]
									</a>
									</sup>
								</p>
							</article>
						</section>
						<section>
							<article>
								<p>
									3.4.
									
					In April 4, 2013 Qualcomm Inc. announced to took part
					in the BRAIN
					Initiatives and unveiled it has been quietly
					working at the
					frontiers of neuroscience since 2009 at 
            		&#8220; Brain
					Corp.&#8221; 
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_21">
										[23]
									</a>
									</sup>
									
					an independent venture that Qualcomm has kept mostly
					under
					wraps.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_22">
										[24]
									</a>
									</sup>
									<br />
									3.4.1.
									
					Founded in 2009, Brain Corp. set out to develop
					radically
					different computer systems and software,
					based on algorithms that
					emulate the “spiking neuron”
					processes of the human brain.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_22">
										[24]
									</a>
									</sup>
									<br />
									3.4.2.
									
					In 2010, DARPA, provided an undisclosed amount of
					funding to
					Brain Corp. “to design an artificial
					nervous system for UAVs”
					(unmanned aerial vehicles).
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_23">
										[25]
									</a>
									</sup>
									<br />
									3.4.3.
									
					On Feb 13, 2012 Todd Hylton joined Brain Corp. as a
					top
					executive last year, after resigning from DARPA,
					where he spent
					nearly five years as a program manager.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_24">
										[26]
									</a>
									</sup>
								</p>
							</article>
						</section>
						<section>
							<article>
								<p>
									3.5.
									
					On September 30, 2014 it has been announced that
					Google
					engineers are building tools and developing
					infrastructure to
					analyze petabyte scale datasets
					generated by the BRAIN Initiative
					and the neuroscience
					community to better understand the brain’s
					computational
					circuitry and the neural basis for human cognition.
					Google is working closely with the Allen Institute
					for Brain Science
					to develop scalable computational
					solutions to advance scientific
					understanding of the brain.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_19">
										[21]
									</a>
									</sup>
								</p>
							</article>
						</section>
						<section>
							<article>
								<p>
									3.6.
									
					On June 21, 2012, on the #74 of Neuron, one of the most
					influential and relied upon journals in the field of
					neuroscience,
					some ethical considerations about the
					BRAIN initiative (former Brain
					Activity Map Project) are pointed
					out: amongst them,
					issues of
					"mind-control".
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_25">
										[27]
									</a>
									</sup>
									<br />
									3.6.1.
									
					On April 2, 2013, the Office of the Press Secretary
					informed
					that the DARPA will engage a broad range of
					experts to explore the
					ethical, legal, and societal
					issues raised by advances in
					neurotechnology.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_16">
										[18]
									</a>
									</sup>
									<br />
									3.6.2.
									
					On March 2015, the US Presidential Commission for the
					study of
					Bioethical Issues published the second volume
					of the Bioethics
					Commission's two-part response to
					President Obama’s request related
					BRAIN Initiative.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_26">
										[28]
									</a>
									</sup>
									<br />
									3.6.2.1
									
					In the commission's report, the "mind-control" issue and the
					concept itself, are re-shaped in the term of
					"neural modifiers", to
					refer to a wider array of
					mechanisms of brain and nervous system
					change,
					and ignored in its peculiarity.
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_26">
										[28]
									</a>
									</sup>
									<br />
									3.6.2.2
									
					In the commission's report is stated that
					"protecting mental
					privacy is a forward-looking
					concern that neuroscientists and legal
					decision
					makers might need to evaluate as technology
					continues to
					advance."
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_26">
										[28]
									</a>
									</sup>
									<br />
									3.6.2.3
									
					The commission's report denys that neuroscience's achievements
					could lead to "mind-reading". It is stated that "today,
					and in the
					foreseeable future, neuroscience does
					not enable us to read minds.
					Technology remains
					extremely limited and cannot reveal the true
					inner
					desires, psychological states, or motivations that
					are worthy
					of the term mind-reading."
													<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_26">
										[28]
									</a>
									</sup>
								</p>
							</article>
						</section>
					</article><!-- The sub-article (BRAIN) END -->
				</section><!-- The sub-section (BRAIN) END -->
				<section
				title="Thoughts Identification" 
					id="conclusions"><!-- The sub-section (Conclusion) START -->
					<article><!-- The sub-article (Conclusion) START -->
					
						<header>
							<h5 style="margin-top: 36px;">
								
					Conclusions
											</h5>
						</header>
						<section>
							<article>
								<p>
								
					It is absurd to deny the problems of integrity of
					privacy caused by 
            	 
            		<a href="#awareness-the-mind-reading-dossier"
            			 title="the mind reading dossier">
            	
					the achievements of computational neuroscience.
            	 </a> 
												</p>
								<p>
								
					The fact that to deny this problem, it is precisely
					the
					Bio-Ethics Presidential Committee of the US
					largely financed and
					controlled by DARPA, it is dramatic.
												</p>
								<p>
								
					The world's governments are deeply influenced by
					the
					military-industrial-scientific complex that is
					breaking the
					integrity of the privacy of mind.
												</p>
								<p>
								
					Actually, this powerful political-financial complex conceives
					the internal affairs as a matter of military-strategic
					warfare.
												</p>
								<p>
								
					This contemporary polical framework is
					characterized by a  &#8220;fourth
					generation warfare
					(4GW)&#8221; 
												<sup>
									<a 
										href="javascript:void(0);"
										data-tag="HTMLPopoverTrigger"
										data-targetPopoverId="awareness_4GW">
										?
									</a>
								</sup>
								
					carried on the states's internal
					front, i.e. the citizens, in
					order to
					Win hearts and minds of its own population.
												</p>
								<p>
								
					Throughout contemporary history the psychological warfare
					has
					become highly sophisticated, relying on the scientific
					research
					achievements, amongst them, the mind-reading
					technology.
												</p>
								<p>
								
					This scenario highlights that civil rights and social
					justice
					supporters must defend the privacy of thought
					from being
					threatened
					by the abuse of the mind-reading technology.
												</p>
							</article>
						</section>
					</article><!-- The sub-article (Conclusion) END -->
				</section><!-- The sub-section (Conclusion) END -->
				
				
				<footer 
					id="references">
					<h5
						style="margin-top: 36px;">
					References
					</h5>
    				
    				<!-- Reference #0 START-->
    					<div 
    						style="display: none;"
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_thoughts_identification">
    						<div data-tag="HTMLPopoverTitle">
    							Thoughts Identification
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Thought identification refers to the 
	    							empirically verified use of technology to, 
	    							in some sense, read people's minds. 
	    							Advances in research have made this 
	    							possible by using human neuroimaging 
	    							to decode a person's conscious experience 
	    							based on non-invasive measurements of an 
	    							individual's brain activity.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
	    							From Wikipedia, the free encyclopedia
    							</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="https://en.wikipedia.org/wiki/Thought_identification"
										title='Thought identification'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #0 END-->
    				<!-- Reference #1 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_1">
    						<div
    							data-tag="HTMLPopoverTitle">
    							Reference #1
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
	    							Prof. Dr. John-Dylan Haynes <br />
	    							(Max Planck Institute for Cognitive and Brain Sciences)
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Decoding mental states from brain 
	    							activity in humans.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
	    							in Nature Reviews Neuroscience #7, <br />
	    							pag. 523-534,<br />
	    							July 2006.
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/John-Dylan_Haynes_-_Decoding_mental_states_from_brain_activity_in_humans.pdf"
										title='Decoding mental states from brain activity in humans'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://www.nature.com/nrn/journal/v7/n7/abs/nrn1931.html"
										title='Decoding mental states from brain activity in humans'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #1 END -->
    				<!-- Reference #2 START -->
    					<div 
    						
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_2">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #2
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
	    							Prof. Dr. John-Dylan Haynes <br />
	    							(Max Planck Institute for Cognitive and Brain Sciences)
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Revealing secret intentions in 
	    							the brain.
									Scientists decode concealed intentions 
									from human brain activity.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
	    							in Max Planck Institute 
	    							for Human Cognitive and Brain Sciences
	    							website. <br />
	    							February 08, 2007
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/John-Dylan_Haynes_-_Research_news_-_2007_-_Revealing_Secret_Intention.pdf"
										title='Revealing secret intentions in the brain.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://www.mpg.de/550068/pressRelease20070206"
										title='Revealing secret intentions in the brain.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #2 END -->
    				<!-- Reference #3 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_3">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #3
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
	    							Chun Siong Soon, Marcel Brass, 
	    							Hans-Jochen Heinze, John-Dylan Haynes.
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Unconscious determinants of free 
	    							decisions in the human brain.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
	    							in Nature Neuroscience. <br />
	    							April 13, 2008
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/John-Dylan_Haynes_and_others_-_NatureNeuroScience_-_Unconscious_determinants_of_free_decisions_in_the_human_brain.pdf"
										title='Unconscious determinants of free decisions in the human brain.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://www.nature.com/neuro/journal/v11/n5/full/nn.2112.html"
										title='Unconscious determinants of free decisions in the human brain.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #3 END -->
    				<!-- Reference #4 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_4">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #4
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
	    							Carnegie Mellon University Press.
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Carnegie Mellon Study Identifies 
	    							Where Thoughts Of Familiar Objects 
	    							Occur Inside the Human Brain.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
	    							in Carnegie Mellon University website. <br />
	    							Jannuary 3, 2008
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Carnegie_Mellon_Study_Identifies_Where_Thoughts_Of_Familiar_Object_Occur_Inside_the_Human_Brain.pdf"
										title='Carnegie Mellon Study Identifies Where Thoughts Of Familiar Objects Occur Inside the Human Brain.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="https://www.cmu.edu/news/archive/2008/January/jan3_justmitchell.shtml"
										title='Carnegie Mellon Study Identifies Where Thoughts Of Familiar Objects Occur Inside the Human Brain.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #4 END -->
    				<!-- Reference #5 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_5">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #5
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
	    							Michael_Johnson
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Watch what you think.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								International Herald Tribune.<br />
	    							march 3, 2009. <br />
	    							hosted at the Center for Cognitive Brain Imaging of
	    							the Carnegie Mellon University
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Michael_Johnson_-_Watch_what_you_think_-_InternationalHeraldTribune.pdf"
										title='Watch what you think.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://www.ccbi.cmu.edu/news/InternationalHeraldTribune.pdf"
										title='Watch what you think.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #4 END -->
    				<!-- Reference #6 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_6">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #6
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
	    							John-Dylan Haynes quoted in
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Watch what you think.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
	    							by Sharon Begley on Newsweek, <br />
	    							Jennuary 12, 2008. <br />
	    							(Also in John-Dylan Haynes. Brain reading. 
	    							Oxford University Press. 2012)
	    							
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/John-Dylan_Haynes_-_Mind_Reading_Is_Now_Possible_-_Newsweek.pdf"
										title='Begley: Mind Reading Is Now Possible.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://www.newsweek.com/begley-mind-reading-now-possible-86567"
										title='Begley: Mind Reading Is Now Possible.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #4 END -->
					<!-- Reference #7 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_7">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #7
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
	    							Marcel Just and Tom Mitchell
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Thought Reading Demonstration.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
	    							in Carnegie Mellon University youtube channel. <br />
	    							Jennuary 8, 2009
	    							
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/video/Marcel_Just_and_Tom_Mitchell_-_Thought_Reading_Demonstration_-_CMU.webm"
										title='Thought Reading Demonstration.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://www.youtube.com/watch?v=JVLu5_hvr8s"
										title='Watch what you think.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #7 END -->
					<!-- Reference #8 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_8">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #8
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
	    							
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Intel Research Lablets.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								from Wikipedia, the free encyclopedia
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Intel_Research_Lablets_-_Wikipedia_the_free_encyclopedia.webm"
										title='Thought Reading Demonstration.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="https://en.wikipedia.org/wiki/Intel_Research_Lablets"
										title='Watch what you think.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #8 END -->
    				<!-- Reference #9 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_9">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #9
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
	    							The Defense Advanced Research Projects Agency (DARPA).
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Fiscal Year 2010 Budget Estimates.&#8221;
	    							
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								pag. 12, <br />
    								May 2009. 
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Department_of_Defense_Fiscal_Year_2010_Budget_Estimates.pdf"
										title='Thought Reading Demonstration.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://www.darpa.mil/attachments/%282G7%29%20Global%20Nav%20-%20About%20Us%20-%20Budget%20-%20Budget%20Entries%20-%20FY2010%20%28Approved%29.pdf"
										title='Watch what you think.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #9 END -->
    				<!-- Reference #10 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_10">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #10
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
	    							Schmidek and Sweet.
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Operative Neurosurgical Techniques.&#8221;
	    							
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								pag. 1372, <br />
    								Ed. Elsevier Inc. <br />
    								2012 
    							</div>
    							
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Schmidek_Sweet_-_Operative_Neurosurgical_Techniques.pdf"
										title='Thought Reading Demonstration.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="https://books.google.es/books?id=4sKQSY-zdvgC&pg=PA1372&lpg=PA1372&dq=speech+exists+as+word-specific+neural+signals+in+the+mind&source=bl&ots=eXw21MDrlX&sig=-NvyQAhD8sj-1WwIe232176Hm0w&hl=en&sa=X&ved=0CCgQ6AEwAWoVChMIl9Td_OWUyAIVx1AUCh36vQRn#v=onepage&q&f=false"
										title='Operative Neurosurgical Techniques.'
										target="_blank">
										external link
									</a>
								</div>
								
    						</div>
    					</div>
    				<!-- Reference #10 END -->
    				<!-- Reference #11 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_11">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #11
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
	    							Mark M. Palatucci.
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Thought Recognition: Predicting and Decoding.
	    							Brain Activity Using the Zero-Shot Learning Model.&#8221;
	    							
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								pag. iv, <br />
    								Carnegie Mellon University <br />
    								Dissertations, paper 65.  <br />
    								March 25, 2011.
    							</div>
    							
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Thought_Recognition_Predicting_and_Decoding_Brain_Activity_Using_the_zero_shot_learning_model.pdf"
										title='Thought Recognition: Predicting and Decoding. Brain Activity Using the Zero-Shot Learning Model.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://repository.cmu.edu/cgi/viewcontent.cgi?article=1058&context=dissertations"
										title='Thought Recognition: Predicting and Decoding. Brain Activity Using the Zero-Shot Learning Model.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #11 END -->
    				<!-- Reference #11_1 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_11_1">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #12
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
	    							Jack L. Gallant and al.
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Reconstructing visual experiences from brain activity evoked by natural movies.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								UC Berkeley<br />
    								US National Library of Medicine National Institutes of Health<br />
    								September 22, 2011.
    							</div>
    							
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Jack_L._Gallant_-_Reconstructing_visual_experiences_from_brain_activity_evoked_by_natural_movies.pdf"
										title='Jack L. Gallan: Reconstructing visual experiences from brain activity evoked by natural movies'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3326357/pdf/nihms319652.pdf"
										title='Jack L. Gallan: Reconstructing visual experiences from brain activity evoked by natural movies.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #11_1 END -->
    				<!-- Reference #11_2 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_11_2">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #13
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
	    							Brian N. Pasley and al.
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Reconstructing Speech from Human Auditory Cortex.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								UC Berkeley<br />
    								PLOS Biology<br />
    								January the 31st, 2012.
    							</div>
    							
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Brian_N._Pasley_-_Reconstructing_Speech_from_Human_Auditory_Cortex.pdf"
										title='Brian N. Pasley: Reconstructing Speech from Human Auditory Cortex'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://journals.plos.org/plosbiology/article/file?id=10.1371/journal.pbio.1001251&type=printable"
										title='Brian N. Pasley: Reconstructing Speech from Human Auditory Cortex.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #11_1 END -->
    				<!-- Thoughts Identification END -->
    				<!-- Reference The Brain Atlas START -->
    					<div 
    						style="display: none;"
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_aibs">
    						<div data-tag="HTMLPopoverTitle">
    							The Brain Atlas
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;The Allen Mouse and Human Brain 
	    							Atlases are projects within the Allen 
	    							Institute for Brain Science which seek 
	    							to combine genomics with neuroanatomy by 
	    							creating gene expression maps for the 
	    							mouse and human brain.&#8221;
	    							
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								From Wikipedia, the free encyclopedia
    							</div>
    							<!-- 
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Thought_Recognition_Predicting_and_Decoding_Brain_Activity_Using_the_zero_shot_learning_model.pdf"
										title='Thought Recognition: Predicting and Decoding. Brain Activity Using the Zero-Shot Learning Model.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
							 	-->
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="https://en.wikipedia.org/wiki/Allen_Brain_Atlas"
										title='Allen Brain Atlas'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference The Brain Atlas END -->
    				<!-- Reference #12 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_12">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #14
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Allen Insitute for Brain Science, Founders&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								Allen Institute for Brain Science website.
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Allen_Institute_for_Brain_Science_-_Founders.pdf"
										title='Allen Insitute for Brain Science, Founders.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://www.alleninstitute.org/our-institute/founders/"
										title='Allen Insitute for Brain Science, Founders.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #12 END -->
					<!-- Reference #13 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_13">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #15
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Allen Institute for Brain Science 
	    							completes brain atlas&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								Allen Institute for Brain Science website. <br />
    								September 26, 2006.
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Allen_Institute_for_Brain_Science_-_The_Allen_Institute_for_Brain_Science_Completes_the_Brain_Atlas.pdf"
										title='Allen Institute for Brain Science completes brain atlas.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://www.alleninstitute.org/news-events/press/press-release/allen-institute-brain-science-completes-brain-atlas"
										title='Allen Institute for Brain Science completes brain atlas.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #13 END -->
    				<!-- Reference #14 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_14">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #16
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Allen Brain Atlas, Contributions to neuroscience.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								From Wikipedia, the free encyclopedia.
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Allen_Brain_Atlas_-_Wikipedia_the_free_encyclopedia.pdf"
										title='Allen Brain Atlas, Contributions to neuroscience.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="https://en.wikipedia.org/wiki/Allen_Brain_Atlas#Contributions_to_neuroscience"
										title='Allen Brain Atlas, Contributions to neuroscience.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #14 END -->
    				<!-- Reference #15 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_15">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #17
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Allen Institute for Brain Science launches 
	    							Allen Human Brain Atlas with first data set charting genes at 
	    							work in the adult human brain.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								Allen Institute for Brain Science website. <br />
    								May 24, 2010
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Allen_Institute_for_Brain_Science_-_Allen_Institute_for_Brain_Science_launches_Allen_Human_Brain_Atlas_with_first_data_set_charting_genes_at_work_in_the_adult_human_brain.pdf"
										title='Allen Institute for Brain Science launches Allen Human Brain Atlas.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://alleninstitute.org/news-events/press/press-release/allen-institute-brain-science-launches-allen-human-brain-atlas-first-data-set-charting-genes-work-ad"
										title='Allen Institute for Brain Science launches Allen Human Brain Atlas.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #15 END -->
    				<!-- Reference Brain Initiative START -->
    					<div 
    						style="display: none;"
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_brain_initiative">
    						<div data-tag="HTMLPopoverTitle">
    							The Brain Initiative
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;The White House BRAIN Initiative 
	    							(Brain Research through Advancing Innovative 
	    							Neurotechnologies), is a collaborative, 
	    							public-private research initiative announced 
	    							by the Obama administration on April 2, 2013,
	    							with the goal of supporting the development 
	    							and application of innovative technologies 
	    							that can create a dynamic understanding of 
	    							brain function.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								From Wikipedia, the free encyclopedia.
    							</div>
    							<!--  
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Allen_Institute_for_Brain_Science_-_Allen_Institute_for_Brain_Science_launches_Allen_Human_Brain_Atlas_with_first_data_set_charting_genes_at_work_in_the_adult_human_brain.pdf"
										title='Allen Institute for Brain Science launches Allen Human Brain Atlas.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								-->
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://alleninstitute.org/news-events/press/press-release/allen-institute-brain-science-launches-allen-human-brain-atlas-first-data-set-charting-genes-work-ad"
										title='Allen Institute for Brain Science launches Allen Human Brain Atlas.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference Brain Initiative END -->
    				<!-- Reference #16 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_16">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #18
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							The White House, Office of the Press Secretary.
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Fact Sheet: BRAIN Initiative.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								The White House website <br />
    								April 02, 2013
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/The_White_House_Office_of_the_Press_Secretary_-_Fact_Sheet_BRAIN_Initiative.pdf"
										title='Fact Sheet: BRAIN Initiative.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="https://www.whitehouse.gov/the-press-office/2013/04/02/fact-sheet-brain-initiative"
										title='Fact Sheet: BRAIN Initiative.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #16 END -->
    				<!-- Reference #17 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_17">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #19
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							Maia Szalavitz
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Brain Map: President Obama Proposes 
	    							First Detailed Guide of Human Brain Function.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								Time <br />
    								February 19, 2013
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Maia_Szalavitz_-_Brain_Map_President_Obama_Proposes_First_Detailed_Guide_of_Human_Brain_Function.pdf"
										title='Brain Map: President Obama Proposes First Detailed Guide of Human Brain Function.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://healthland.time.com/2013/02/19/brain-map-president-obama-proposes-first-detailed-guide-of-human-brain-function/"
										title='Brain Map: President Obama Proposes First Detailed Guide of Human Brain Function.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #17 END -->
    				<!-- Reference #18 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_18">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #20
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							Francis Collins (Director of the National Institutes of Health) 
    							and Arati Prabhakar (Director of Defense Advanced Research Projects Agency).
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;BRAIN Initiative Challenges Researchers 
	    							to Unlock Mysteries of Human Mind.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								White House Blog <br />
    								April 2, 2013
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Francis_Collins_and_Arati_Prabhakar_-_BRAIN_Initiative_Challenges_Researchers_to_Unlock_Mysteries_of_Human_Mind.pdf"
										title='BRAIN Initiative Challenges Researchers to Unlock Mysteries of Human Mind.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="https://www.whitehouse.gov/blog/2013/04/02/brain-initiative-challenges-researchers-unlock-mysteries-human-mind"
										title='BRAIN Initiative Challenges Researchers to Unlock Mysteries of Human Mind.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #18 END -->
    				<!-- Reference #19 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_19">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #21
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Fact Sheet: Over $300 Million in 
	    							Support of the President’s BRAIN Initiative.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								The White House website <br />
    								April 2, 2013
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Fact_Sheet_Over_300_Million_in_Support_of_the_President_s_BRAIN_Initiative.pdf"
										title='Fact Sheet: Over $300 Million in Support of the President’s BRAIN Initiative.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="https://www.whitehouse.gov/sites/default/files/microsites/ostp/brain_fact_sheet_9_30_2014_final.pdf"
										title='Fact Sheet: Over $300 Million in Support of the President’s BRAIN Initiative.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #19 END -->
    				<!-- Reference #20 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_20">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #22
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							John Markoff
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Obama Seeking to Boost Study of Human Brain.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								New York Times <br />
    								February 17, 2013
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/John_Markoff_-_Obama_Seeking_to_Boost_Study_of_Human_Brain.pdf"
										title='Obama Seeking to Boost Study of Human Brain.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://www.nytimes.com/2013/02/18/science/project-seeks-to-build-map-of-human-brain.html"
										title='Obama Seeking to Boost Study of Human Brain.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #20 END -->
    				<!-- Reference #21 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_21">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #23
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							Michael Copeland (Qualcomm Sr. Manager, Marketing)
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;The Government’s New BRAIN Initiative: 
	    							The Next Manhattan Project?&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								Qualcomm website <br />
    								April 22, 2013.
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/The_Government_s_New_BRAIN_Initiative_The_Next_Manhattan_Project.pdf"
										title='Obama Seeking to Boost Study of Human Brain.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="https://www.qualcomm.com/news/spark/2013/04/22/governments-new-brain-initiative-next-manhattan-project"
										title='Obama Seeking to Boost Study of Human Brain.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #21 END -->
    				<!-- Reference #22 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_22">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #24
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							Michael Copeland (Qualcomm Sr. Manager, Marketing)
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Qualcomm Started Computing Like a Neuron Years Ago&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								Qualcomm website <br />
    								March 4, 2013
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Bruce_V_Bigelow_-_With_Brain_Corp._Qualcomm_Started_Computing_Like_a_Neuron_Years_Ago.pdf"
										title='Qualcomm Started Computing Like a Neuron Years Ago'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://www.xconomy.com/san-diego/2013/04/03/qualcomm-started-its-brain-initiative-years-before-president-obama/"
										title='Qualcomm Started Computing Like a Neuron Years Ago'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #22 END -->
    				<!-- Reference #23 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_23">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #25
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;DARPA funds Brain Corporation.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								Brain Corporation website <br />
    								November 1, 2010. 
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/DARPA_funds_Brain_Corporation.pdf"
										title='DARPA funds Brain Corporation.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://www.braincorporation.com/darpa-funds-brain-corporation/"
										title='DARPA funds Brain Corporation.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #23 END -->
    				<!-- Reference #24 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_24">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #26
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Dr. Todd Hylton joins Brain Corporation.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								Brain Corporation website <br />
    								February 12, 2013. 
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/Dr_Todd_Hylton_joins_Brain_Corporation.pdf"
										title='Dr. Todd Hylton joins Brain Corporation.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://www.braincorporation.com/dr-todd-hylton-joins-brain-corporation/"
										title='Dr. Todd Hylton joins Brain Corporation.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #24 END -->
    				<!-- Reference #25 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_25">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #27
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							A. Paul Alivisatos, Miyoung Chun, George M. Church, 
    							Ralph J. Greenspan, Michael L. Roukes, and Rafael Yuste
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;The Brain Activity Map Project and 
	    							the Challenge of Functional Connectomics.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								Neuron #74, 
    								pag. 973, <br />
    								Cell press. Elsevier Inc. <br />
    								June 21, 2012.
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/The_Brain_Activity_Map_Project_and_Challenge_of_Functional_Connectocomics.pdf"
										title='The Brain Activity Map Project and the Challenge of Functional Connectomics.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://arep.med.harvard.edu/pdf/Alivisatos_BAM_12.pdf"
										title='The Brain Activity Map Project and the Challenge of Functional Connectomics.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #25 END -->
    				<!-- Reference #26 START -->
    					<div 
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_26">
    						<div data-tag="HTMLPopoverTitle">
    							Reference #28
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							Presidential Commission for the study of Bioethical Issues.
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Gray Matters: Topics at the Intersection 
	    							of Neuroscience, Ethics, and Society.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								Presidential Commission for the study of 
    								Bioethical Issues website. <br />
    								Pag 28. <br />
    								March 2015<br />
    								
    							</div>
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/The_Brain_Activity_Map_Project_and_Challenge_of_Functional_Connectocomics"
										title='Gray Matters: Topics at the Intersection of Neuroscience, Ethics, and Society.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="http://bioethics.gov/sites/default/files/GrayMatter_V2_508.pdf"
										title='Gray Matters: Topics at the Intersection of Neuroscience, Ethics, and Society.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #26 END -->
    				<!-- Reference #4GW START -->
    					<div 
    						style="display: none;"
    						data-tag="HTMLPopover"
    						data-popoverId="awareness_4GW">
    						<div data-tag="HTMLPopoverTitle">
    							 Fourth Generation Warfare
    						</div>
    						<div data-tag="HTMLPopoverContent">
    							<div class="popoverReferenceAuthor">
    							</div>
    							<div class="popoverReferenceSourceTitle">
	    							&#8220;Fourth-generation warfare (4GW) 
	    							is conflict characterized by a blurring 
	    							of the lines between war and politics, 
	    							combatants and civilians.&#8221;
    							</div>
    							<div class="popoverReferenceSourceDetails">
    								From Wikipedia, the free encyclopedia
    								
    							</div>
    							<!--
    							<div class='popoverReferenceInternalLink'>
	    							<a 
										class='popoverReferenceInternalLink'
										href="/pdf/The_Brain_Activity_Map_Project_and_Challenge_of_Functional_Connectocomics"
										title='Gray Matters: Topics at the Intersection of Neuroscience, Ethics, and Society.'
										target="_blank">
										<i class="fa fa-link "></i> 
										hosted document
									</a>
								</div>
								-->
								<div class='popoverReferenceExternalLink'>
									<a 
										class='popoverReferenceExternalLink external'
										href="https://en.wikipedia.org/wiki/Fourth-generation_warfare"
										title='Fourth-generation warfare.'
										target="_blank">
										external link
									</a>
								</div>
    						</div>
    					</div>
    				<!-- Reference #26 END -->

  				</footer>
			</article>
		
		</div>
		<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12" >
		
			
		
		</div>
	</div>
	
	
</div>
<!-- dossier partial END -->        
<!-- END Content -->

<!-- footer START -->
@include('footer');
<!-- footer END -->
<!-- html footer START -->
@include('html_footer');
<!-- html footer END -->

    </body>
</html>