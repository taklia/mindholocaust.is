<!DOCTYPE html>
<html lang="es">

@include('html_header');

<body>
@include('es_header')
<!-- es_about.blade.php -->

<!-- START Content -->

<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-sm-4">
				<h1>
					Acerca de
				</h1>
				<p style="color: #BFBFEF">
					Quién, qué, cuándo, dónde, por qué
				</p>
			</div>
			<div class="col-lg-8 col-sm-8 navigation">
				<a href="/">MindHolocaust</a> &nbsp; &gt; &nbsp; 
					Acerca de
			</div>
		</div>
	</div>
</div>


<div class="container">
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-4">
			<img style="width: 100%; margin-bottom: 20px;"
				src="/images/holocaust_memorial_628x442.jpg">
		</div>
		<div class="col-lg-6 col-md-6 col-sm-8 about">
		
			<article>
				<header>
					<h3>
					Bienvenido a MindHolocaust
				</h3>
				</header>
				<p>
				
					MindHolocaust es un  
				<b>sitio
					web</b>
					y una
				<b>organización</b>
					enfocada a las
				<i style="color: #C00000;">
					tecnologías de acondicionamiento de la mente</i>.
								</p>
				<p>
					
					La web de MindHolocaust está aquí para informar a la
					gente
					acerca de los abusos realizados por estas tecnologías
					y para
					conectar a personas que están interesadas en hacer
					algo contra estos
					abusos.
								</p>
				<p>
					
					La organización MindHolocaust tiene como objetivos:
								</p>
					<ul class="about">
						<li>
							
					investigar y exponer los hechos para denunciar públicamente
					los
					abusos de las tecnologías de acondicionamiento
					de la mente
					averiguando cuándo y dónde accurren los abusos;
										</li>
						<li>
							
					apoyar a las víctimas de las tecnologías
					de acondicionamiento de
					la mente;
										</li>
						<li>
							
					presionar a los gobiernos, exigiéndoles que promulguen
					leyes
					contra las tecnologías de acondicionamiento de la mente;
										</li>
						<li>
							
					investigar cómo estas tecnologías puedan ser
					reveladas y
					neutralizadas;
											</li>
					</ul>
				<p>
					
					MindHolocaust acaba de ser lanzado (junio de 2015)
					y por el
					momento es una iniciativa privada.
								</p>
			</article>
		</div>
	</div>
</div>

<div class="gray-box">
	<div class="container">
		<div class="row text-center">
			<div class="text-center feature-head">
				<h1>
					Conoce al Fundador
				</h1>
				<p>
					&#8220;
					La falta de imaginación es imperdonable
				&#8222;
				</p>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-lg-offset-3 col-md-offset-3 col-sm-offset-3">
				<div class="person text-center">
					<img alt="" src="/images/founder.jpg">
				</div>
				<div class="person-info text-center">
					<h4>
						<a href="javascript:;">
						Massimo						Opposto						</a>
					</h4>
					<p class="text-muted">
					El fundador de MindHolocaust
				</p>
					<div class="team-social-link">
						<a href="/es/contact"><i class="fa fa-at"></i></a>
						<a href="https://independent.academia.edu/MassimoOpposto">
							<i class="fa fa-graduation-cap"></i>
						</a>
					</div>
					<p>
						
					Educación
										<br /> 
						
					Graduado en Filosofía de la Inteligencia Artificial.
										<br />
						
					Universidad de Bolonia (Italia)
										<br />
					</p>
					<p>
						
					Se gana la vida como:
										<br />
						
					Aplicador web / desarrollador de servicios,
					Arquitecto Software,
					Analista.
				 
					</p>
				</div>
				<div class="person-info" style="text-align: justify;">
					<p>
						Massimo						Opposto, 
						
					nacido en Brescia (Italia), en 1975, es un
					filósofo, programador
					web y activista político.
										<br />
						
					A la edad de cuatro años, sus padres se mudaron a un
					pequeño
					pueblo, cerca de la ciudad de Bolonia (Italia).
										<br />
						
						
					Durante los últimos años de los '90, Massimo participó en
					algunas
					actividades políticas en la ciudad de Bolonia,
					dentro del movimiento
				
				<a href="http://error.410" target="_blank">
				
					Rekombiannt</a>:
					un movimiento cultural simbólico-deconstruccionista inspirado
					por
					Franco Bifo Berardi con algunas influencias de
				
				<a href="https://www.adbusters.org/"
				target="_blank">
				
					adbuster.org</a>.
					El movimiento Rekombinant adoptó las técnicas de 
				
				<a href="https://en.wikipedia.org/wiki/Culture_jamming"
				target="_blank">
				
					culturejamming/guerrilla de la comunicación
				
				</a>
				
					aplicándola a la crítica social y política.
										
						<br />
						
					En el 2000 Massimo se unió a un grupo de art-ivismo para la
					defensa
					de los derechos de privacidad,
					llamado <a href="http://www.notbored.org/the-scp.html"
				target="_blank">actores
					de las cámaras de vigilancias (SCP)</a>".
					Fundó un
				<a href="http://ricerca.repubblica.it/repubblica/archivio/repubblica/2002/12/19/un-pennarello-per-battere-le-telecamere.html" 
					target="_blank">
					grupo SCP local que hizo su performance en las calles de Bolonia</a>
					durante el 2001/2002.
					Hay 
				<a href="/pdf/Matteo_Pasquinelli_-_ES_-_Media_Activism_SCP.pdf" 
					target="_blank">
					un artículo 
				</a>
					que habla de esto, en el libro de
				<a href="http://www.deriveapprodi.org/2002/09/media-activism/"
					target="_blank">
					Media-Activismo
				</a>,
					de 
				<a href="http://matteopasquinelli.com/" target="_blank">
					Matteo Pasquinelli
				</a>
					publicado por DeriveApprodi.
										<br />
						
					En 2004 se graduó en filosofía en la Universidad de Bolonia,
					con una tesis sobre la inteligencia artificial.
										<br />
						
					En el 2007 se trasladó a Barcelona, España, donde llevó
					a cabo
					algunas investigaciones sobre la privacidad digital,
					ha estado  <i>hackctivo</i>
					en línea ...
										<br />
						
					En el 2013 descubrió los abusos de la tecnología telepática
					y
					decidió fundar la iniciativa MindHolocaust. Actualmente
					trabaja en
					un libro sobre los efectos y los abusos de la
					tecnología telepática.
										<br />
						
					También está colaborando con 
				<a href="http://revistacombate.com/" target="_blank">
					Combate</a>,
					una revista digital de periodismo humanístico.
									</p>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END Content -->

<!-- Footer START -->
@include('es_footer');
@include('html_footer');
<!-- Footer END -->

    </body>
</html>