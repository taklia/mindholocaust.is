<!DOCTYPE html>
<html lang="en">

@include('html_header');

<body>
@include('header');
<!-- es_metropia_it_is_hard_to_talk_about_it.blade.php -->

<!-- START Content -->

<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-sm-4">
				<h1>
					Obras de arte
				</h1>
				<p style="color: #BFBFEF">
					Un imaginario sobre la tecnología telepática.
				</p>
			</div>
			<div class="col-lg-8 col-sm-8 navigation">
				<a href="/">MindHolocaust</a> &nbsp; &gt; &nbsp; 
        <a href="/artworks">
					obras de arte
				</a> &nbsp; &gt; &nbsp; metropia - es difícil hablar de esto
      </div>
		</div>
	</div>
</div>

<div class="property gray-bg">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-sm-12">

			<video class="video-clip" 
				controls="controls" 
				autoplay="autoplay">
				<!-- 
				<source 
					src="/public/video/metropia_-_hard_to_talk_about.ogv" 
					type="video/ogg" 
					media="all" />
				 -->
				<source 
					src="/video/metropia_-_hard_to_talk_about.webm" 
					type="video/webm" 
					media="all" />
				<source 
					src="/video/metropia_-_hard_to_talk_about.mp4" 
					type="video/mp4" 
					media="all" />
			</video>
			
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<h3>Metropia: es difícil hablar de esto.</h3>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">	
				<p>
					Metropia es una película sobre una distopía impulsada por
					las empresas en un mundo en el cual las corporaciones
					pueden leer
					los pensamientos de la gente y entregar
					información a la mente de la
					gente...
				</p>
				<p>
					Este videoclip muestra cuán violento puede ser hablar
					de este fenómeno.
				</p>
				<p style="font-style: italic;">
					Copyright © 2009 por Atmo. Todos Los Derechos
					Reservados.
					<br />
					Se cree que el uso de un videoclip parcial de escala
					reducida y de baja resolución se califica como uso justo.
			  </p>
			</div>	
		</div>
		
	</div>
</div>

<!-- END Content -->

<!-- footer START -->
@include('es_footer');
<!-- footer END -->
<!-- html footer START -->
@include('html_footer');
<!-- html footer END -->


    </body>
</html>