<!DOCTYPE html>
<html lang="es">

@include('html_header');

<body>
@include('es_header')

<!-- START Content -->

<div class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-sm-4">
				<h1>videos</h1>
				<p style="color: #BFBFEF ">
					
					nuestros videos son algo que ver
								</p>
			</div>
			<div class="col-lg-8 col-sm-8 navigation">

								
			</div>
		</div>
	</div>
</div>


<div class="container">

	
	<div class="row" style="margin-bottom: 60px;">
		<div class="col-lg-3 col-md-6 col-sm-6  text-right">
			<a href="/videos/watch/v/Science_Bytes_-_Decoding_Our_Senses">
		
			<img style="width: 263px; margin-bottom: 10px; margin-top: 60px;"
				alt="" src="/images/thumbnail-Science_Bytes_-_Decoding_Our_Senses.png" />
				
			</a>
		</div>
		<div class="col-lg-9 col-md-6 col-sm-6">
			<h3>
					Descodificando nuestros sentidos
				</h3>
			<p>
					Nuestros sentidos, nuestras experiencias audiovisuales, pueden ser decodificados y reconstruidos usando fMRI y los algoritmos informáticos científicos demandados a finales de 2011 a partir de 2012.
				</p>
			<p>
					Un corto documental sobre la descondificaciòn experiencias audiovisuales producido en la serie "Bytes de la Ciencia", financiado por Alfred P. Sloan Foundation.
				</p>

			<a class="btn btn-purchase"
				href="/videos/watch/v/Science_Bytes_-_Decoding_Our_Senses">
				
					Míralo
								</a>
		</div>
	</div>
	
	
	<div class="row" style="margin-bottom: 60px;">
		<div class="col-lg-3 col-md-6 col-sm-6  text-right">
			<a href="/videos/watch/v/CBS_Reading_Your_Mind">
		
			<img style="width: 263px; margin-bottom: 10px; margin-top: 60px;"
				alt="" src="/images/thumbnail-CBS_Reading_Your_Mind.png" />
				
			</a>
		</div>
		<div class="col-lg-9 col-md-6 col-sm-6">
			<h3>
					La
					lectura de tu mente
				</h3>
			<p>
					el documental de la CBS acerca de
					la técnica de la neurociencia
					llamada
					&#8220;identificación
					del pensamiento &#8221;
				</p>
			<p>
					La neurociencia ha aprendido mucho acerca de la
					actividad del
					cerebro y su relación con ciertos pensamientos. 
					 <br /> 
					Como
					informa Lesley Stahl, que ahora puede ser posible, en un nivel
					básico, de leer la mente de una persona.
				</p>

			<a class="btn btn-purchase"
				href="/videos/watch/v/CBS_Reading_Your_Mind">
				
					Míralo
								</a>
		</div>
	</div>
	
	
	<div class="row" style="margin-bottom: 60px;">
		<div class="col-lg-3 col-md-6 col-sm-6  text-right">
			<a href="/videos/watch/v/John-Dylan_Haynes_-_Mind_reading_with_brain_scanners">
		
			<img style="width: 263px; margin-bottom: 10px; margin-top: 60px;"
				alt="" src="/images/thumbnail-John-Dylan_Haynes_-_Mind_reading_with_brain_scanners.png" />
				
			</a>
		</div>
		<div class="col-lg-9 col-md-6 col-sm-6">
			<h3>
					John-Dylan Haynes: Lectura de la mente con escáneres
					cerebrales
				</h3>
			<p>
					Un TEDx Talk sobre Mind Reading
				</p>
			<p>
					La investigación de John-Dylan Haynes se centra en los
					mecanismos neurales que subyacen a los procesos cognitivos humanos.
					Sus intereses especiales son los fundamentos técnicos y éticos de
					decodificación del estado mental, así como la neurociencia de la
					conciencia, las intenciones y el libre albedrío.
				</p>

			<a class="btn btn-purchase"
				href="/videos/watch/v/John-Dylan_Haynes_-_Mind_reading_with_brain_scanners">
				
					Míralo
								</a>
		</div>
	</div>
	
</div>

<!-- END Content -->

<!-- Footer START -->
@include('es_footer');
@include('html_footer');
<!-- Footer END -->

    </body>
</html>