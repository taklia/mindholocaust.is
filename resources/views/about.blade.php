<!DOCTYPE html>
<html lang="en">

@include('html_header');

<body>
@include('header');
<!-- about.blade.php -->

<!-- START Content -->

<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-sm-4">
				<h1>
					About
				</h1>
				<p style="color: #BFBFEF">
					Who, what, when, where, why.
				</p>
			</div>
			<div class="col-lg-8 col-sm-8 navigation">
				<a href="/">MindHolocaust</a> &nbsp; &gt; &nbsp; 
					About
			</div>
		</div>
	</div>
</div>


<div class="container">
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-4">
			<img style="width: 100%; margin-bottom: 20px;"
				src="/images/holocaust_memorial_628x442.jpg">
		</div>
		<div class="col-lg-6 col-md-6 col-sm-8 about">
		
			<article>
				<header>
					<h3>
					Welcome to MindHolocaust
				</h3>
				</header>
				<p>
				
					MindHolocaust is a 
				<b>website</b> 
					and an 
				<b>organization</b> 
					focused on 
				<i style="color: #C00000;">
					Mind Conditioning Technologies</i>.
								</p>
				<p>
					
					MindHolocaust website is here to inform
					people about the abuses
					made by these technologies and to connect
					together people that are
					interested in doing something about
					these abuses.
								</p>
				<p>
					
					MindHolocaust organization aims to:
								</p>
					<ul class="about">
						<li>
							
					to investigate and expose the facts, to publicly
					denounce the
					abuses of Mind Conditioning Technologies,
					whenever and wherever
					abuses happen;
										</li>
						<li>
							
					to support the victims of Mind Conditioning Technologies;
										</li>
						<li>
							
					to lobby governments, asking to promulgate laws
					against Mind
					Conditioning Technologies abuses;
										</li>
						<li>
							
					to investigate how these technologies can be
					revealed and
					neutralized;
											</li>
					</ul>
				<p>
					
					MindHolocaust has just been launched (June 2015) and at
					the
					moment is a private's initiative.
								</p>
			</article>
		</div>
	</div>
</div>

<div class="gray-box">
	<div class="container">
		<div class="row text-center">
			<div class="text-center feature-head">
				<h1>
					Meet the Founder
				</h1>
				<p>
					&#8220;
					The lack of imagination is unforgivable.
				&#8222;
				</p>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-lg-offset-3 col-md-offset-3 col-sm-offset-3">
				<div class="person text-center">
					<img alt="" src="/images/founder.jpg">
				</div>
				<div class="person-info text-center">
					<h4>
						<a href="javascript:;">
						Massimo						Opposto						</a>
					</h4>
					<p class="text-muted">
					MindHolocaust's founder
				</p>
					<div class="team-social-link">
						<a href="/contact"><i class="fa fa-at"></i></a>
						<a href="https://independent.academia.edu/MassimoOpposto">
							<i class="fa fa-graduation-cap"></i>
						</a>
					</div>
					<p>
						
					Education
										<br /> 
						
					Philosophy degree on Artificial Intelligence.
										<br />
						
					Bologna University (Italy)
										<br />
					</p>
					<p>
						
					Making money as:
										<br />
						
					Web Application/Services Developer, Architect, Analyst.
				 
					</p>
				</div>
				<div class="person-info" style="text-align: justify;">
					<p>
						Massimo						Opposto, 
						
					born in Brescia (Italy), in 1975, is a
					philosopher, web
					programmer and political activist.
										<br />
						
					At the age of four, his parents moved to a small
					town, close to
					the city of Bologna (Italy).
										<br />
						
						
					During the lately 90's, he took part to
					some political activities in
					Bologna city,
					within the 
				
				<a href="http://error.410" target="_blank">
				
					Rekombiannt
				
				</a> 
				
					movement:
					a symbolic-deconstructionist cultural movement
					inspired by
					Franco Bifo Berardi and having some influences
					from 
				
				<a href="https://www.adbusters.org/"
				target="_blank">
				
					adbuster.org
				</a>.
					The Rekombinant movement was adopting the 
				
				<a href="https://en.wikipedia.org/wiki/Culture_jamming"
				target="_blank">
				
					culturejamming/guerrilla communication
				
				</a>
				
					techniques, applied to social and political criticism.
										
						<br />
						
					In 2000 he joined an art-ivist group for the defence
					of privacy's
					rights,
					called "<a href="http://www.notbored.org/the-scp.html"
				target="_blank">survellance
					camera players (SCP)</a>".
					He founded a 
				<a href="http://ricerca.repubblica.it/repubblica/archivio/repubblica/2002/12/19/un-pennarello-per-battere-le-telecamere.html" 
					target="_blank">
					local SCP-group that was performing in Bologna's streets</a>
					during 2001/2002. 
				<a href="/pdf/Matteo_Pasquinelli_-_ES_-_Media_Activism_SCP.pdf" 
					target="_blank">
					An article 
				</a>
					about these facts is included in the book 
				<a href="http://www.deriveapprodi.org/2002/09/media-activism/"
					target="_blank">
					Media Activism
				</a>,
					by 
				<a href="http://matteopasquinelli.com/" target="_blank">
					Matteo Pasquinelli
				</a>
					published by DeriveApprodi.
					
										<br />
						
					In 2004 he graduated in philosophy at the Bologna's university,
					debating a thesis on artificial intelligence.
										<br />
						
					In 2007 he moved to Barcelona,
					Spain, where he conducted some
					researches about digital privacy,
					while he has being 
				<i>hackctive</i>
					online...
										<br />
						
					In 2013 he discovered the abuses of the telepathic technology
					and so he decided to found the MindHolocaust initiative.
					Currently
					he is working on a book about the effects and the
					abuses of the
					telepathic technology.
										<br />
						
					He is also collaborating with 
				<a href="http://revistacombate.com/" target="_blank">
					Combate</a>,
					a humanistic online magazine.
									</p>
				</div>
			</div>
		</div>
	</div>
</div>
	
<!-- END Content -->

<!-- END Content -->

<!-- footer START -->
@include('footer');
<!-- footer END -->
<!-- html footer START -->
@include('html_footer');
<!-- html footer END -->

    </body>
</html>