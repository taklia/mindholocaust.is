<!DOCTYPE html>
<html lang="es">

@include('html_header');

<body>
@include('es_header')
<!-- es_contact.blade.php -->

<!-- START Content -->

<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-sm-4">
				<h1>
					contacto
				</h1>
				<p style="color: #BFBFEF">
					Siéntete libre de contactarnos.
				</p>
			</div>
			<div class="col-lg-8 col-sm-8 navigation">
				<a href="/">MindHolocaust</a> &nbsp; &gt; &nbsp; 
					contacto
			</div>
		</div>
	</div>
</div>


<div class="container">
	<div class="row">
		<div class="col-lg-7 col-sm-7 address">
			<h4>
					Enviar un mensaje
				</h4>
			<div class="contact-form">
				<form 
					role="form" 
					name="contactForm"
					action="/contact"
					method="post">
					<div class="form-group">
						<label for="name">
						
					Nombre
				</label> 
						<input type="text"
							class="form-control" 
							name="formContactName" 
							placeholder="Winston Smith" />
						<div class="formError-text"></div>
					</div>
					<div class="form-group">
						<label for="email">
						
					Email
				</label> 
						<input type="text"
							class="form-control" 
							name="formContactEmail" 
							placeholder="email@domain.org" />
						<div class="formError-text"></div>
					</div>
					<div class="form-group">
						<label for="phone">
						
					Teléfono
										</label> 
						<input type="text"
							class="form-control" 
							name="formContactPhone" 
							placeholder="0037 45658878451" />
						<div class="formError-text"></div>
					</div>
					<div class="form-group">
						<label for="phone">
						
					Mensaje
										</label>
						<textarea 
							class="form-control" 
							name="formContactMessage" 
							rows="5" 
							placeholder="Your message here."></textarea>
						<div class="formError-text"></div>
					</div>
					<button type="submit" class="btn btn-danger">
					
					Enviar
									</button>
				</form>

			</div>
		</div>
		
		<div class="col-lg-5 col-md-5 col-sm-5 address">

			<div class="f-box-static"
				style="padding-bottom: 20px; 
					margin-bottom: 40px; 
					min-height: 0; 
					border: solid 1px #BDBDBD;
					color: #7e7e7e;">
				<h4>
					Nota sobre  <br /> la
					Privacidad y la Seguridad
								</h4>
				<p>
					
					Nuestros servidores son “amigos de la privacidad”.
									<br /> 
					
					Nosotros NO utilizamos los servicios de Google NI
					cualquier otro
					servicio de espionaje de datos.
				 
					<br /> 
					
					Tus correos electrónicos son leídos solo por nosotros.
				 
					<br /> 
					<b>
					
					Tu privacidad digital es preservada, al menos por nuestra
					parte.
				 
						</b>
				</p>
			</div>


			<h4>
					Otras formas de contactar con nosotros.
				</h4>
			<p>
				<b>
					teléfono
				</b> 
				<br /> 
				<span class="muted"> 
					+46 72 868 1516 
				</span> 
				<br />  <br />
				<b>
					Dirección de correo electrónico
				</b> <br /> 
				<span class="muted">
					info@nextholocaust.com 
				</span> 
				<br /> <br />
				<b>
					dirección física
				</b> <br /> 
				<span
					class="muted"> 
					NextHolocaust <br />
					Kronborgsvägen, N. 13B<br /> 
					21742, Malmö, Sverige 
				</span>
			</p>
		</div>		

	</div>
</div>



<!-- Modal -->
<div 
	class="modal fade" 
	id="modalFormContact" 
	tabindex="-1" 
	role="dialog"
	aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">
					
					Correo enviado.
								</h4>
			</div>
			<div class="modal-body">
					Tu correo será atendido lo más pronto posible.
				</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<!-- 
				<button type="button" class="btn btn-primary">Save changes</button>
				 -->
			</div>
		</div>
	</div>
</div>

<!-- END Content -->

<!-- Footer START -->
@include('es_footer');
<!-- footer END -->
<!-- html footer START -->
@include('html_footer');
<!-- html footer END -->

    </body>
</html>