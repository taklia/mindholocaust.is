<!DOCTYPE html>
<html lang="es">

@include('html_header');

<body>
@include('es_header')
<!-- es_metropia_the_main_idea.blade.php -->

<!-- START Content -->

<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-sm-4">
				<h1>
					Obras de arte
				</h1>
				<p style="color: #BFBFEF">
					Un imaginario sobre la tecnología telepática.
        </p>
			</div>
			<div class="col-lg-8 col-sm-8 navigation">
				<a href="/es/welcome">MindHolocaust</a> &nbsp; &gt; &nbsp; 
        <a href="/artworks">
					obras de arte
				</a> &nbsp; &gt; &nbsp; metropia: la idea principal
      </div>
		</div>
	</div>
</div>

<div class="property gray-bg">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-sm-12">

			<video class="video-clip" 
				controls="controls" 
				autoplay="autoplay">
				<!-- 
				<source 
					src="/video/metropia_-_the_main_idea.ogv" 
					type="video/ogg" 
					media="all" />
				 -->
				<source 
					src="/video/metropia_-_the_main_idea.webm" 
					type="video/webm" 
					media="all" />
				<source 
					src="/video/metropia_-_the_main_idea.mp4" 
					type="video/mp4" 
					media="all" />
			</video>
			
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<h3>Metropia: La idea principal.</h3>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">	
			
				<p>
					Metropia es una película sobre una distopía impulsada por
					las empresas en un mundo en el cual las corporaciones
					pueden leer
					los pensamientos de la gente y entregar
					información a la mente de la
					gente...
				</p>
				<p>
					Este videoclip es un extracto a partir de una reseña
					sobre Metropia: aquí se explica la idea principal de la película.
				</p>
				<p style="font-style: italic;">
					
					Copyright © 2009 por Atmo. Reservados Todos Los
					Derechos.
									<br />
					
					Se cree que el uso de un videoclip parcial de escala
					reducida y de baja resolución se califica como uso justo.
								</p>
				
			</div>	
		</div>
		
	</div>
</div>
<!-- END Content -->

<!-- footer START -->
@include('es_footer');
<!-- footer END -->
<!-- html footer START -->
@include('html_footer');
<!-- html footer END -->

    </body>
</html>