<!-- es_header.blade.php -->
<!-- header start -->
<header class="header-frontend">
	<div class="navbar navbar-default navbar-static-top">
		<div class="container">
			<div class="navbar-header">
				<button 
					type="button" 
					class="navbar-toggle" 
					data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="icon-bar"></span> 
					<span class="icon-bar"></span> 
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="http://nextholocaust.com">
					Next<span>Holocaust</span>
				</a>
				<br />
				<span class="header-tagline-text">
					observatorio de distopía				</span>
			</div>

			<!-- START navigation -->
			<div class="navbar-collapse collapse ">
				<ul class="nav navbar-nav">
					<li class=" active ">
						<a href="/es/welcome" style="font-weight: bold;">MindHolocaust</a>
					</li>
					<li  class="">
						<a href="/es/about">Acerca de</a>
					</li>
					<li class="dropdown ">
						<a 
							class="dropdown-toggle"
							data-close-others="false"
							data-delay="0"
							data-hover="dropdown"
							data-toggle="dropdown"
							href="#">
							Herramientas							<b class="fa fa-caret-down"></b>
						</a>
						<ul class="dropdown-menu">
							
							<li class="">
								<a href="/es/awareness">Conocimiento</a>
							</li>
							<li class="">
								<a href="https://news.mindholocaust.is">Noticias</a>
							</li>
							<!--
							<li class="">
								<a href="/events">Eventos</a>
							</li>
							-->
							<li class="">
								<a href="/es/artworks">Obras de arte</a>
							</li>
							<!--
							<li class="">
								<a href="/meetup">Meetup</a>
							</li>
							<li class="">
								<a href="/channels">Canales</a>
							</li>
							<li class="">
								<a href="/biennal">Bienal</a>
							</li>
							-->
						</ul>
					</li>
					<li class="">
						<a href="/es/contact">Contacto</a>
					</li>

					<li class="dropdown">
						<a 
							class="dropdown-toggle"
							data-close-others="false"
							data-delay="0"
							data-hover="dropdown"
							data-toggle="dropdown"
							href="#">
							Idiomas
							<b class="fa fa-caret-down"></b>
						</a>
						<ul class="dropdown-menu">
							<li class="">
								<?php
									$path = Request::path();
									$path = substr($path, 2);
								?>
								@if ($lang === 'en' )
									<a href="/es{{$path}}">Español</a>
								@elseif ($lang === 'es' )
									<a href="/en{{$path}}">English</a>
								@endif
							</li>
						</ul>
					</li>
				</ul>
			</div>
			<!-- END navigation -->

		</div>
	</div>
</header>
