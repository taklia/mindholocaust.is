<head>
    <meta charset="utf-8" />

    <title>NextHolocaust: dystopia observatory</title>

    <meta name="description"
        content="MindHolocaust is a website and an organization focused on Mind Conditioning Technologies." />
    <meta name="author"
        content="taklia.com" />
    <meta name="keywords"
        content="nextholocaust , next holocaust, NH,
            mindholocaust , mind holocaust ,

            telepathic technology ,
            synthetic telepathy ,
            mind conditioning technologies ,
            mind reading ,
            thought reading ,
            mind surveillance ,
            mind control ,
            voice to skull ,
            hearing voices ,
            hearing subliminal messages ,
            human rights ,
            subliminal conditioning ,
            thought identification ,
            remote neural monitoring ,
            "
        />
    <!--
        @TODO favicon
    <link rel="shortcut icon" href="img/favicon.png">
    -->


    <!-- Mobile first: to ensure proper rendering and touch zooming -->
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- Bootstrap core CSS -->
    <link
        rel="stylesheet"
        href="/flatlab/css/bootstrap.min.css" />
    <link
        rel="stylesheet"
        href="/flatlab/css/theme.css" />
    <link
        rel="stylesheet"
        href="/flatlab/css/bootstrap-reset.css" />
    <!--external css-->
    <link
        rel="stylesheet"
        href="/font-awesome-4.7.0/css/font-awesome.css" />
    <link rel="stylesheet"
        href="/flatlab/css/flexslider.css" />
    <link
        rel="stylesheet"
        href="/flatlab/assets/bxslider/jquery.bxslider.css"  />
    <link
        rel="stylesheet"
        href="/flatlab/assets/fancybox/source/jquery.fancybox.css" />

    <link
        rel="stylesheet"
        media="screen"
        href="/flatlab/assets/revolution_slider/css/rs-style.css" />
    <link
        rel="stylesheet"
        media="screen"
        href="/flatlab/assets/revolution_slider/rs-plugin/css/settings.css" />

    <!-- Custom styles for this template -->
    <link
        rel="stylesheet"
        href="/flatlab/css/style.css" />
    <link
        rel="stylesheet"
        href="/flatlab/css/style-responsive.css"  />




    <!-- Custom styles for mind holocaust -->
    <link
        rel="stylesheet"
        href="/css/nextholocaust.css" />

</head>