<!DOCTYPE html>
<html lang="en">

@include('html_header');

<body>
@include('header');
<!-- welcome.blade.php -->

<!-- START Content -->

<!-- revolution slider start -->
<div class="fullwidthbanner-container main-slider">
	<div class="fullwidthabnner">
		<ul id="revolutionul" style="display: none;">
			<!-- 1st slide -->
						<li data-transition="fade" 
				data-slotamount="8" 
				data-masterspeed="300"
				data-delay="15000" 
				data-thumb="">
				<!-- Image background on first slide -->
				<img
					src="/images/brain_lens_1900x590.jpg"
				alt="">
				
				
				<div class="caption lft slide_subtitle dark-text" 
					data-x="790"
					data-y="60"
					data-speed="400" 
					data-start="800"
					data-easing="easeOutExpo">
					are
					thoughts  
            	 <br /> 
					absolutly private?
				 
				</div>	
				
				<div class="caption lfr slide_subtitle dark-almost-text" 
					data-x="790"
					data-y="180" 
					data-speed="400" 
					data-start="1500"
					data-easing="easeOutExpo">
					is
					mind-reading possible  
            	 <br /> 
					due to technology?
				 
				</div>
				
				
				
				<div class="caption lfr slide_title" 
					data-x="26" 
					data-y="70"
					data-speed="400" 
					data-start="3400" 
					data-easing="easeOutExpo">
					
					mind-reading 
				 <br /> 
					technology
					is here.
				 
				</div>
				

				<div class="caption lfr slide_subtitle darker-text" 
					data-x="26"
					data-y="200" 
					data-speed="400" 
					data-start="4000"
					data-easing="easeOutExpo">
					
					The privacy's integrity
				 <br /> 
					of your mind is broken.
				 
				</div>
				
				<div class="caption lfr slide_desc bolder-text darkish-text" 
					data-x="26" 
					data-y="340"
					data-speed="400" 
					data-start="5400" 
					data-easing="easeOutExpo">
					
					Modern neuroscience can do that.
									<br />
					
				 <a href="https://en.wikipedia.org/wiki/Thought_identification"
					class="blue-link" target="_blank"> 
					"Thought identification"
				 </a> 
					is the name 
				 <br /> 
					of such technique.
									
				</div> 
				
				<a class="caption lfr btn yellow slide_btn"
					href="/awareness" 
					data-x="26"
					data-y="450" 
					data-speed="400" 
					data-start="6000"
					data-easing="easeOutExpo"> 
					Read more 
				</a>

			</li>
			
			
			<!-- 2nd slider -->
			<li data-transition="fade" 
				data-slotamount="8" 
				data-masterspeed="300"
				data-delay="15000" 
				data-thumb="">
				<!-- Image background on first slide -->
				<img
					src="/images/neuroslave_1900x590_01.jpg"
				alt="">
				
				<!-- 
				<div class="caption lfl slide_item_left" 
					data-x="140" 
					data-y="100"
					data-speed="400" 
					data-start="1000" 
					data-easing="easeOutBack">
					<img
						src="/images/waves_to_the_brain_voice_to_skull_360x326_2.png"
						alt="Image 1">
				</div>
				-->
				
				<div class="caption lft slide_subtitle dark-text" 
					data-x="26"
					data-y="60"
					data-speed="400" 
					data-start="800"
					data-easing="easeOutExpo">
					is your mind online?				</div>	
				
				<div class="caption lfr slide_subtitle dark-almost-text" 
					data-x="26"
					data-y="160" 
					data-speed="400" 
					data-start="2000"
					data-easing="easeOutExpo">
					can
					your thought be 
				 <br /> 
					spied remotely?
								</div>
				
				
				
				<div class="caption lfr slide_title" 
					data-x="740" 
					data-y="70"
					data-speed="400" 
					data-start="2500" 
					data-easing="easeOutExpo">
					A
					new technology  
            	 <br /> is
					available
									<!-- and is changing our reality.  -->
				</div>
				

				<div class="caption lfr slide_subtitle darker-text" 
					data-x="740"
					data-y="200" 
					data-speed="400" 
					data-start="3500"
					data-easing="easeOutExpo">
					<!-- 
					Everything has changed.  
					<br />
					-->
					Be
					aware of it. 
            	 <br /> Be
					ready.
								</div>
				
				<div class="caption lfr slide_desc bolder-text" 
					data-x="740" 
					data-y="290"
					data-speed="400" 
					data-start="5000" 
					data-easing="easeOutExpo">
					Discover what's going on.					<br />
					Do not panic: you are not alone.					<br />
					Share your knowledge and skills.					<br />
					Help and be helped.				</div> 
				
				<a class="caption lfr btn yellow slide_btn"
					href="#welcome" 
					data-x="740"
					data-y="450" 
					data-speed="400" 
					data-start="5500"
					data-easing="easeOutExpo"> 
					Read more 
				</a>

			</li>

			
			
			<!-- 3rd Slide START -->
			<li data-transition="fade" data-slotamount="7" data-masterspeed="300"
				data-delay="25000" data-thumb=""><img
				src="/images/subliminal_audio_message_1900x590_01.jpg"
				alt="">
				<div 
					class="caption lfr slide_title slide_item_left yellow-text"
					data-x="670" 
					data-y="125" 
					data-speed="400" 
					data-start="500"
					data-easing="easeOutExpo">
					
					subliminal audio messages
								</div>
				<div class="caption lfr slide_subtitle slide_item_left slide_title bolder-text yellow-text" 
					data-x="670"
					data-y="180" 
					data-speed="400" 
					data-start="1000"
					data-easing="easeOutExpo">
					
					recognize them
								</div>
				
				<div 
					class="caption lfr slide_desc slide_item_left bolder-text" 
					data-x="670"
					data-y="240" 
					data-speed="400" 
					data-start="2000"
					data-easing="easeOutExpo">
					
					If you pay attention, you will recognize 
					 <br /> 
					you are surrounded
					by subliminal audio messages.
								</div>
				
				<div 
					class="caption lfr slide_desc slide_item_left bolder-text" 
					data-x="670"
					data-y="310" 
					data-speed="400" 
					data-start="3000"
					data-easing="easeOutExpo">
					
					Subliminal audio messages are used to supersede 
					 <br /> 
					your inner voice, your original thoughts, 
					 <br /> 
					to suggest
					you what to think, as they were thought by you.
								</div>
				
				<a class="caption lft slide_btn btn red slide_item_left"
					href="#welcome"  
					data-x="670" 
					data-y="450" 
					data-speed="400"
					data-start="4000" 
					data-easing="easeOutExpo"> 
					Read more 
				</a>
			</li>
			<!-- 3rd Slide END -->
			
			
			<!-- 4nd slide  -->
			<li 
				data-transition="fade" 
				data-slotamount="8" 
				data-masterspeed="700"
				data-delay="15000" 
				data-thumb="">
				<!-- THE MAIN IMAGE IN THE Second SLIDE --> 
				<img
					src="/images/mind_reading_mind_programming_1900x590low_2.jpg"
				alt="">
				
				<div class="caption lft slide_subtitle dark-almost-text" 
					data-x="10"
					data-y="60"
					data-speed="400" 
					data-start="1000"
					data-easing="easeOutExpo">
					The
					terrible discovery 
            	 <br /> of
					present life:
								</div>				
				
				<div class="caption lft slide_title" 
					data-x="10" 
					data-y="150"
					data-speed="400" 
					data-start="1800" 
					data-easing="easeOutExpo">
					Thoughts
					can be read
            	 <br /> and
					“suggested”.
								</div>

				<div class="caption lft slide_desc bolder-text dark-almost-text" 
					data-x="10"
					data-y="282" 
					data-speed="400" 
					data-start="2500"
					data-easing="easeOutExpo">
					“The powers that be” have the technology...				</div> 
				<div class="caption lft slide_desc bolder-text dark-almost-text" 
					data-x="10"
					data-y="306" 
					data-speed="400" 
					data-start="2800"
					data-easing="easeOutExpo">
					...and they are using it.				</div> 
				
				<a class="caption lft slide_btn btn red slide_item_left"
					href="#welcome" 
					data-x="10" 
					data-y="400" 
					data-speed="400"
					data-start="7000" 
					data-easing="easeOutExpo"> 
					Read more 
				</a>
				<div 
					class="caption lft start" 
					data-x="640" 
					data-y="55"
					data-speed="400" 
					data-start="4000" 
					data-easing="easeOutBack">
					
					<img
						src="/images/human_body_hacked_02.png"
						alt="man">
					
				</div>
				<div class="caption lft slide_subtitle dark-almost-text" 
					style="text-align: right"
					data-x="718" 
					data-y="15"
					data-speed="500" 
					data-start="5000" 
					data-easing="easeOutExpo">
					Your
					mind 
            	 <br /> can
					be hacked
								</div>
				<div class="caption lft slide_item_right" 
					data-x="870" 
					data-y="50"
					data-speed="500" 
					data-start="5000" 
					data-easing="easeOutBack">
					<img
						src="/images/pointer_left_up_down_01.png"
						id="rev-hint2" alt="txt img">
				</div>
			</li>

			
			
			
			
			<!-- 5rd slide  -->
			<li data-transition="fade" data-slotamount="7" data-masterspeed="300"
				data-delay="25000" data-thumb=""><img
				src="/images/holocaust_memorial_1900x590_02.jpg"
				alt="">
				<div 
					class="caption lfr slide_title slide_item_left yellow-txt"
					data-x="670" 
					data-y="125" 
					data-speed="400" 
					data-start="500"
					data-easing="easeOutExpo">
					The Mind Holocaust				</div>
				<div class="caption lfr slide_subtitle slide_item_left slide_title bolder-text" 
					data-x="670"
					data-y="180" 
					data-speed="400" 
					data-start="1000"
					data-easing="easeOutExpo">
					is the next holocaust				</div>
				
				<div 
					class="caption lfr slide_desc slide_item_left bolder-text" 
					data-x="670"
					data-y="240" 
					data-speed="400" 
					data-start="2000"
					data-easing="easeOutExpo">
					
					Technologic-telepathy
					 <br /> 
					and other techniques of subliminal conditioning 
					 <br /> 
					are used
					to enslave
					the population,
					 <br /> 
					to torture and neutralize
					libering individuals,
					 <br /> 
					in order to build the most terrific totalitarianism
					 <br /> 
					and to
					annihilate the humanity
					as we know it.
									
				</div>
				
				<a class="caption lft slide_btn btn red slide_item_left"
					href="#welcome"  
					data-x="670" 
					data-y="450" 
					data-speed="400"
					data-start="2500" 
					data-easing="easeOutExpo"> 
					Read more 
				</a>
				
			</li>

		</ul>
		<div class="tp-bannertimer tp-top"></div>
	</div>
</div>
<!-- revolution slider end -->
<!-- END revolutionSlider -->



<!-- START revolutionSlider -->


<!--container start-->
<div class="container">
	<div class="row">
		<!--feature start-->
		<div class="text-center feature-head">
			<h1 style="text-transform: none;"
				id="welcome">
				Welcome to MindHolocaust				<span style="text-transform: uppercase;"></span>
			</h1>
			<p class="dark-almost-text">
				Observatory Against Mind Conditioning
					Technologies			</p>
		</div>
	</div>
	
	<div class="row" style="margin-bottom: 36px;">
		<div class="col-lg-4 col-sm-4">
			<section id="awareness-in-short">
				<div class="f-box">
					<i class="fa fa-search"></i>
					<h2>research					</h2>
					<p class="dark-almost-text">
					
					about military practices
									</p>
				</div>

				<p class="f-text">
					
					The observatory mindholocaust is conducting a research on
					military practices for mind and thoughts conditioning.
								</p>
				<p class="f-text">
					
					The goal of this research is to aknoledge and reveal new technologies and derived techniques used specifically to reduce and to constrain the mind's potential into an entangled automation.
								</p>
				<!--
				<p class="f-text">
					
					Amongst these practices, two main paradigms are applied:
								</p>
				<p class="f-text">
					
					1) that a functional model of the mind can be defined
									<br />
					
					2) that operant-conditioning psychology techniques can be used
					in order to atrophy and to shape the brain's corpus to reduce our
					ability to acquire and maintain meaningful perceptions of the
					world.
								</p>
				-->
				<p class="f-text">
					
					At the moment the research is focusing on three new practices
					that can be used together to fulfill the objective:
								</p>
				<p class="f-text">
					
					1) "mind reading" or "thought identification": the capacity of
					knowing someone else thoughts, thanks to recent neuroscience
					achievements.
									<br />
					
					2) remote brain spying: the capacity of performing mind-reading
					from remote distance, kind of "having the mind online".
									<br />
					
					3) subliminal audio messages: the capacity to influence
					someone's thought using barely perceptible audio stimulations.
								</p>
				<p class="f-text">
					
					 <a href="/awareness/dossier/d/mind-reading" target="_blank"> 
					The first dossier on mind-reading has already been published: a brief but exhaustive and well-documented chronological history of the recent achievements of neuroscience.
					 </a> 
								</p>
				<p class="f-text">
					<a href="/awareness/dossier/d/mind-reading" target="_blank"><i class="fa fa-link"></i>
					Enjoy it					</a>
				</p>
				
			</section>
		</div>
	
		<div class="col-lg-4 col-sm-4">
			<section id="awareness-in-short">
				<div class="f-box">
					<i class="fa fa-comments-o"></i>
					<h2>awareness					</h2>
					<p class="dark-almost-text">
						acknowledge the situation					</p>
				</div>

				<p class="f-text">
					It is important to be aware of the
					situation: knowing the
					phenomenons and its consequences.				</p>

				<p class="f-text">
					In short: an hidden
					power is using
					telepathic technology and
					other techniques
					of mind conditioning with
					the aim of dominating
					individuals
					and the society.				</p>
					
				<p class="f-text">
					With this technology the mind's inner
					voice can be "heard" by a
					third person.				</p>
				
				<p class="f-text">
					Auditory messages can also be sent to the
					subject.				</p>
					
				<p class="f-text">
					This treatment can deeply manipulate a person's behaviour. Anyway, effects can be attenuated or neutralized, if they are known.
								</p>

				<p class="f-text">
					The technology used is almost intangible:
					it is difficult to
					detect, its use is difficult to be
					identified.				</p>

				<p class="f-text">
					Una vez identificado, es difícil de comunicar: es difícil hablar de este fenómeno con otras personas.				</p>

				<p class="f-text">
					These two characteristics (that is
					difficult to be identified
					and its abuse hardly disclosable)
					have slowed down its 
					complaint.				</p>

				<p class="f-text">
					The mindholocaust's chapter "awareness"
					aims to ly
					denounce this phenomenon and to describe
					it in details.				</p>

				<p class="f-text">
					<a href="/awareness" target="_blank"><i class="fa fa-link"></i>
					Read more					</a>
				</p>

			</section>
		</div>
		<div class="col-lg-4 col-sm-4">
			<section id="fellowship-in-short">
				<div class="f-box">
					<i class="fa fa-life-ring"></i>
					<h2>fellowship</h2>
					<p class="dark-almost-text">receive and give mutual support</p>
				</div>

				<p class="f-text">
					If you hear voices and you think they are
					real, then you are in
					the right place and you have found
					people that think the same about.				</p>

				<p class="f-text">
					If you perceive that your thoughts are suggested by subliminal audio messages, sometimes a barely perceptible voice, then you may be a victim of technological telepathy.				</p>

				<p class="f-text">
					If you can listen to subliminal messages in social contexts, radio, television or through any other "white noise", you can then be aware of the control exercised over the masses through the use of subliminal conditioning techniques with technological means.				</p>

				<p class="f-text">
					It is not a disease of yours. It is the
					world where we live in.
					This is happening because of the
					powers that be. They exist.				</p>

				<p class="f-text">
					When someone discover it, usually the
					first reaction he/she has,
					is to have fear and panicking.
					But you can be helped passing the
					worst. We already passed
					through it, it seems now it is your turn.
					You can count
					on our help.				</p>

				<p class="f-text">
					So, feel free to contact us.
					One day, you might help someone
					that is in your current
					situation.				</p>

				<p class="f-text">
					<a href="/contact"><i class="fa fa-link"></i>
					Contact us					</a>
				</p>
			</section>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4 col-sm-4">
			<section id="actions-in-short">
				<div class="f-box">
					<i class="fa fa-sun-o"></i>
					<h2>actions</h2>
					<p class="dark-almost-text">
					how to resist and respond					</p>
				</div>

				<p class="f-text">
					As you have become aware of the telepathic
					technology, what to
					do?				</p>

				<p class="f-text">
					
					First, you need to keep calm. The first goal is to
					win the shock
					taken due to the awareness of what is happening. How?
				 <a href="#techniques_to_avoid_panic"> 
					Some techniques to avoid panic
				 </a> 
					are explained below.
					Help yourself to avoid a  mental
					treatment program.
								</p>

				<p class="f-text">
					
					The awareness shock may stimulate self-injuring
					behaviors and the
					desire to commit suicide. It is good to avoid
					knee-jerk reactions.
					Below there is 
				  <a href="#instinctive_reactions_to_be_avoided"> 
					a list of instinctive reactions that is better to avoid
				 </a> .
								</p>

				<p class="f-text">
					
					Become aware usually causes distress. This
					emotional state can last
					for years. It is good to make an effort to
					reduce distress's impact.
					Some techniques that explain 
				 <a href="#how_to_minimize_the_anguish"> 
					how to minimize the anguish
				 </a> 
					are presented below.
								</p>

				<p class="f-text">
					
					Once the anguish has been attenuated, then it is
					possible to direct
					the response to the outside world. 
				 <a href="#actions_to_counter_the_threat"> 
					A list of actions to counter the threat of the MindHolocaust 
				 </a> 
					are presented below.
								</p>
				
				<p class="f-text">
					<a href="#take-action"><i class="fa fa-link"></i> 
					Take action					</a>
				</p>
			</section>
		</div>
		<div class="col-lg-4 col-sm-4">
			<section>
				<div class="f-box">
					<i class="fa fa-map-marker"></i>
					<h2>places</h2>
					<p class="dark-almost-text">
					meet up, staying and relaxing					</p>
				</div>

				<p class="f-text">
					
					Would you like to meet other people who are fighting against
					the abuse of telepathic technology?				</p>

				<p class="f-text">
					
        		 <a href="#meet_activists"> 
					A list of places where you can meet
					MindHolocaust's activists 
				 </a> 
					is presented below. You can request a
					meeting using the
				 <a href="/contact"> 
					contact form
				 </a> .
								</p>

				<p class="f-text">
					
					It also possible to use the 
        		
        		<a href="/meetup" 
					title="meetup"
					target="_blank">
				
					"Meetup" application
				 </a> 
					in order to organize meetings in your city.
								</p>

				<p class="f-text"> 
					
            	
        		<a href="/channels" 
					title="channels" 
					target="_blank">
				
					Some online communication channels
					
				</a> 
				
					are available for the community.
								</p>

				<p class="f-text">
					
					Among the blueprints under construction we mention the
					most
					important.				</p>

				<p class="f-text">
					
					The MindHolocaust biennial, about the abuse
					of the telepathic
					technology and other threats.
					It is possible to present
					artwork/research proposal
					using the 
				
				<a href="/biennal" 
					title="biennal" 
					target="_blank">
				
					submission form</a>.
				 
					</p>

				<p class="f-text">
					
					The laboratory "Mindlab", to practice techniques and
					technologies of resistance and response to the phenomenon. 
				</p>

				<p class="f-text">
					
					A secured residence in the nature, to get a clear mind
					and to
					relax. 
				</p>

				<p class="f-text">
					<a href="#take-part"><i class="fa fa-link"></i> 
					Take part 
					</a>
				</p>

			</section>
		</div>
		<!--feature end-->
	</div>
</div><!-- END revolutionSlider -->


<!-- START newsletter -->

<div class="container">
	<div class="row">
            <!--quote start -->
            <div class="quote newsletter"> 
            
                <div class="col-lg-9 col-sm-9">
	                <div class="quote-info">
    	            	<div style="margin-top: 4px;">
        	                <span class="title">newsletter</span>
                   		</div>
                   		
		                <input type="text" 
		                	name="subscribeNewsletterEmail" 
		                	data-tag="subscribeNewsletterEmail"
		                	placeholder="your-email@example.com" />
		                	
		                <div 
		                	style="color: #a8a8a8; margin-top: 4px;">
		                The unsubscribe page is		                <a href="http://ml.nextholocaust.com/?p=unsubscribe" 
		                style="color:  #f77b6f;" 
		                target="_blank">
		                here		                </a>
		                </div>
		                
		                <div class="subscribeMessages">
        	                <span
        	                	data-tag="subscribeMessageSuccess" 
        	                	style="color: #41CAC0;">
							
	             <b> 
					You subscription was sent! 
	             </b> 
            	 <br /> 
					We are waiting for your confirmation...
					Check your email (check also
					the spambox).
				        	                </span>
        	                <span
        	                	data-tag="subscribeMessageError" 
        	                	style="color: #ec6459;">
        	                Error: the email address is not valid.        	                </span>
                   		</div>
					</div>
                </div>
                
                
                <div class="col-lg-3 col-sm-3">
                    <a class="btn btn-danger subscribe-btn" 
                    	href="javascript:void(0)"
                    	data-tag="subscribeButton">
                    	Subscribe                    </a>
                </div>
                
            </div>
            <!--quote end-->
        </div>
</div><!-- END newsletter -->


<!-- START byte science decoding_our_sense -->
<!-- /application/modules/default/views/scripts/partials/cbs_documentary.phtml -->

<div class="container" style="margin-bottom: 48px;">

	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xm-12">
			<h2 class="r-work">
					Decoding Our Senses
				</h2>
			<h3 class="r-work-subtitle">
					Our senses, our audio-visual experiences, can be decoded and reconstructed using fMRI and computer algorithms scientists claimed at the end of 2011 beginning 2012.
				</h3>
		</div>
	</div>


	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-12 col-xm-12">

		<video class="video-clip" 
			controls="controls"
			poster="/images/Science_Bytes_-_Decoding_Our_Senses2.png">
			<!-- 
			<source 
				src="/video/Science_Bytes_-_Decoding_Our_Senses.ogv" 
				type="video/ogg" 
				media="all" />
			 -->
			<source 
				src="/video/Science_Bytes_-_Decoding_Our_Senses.webm" 
				type="video/webm" 
				media="all" />
			<source 
				src="/video/Science_Bytes_-_Decoding_Our_Senses.mp4" 
				type="video/mp4" 
				media="all" />
		</video>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-12 col-xm-12">	
		
			<p>
					A short documentary about decoding audio-visual experiences reserches has been produced in the series Science Bytes, funded by Alfred P. Sloan Foundation.
				</p>
			<p style="font-style: italic;">
				
					Copyright not specified. Alfred P. Sloan Foundation.
								<br />
				
					It is believed that the use of partial scaled-down and
					low-resolution videoclip is qualified as fair use.
							</p>
			
			<p style="margin-top:36px;">
				<a href="https://vimeo.com/42863899"
				target="_blank">
				<i class="fa fa-vimeo"></i> &nbsp;
					play it on Vimeo				</a>
				<br />
				<a href="/video/Science_Bytes_-_Decoding_Our_Senses.mp4"
				target="_blank">
				<i class="fa fa-download"></i> &nbsp;
					Download
				</a>
			</p>
			<br />
			<p>
				<a class="btn morevideo-btn" href="/videos">
				more videos				</a>
			</p>
			
		</div>	
	</div>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xm-12">
		
		<br /><br />
		
		</div>
	</div>
	
</div><!-- END cbyte science decoding_our_sense -->


<!-- START facebook-building8 -->
<!--parallax start-->
<section class="parallax2">
	<div class="container fbb8-container" style="">
		<div class="row fbb8-line"  style="margin-top: 5%;">
			<div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-4 fbb8-fb-image fbb8-fb-image-fb" 
				style="margin-top: 0%;">

				<div style="width: 70%; margin-right: auto; margin-left: auto; margin-top: 3%;">
					<img src="/images/facebook-brain-chip.png" class="img-responsive">
				</div>


			</div>
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-4 fbb8-title" 
				style="">

				<h1 class="r-work">
					<span>
					Mind Control and Social Media					</span>
				</h1>

				<div class="fbb8-subtitle">
					<h3 class="r-work-subtitle">
						<span>
						DARPA's former director Regina Douglas kickstarts Facebook's project on mind-reading technologies.						</span>
					</h3>
				</div>


			</div>
			<div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-4 fbb8-fb-image fbb8-fb-image-dp" 
				style="margin-top: 0%;">

				<img src="/images/darpa-logo-618x339.png" class="img-responsive">

			</div>
		</div><!-- row end -->
		<div class="row fbb8-line"  style="margin-top: 5%;">
			<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-xs-5 fbb8-fb-image fbb8-fb-image-rd" 
				style="">

				<img src="/images/douglas-fbb8.jpg" class="img-responsive">

			</div>
			<div class="col-xl-8 col-lg-8 col-md-6 col-sm-6 col-xs-7 fbb8-paragraph">
				<p class="">
					<span>
						<a href="https://news.mindholocaust.is/facebooks-darpa-esque-building-8-project-is-mind-control-for-the-masses/" 
							target="_blank">
						"building 8"
						</a>
						is the name of the project, that will benefit of "hundreds of people and hundreds of millions of dollars" 
						said Mark Zuckerberg in a tweet. It is probably DARPA's imprinting to the next "social" generation.
					</span>
				</p>
			</div>
		</div><!-- row end -->

		
		<div class="row fbb8-line"  style="margin-top: 5%; margin-bottom: 6%;">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 fbb8-link" 
				style="margin-top: 0%;">

				<p class="">
					<span>
					<a class="btn fbb8-btn" href="https://news.mindholocaust.is/facebook-unveils-building-8-brain-activity-decoding/" target="_blank">
					Read more...					</a>
					</span>
				</p>

			</div>
		</div><!-- row end -->
	</div>
</section>
<!--parallax end-->
<!-- END facebook-building8 -->


<!-- START awarness -->
<div class="container" 
	id="awareness"
	style="margin-bottom: 50px">

	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<section class="panel tab">
			
				<header class="panel-heading tab-dossier">
					<ul class="nav nav-tabs nav-justified ">
						<li class="active only">
							
							<div style="float:left;">
								<i class="fa fa-5x fa-comments-o"
									style="vertical-align: middle; color: #475168 ;">
								</i>
							</div>
							<div style="float:left; margin-left: 10px;">
								<p class="dark-almost-text" 
									style="color: #475168 ; margin-bottom: 0px;">
									
					in deep:
									
								</p>
								<h1 class="r-work" 
									style="margin-bottom: 0px; margin-top: 0px;">
									
					awareness
												</h1>
								<p class="dark-almost-text" 
									style="color: #475168 ;">
									
					acknowledge the situation
				 					
								</p>
							</div>
							<div style="clear: both;"></div>
						
						</li>
						<li class="none"></li>
						<li class="none"></li>
					</ul>
				</header>
				<div class="panel-body">
 						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"
 							style="height: 172px">
							<div class="element item view view-tenth" 
								data-zlname="reverse-effect">
									<img src="/images/dossier.png" 
										alt="dossier mind reading" 
										style=" 
											border-radius: 4px; 
											width: 100%;
											margin-bottom: 12px;"
									/>
									<div class="mask">
											<a data-zl-popup="link" 
												href="/en/mind-reading-dossier-part-1">
													<i class="fa fa-link"></i>
											</a>
									</div>
							</div>
						</div>

						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
							<h3 class="r-work-subtitle-bigger">
								<a href="/en/mind-reading-dossier-part-1">
								
									History of Thought Identification 
									<br /><span class='reduxtext'>
										A dossier about Mind
										Reading, Part I (2006-2015)
									</span>
												</a>		
							</h3>
							<p class="f-text">
								
					MindHolocaust is proud to present and to publish the
					"Mind
					Reading" dossier: a short but exhaustive chronological history
					about mind-reading
					technology.
											</p>
							<p class="f-text">
								
					The dossier answers to the question "Does mind-reading
					technology
					exist?" with extreme accuracy.
											</p>
							<p class="f-text">
								
					Each statement of the dossier is well documented and well
					proved.
											</p>	
							<p class="f-text">
								
					All the documentation is provided with the dossier as web
					links
					and PDF documents.
				 
							</p>
							<p class="f-text">
								
					All the dossier's documents are official documents: scientific
					ations, press releases published by universities or
					institutions or federal agencies.
				 
							</p>
							<p class="f-text">
								<a href="/en/mind-reading-dossier-part-1">
									<i class="fa fa-link"></i>
									
					Read and enjoy the dossier
				 
								</a>
							 </p>
							
						</div>
				</div>
			</section>
		</div>
	</div>

</div><!-- container END -->
<!-- END awareness -->

<!-- START artworks video -->
<!-- Educationsl VIDEO -->
<div class="property gray-bg">
	<div class="container educationals">
		<div class="row">
			<div class="col-lg-12 col-sm-12">
				<h2 class="r-work">Metropia: a likely dystopia</h2>
				<h3 class="r-work-subtitle">
				How the telepathic technology looks like.				</h3>
				
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-3 col-sm-3">
				<div class="quote-info">
					<p>
						Metropia is a movie about a world's
						corporate-driven dystopia in
						which corporations can read
						people's thoughts and deliver
						informations to the
						people's mind...
					</p>
					<p>
						<a 
							class="p-head"
							style="font-size: 12px; font-weight: 300"
							href="http://en.wikipedia.org/wiki/Metropia_%28film%29"
							target="_blank"><i class="fa fa-link"></i>  &nbsp;
							Read more about Metropia						</a>
						<a href="/artworks" class="btn btn-danger mediateca-btn">
							more artworks						</a>
					</p>
				</div>
			</div>

			<div class="col-lg-3 col-sm-3">
				<div class="video-thumb element view-tenth item" data-zlname="reverse-effect">
					<img src="/images/thumbnail-metropia-video-01-review.jpg" 
    					alt="" />
						<div class="mask">
							<a data-zl-popup="link" 
								href="/en/artworks/metropia-the-main-idea">
								<i class="fa fa-play"></i>
							</a>
						</div>
				</div>
				<div class="title-bottom">
					<a class=" p-head" 
						href="/en/artworks/metropia-the-main-idea">
						Metropia: The main idea.
					</a>
					<p>
						
						This videoclip is an extract from a Metropia's review:
						the movie's main idea is explained.

					</p> 
				</div>
			</div>

			<div class="col-lg-3 col-sm-3">
				<div class="video-thumb element view-tenth item" data-zlname="reverse-effect">
					<img src="/images/thumbnail-metropia-video-02-inneryou.jpg" 
    					alt="" />
						<div class="mask">
								<a data-zl-popup="link" 
									href="/en/artworks/metropia-am-i-hearing-things">
									<i class="fa fa-play"></i>
								</a>
						</div>
				</div>
				<div class="title-bottom">
					<a class=" p-head" 
						href="/en/artworks/metropia-am-i-hearing-things">
						Metropia: Am I hearing things?
					</a>
					<p>
						This videoclip is about a man that is experiencing the
						hearing of a stranger's voice inside his own head.
						The stranger
						listen to the man's inner voice's thoughts...
					</p> 
				</div>
			</div>

			<div class="col-lg-3 col-sm-3">
				<div class="video-thumb element view-tenth item" data-zlname="reverse-effect">
					<img src="/images/thumbnail-metropia-video-02-hardtotalk.jpg" 
    					alt="" />
						<div class="mask">
								<a data-zl-popup="link" 
									href="/en/artworks/metropia-it-is-hard-to-talk-about-it">
									<i class="fa fa-play"></i>
								</a>
						</div>
				</div>
				<div class="title-bottom">
					<a class=" p-head" 
						href="/en/artworks/metropia-it-is-hard-to-talk-about-it">
						Metropia: It is hard to talk about it.
					</a>
					<p>
						This videoclip shows how embarrassing can be to
						talk
						about this phenomenon.
					</p> 
				</div>
			</div>
			
		</div>
	</div>
</div>
<!-- END VIDEO --><!-- END artworks video -->




<!-- START cbs_documentary -->
<!-- /application/modules/default/views/scripts/partials/cbs_documentary.phtml -->

<div class="container" style="margin-bottom: 48px;">

	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xm-12">
			<h2 class="r-work">
					Reading
					your mind
				</h2>
			<h3 class="r-work-subtitle">
					the CBS documentary about
					the nuroscience technique called
					&#8220;thought
					identification&#8221;
				</h3>
		</div>
	</div>


	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-12 col-xm-12">

		<video class="video-clip" 
			controls="controls"
			poster="/images/CBS_Reading_Your_Mind.png">
			<!-- 
			<source 
				src="/video/CBS_Reading_Your_Mind.ogv" 
				type="video/ogg" 
				media="all" />
			 -->
			<source 
				src="/video/CBS_Reading_Your_Mind.webm" 
				type="video/webm" 
				media="all" />
			<source 
				src="/video/CBS_Reading_Your_Mind.mp4" 
				type="video/mp4" 
				media="all" />
		</video>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-12 col-xm-12">	
		
			<p>
					Neuroscience has learned much about the brain's
					activity
					and its link
					to certain thoughts. 
					 <br /> 
					As Lesley Stahl reports,
					it may
					now be possible, on a basic level, to
					read a person's mind.
				</p>
			<p style="font-style: italic;">
				
					Copyright © 2009 by CBS. All Rights Reserved.
								<br />
				
					It is believed that the use of partial scaled-down and
					low-resolution videoclip is qualified as fair use.
							</p>
			
			<p style="margin-top:36px;">
				<a href="https://www.youtube.com/watch?v=8jc8URRxPIg"
				target="_blank">
				<i class="fa fa-youtube"></i> &nbsp;
					play it on Youtube				</a>
				<br />
				<a href="/video/CBS_Reading_Your_Mind.mp4"
				target="_blank">
				<i class="fa fa-download"></i> &nbsp;
					Download
				</a>
			</p>
			<br /> <br />
			<p>
				<a class="btn morevideo-btn" href="/videos">
				more videos				</a>
			</p>
			
		</div>	
	</div>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xm-12">
		<hr />
		</div>
	</div>
	
</div><!-- END cbs_documentary -->


<!-- START targeted-individuals-map -->
<div class="property gray-bg">
	<div class="container educationals">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<h2 class="r-work">Targeted Individuals Map</h2>
				<h3 class="r-work-subtitle">
				Global census of targeted individuals.				</h3>
			
				<div class="title-bottom">
					<a class=" p-head" href="http://targeted-individuals-map.mindholocaust.is" 
					target="_blank" >
					<i class="fa fa-link"></i>
						Open the Targeted Individuals Map census					
					</a>
					<p>
						<i>
						Be aware, clicking  the link will open a webpage that is tracked by Google.						</i>
					</p>
					<p>	
						The targeted individuals map is a global census of targeted 
				individuals displayed on a world map. 					</p> 
					<p>	
						The targeted-individuals-map website offers a wolrd view on the community of persons that marked themself in this census as Mind Control-ed Targeted Individuals. 					</p> 
				</div>

			</div><!-- ends DIV class: column -->

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<a href="http://targeted-individuals-map.mindholocaust.is"
					target="_blank">
				<img style="width: 100%; border-radius: 4px; margin-top: 10px;" 
						src="/images/targeted-individuals-map.png" 
						alt="targeted individuals map" />
				</a>
			</div>

			
		</div><!-- ends DIV class: row -->
	</div><!-- ends DIV class: container -->
</div><!-- ends DIV class: property --><!-- END targeted-individuals-map -->


<!-- START articles & events -->
<div class="container" style="margin-bottom: 50px">
	<div class="row">
		<!-- LATEST ARTICLES -->
		<div class="col-lg-9 col-sm-9">
			<h1 class="r-work">
				Latest Articles
			</h1>
			<div id="news_list"></div>
			<a href="/news" class="btn btn-danger newsevent-btn">
				more articles
			</a>
		</div>
		
		<!-- UPCOMING EVENTS -->
		<div class="col-lg-3 col-sm-3">
			<h1 class="r-work">upcoming events</h1>
			<article class="media">
				<a class="pull-left thumb p-thumb"> <img
					src="/images/events-calendar_01.jpg"
					alt="">
				</a>
				<div class="media-body">
					<a href="#" class=" p-head">Coming soon</a>
					<p>Coming soon.</p>
				</div>
			</article>
			<a href="/events" class="btn btn-danger newsevent-btn">
				more events
			</a>
		</div>
	</div>
</div>
<!-- END articles & events -->


<!-- START donation & sponsorship -->
<!-- DONATIONS & SPONSORSHIPS -->
<div class="property gray-bg">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-sm-12">
				<h1>
				donations  &amp; 
					sponsorships
								</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3 col-sm-3">
				<p>
					
					Support the defense of human rights. Help us to stop the 
				 <span style="color: #c00000"> 
					Mind</span><span style="color: #000000">Holocaust
				</span>.
								</p>
				<p>
					
					Your generosity makes possible to improve our
					investigations and
					to denounce the abuses of the
					telepathic technology more aloud.
								</p>
				<h3 class="donate-thanks">
					<i>
					Thanks.
				</i>
				</h3>
			</div>

			<div class="col-lg-3 col-sm-3">
				<div class='hoverbox' style="margin-top: 0px;">
					<h4>
					Direct Bank Transfer
				</h4>
					<p>
					Direct Bank Transfers Donations
				</p>
					<a href="javascript:;" class="btn btn-purchase"
						style="padding: 16px 16px;">
					donate now
				</a>
				</div>
			</div>

			<div class="col-lg-3 col-sm-3">
				<div class='hoverbox' style="margin-top: 0px;">
					<h4>
					One Time Donation
				</h4>
					<p>
					Global PayPal Donations
				</p>
					<a href="javascript:;" class="btn btn-purchase"
						style="padding: 16px 16px;">
					donate now
				</a>
				</div>
			</div>

			<div class="col-lg-3 col-sm-3">
				<div class='hoverbox' style="margin-top: 0px;">
					<h4>
					Recurring Donation
				</h4>
					<p>
					Global PayPal Donations
				</p>
					<a href="javascript:;" class="btn btn-purchase"
						style="padding: 16px 16px;">
					donate now
				</a>
				</div>
			</div>
		</div>

	</div>
</div>
<!-- END DONATIONS & SPONSORSHIPS --><!-- END donation & sponsorship -->

<!-- START catchy phrase -->
<!--parallax start-->
<section class="parallax1">
	<div class="container">
		<div class="row">
			<h1>
				“
					Despite our fears or our knowledge, try every step however
					little results it can ensure - in short, act as if however
					we had
					the right to a minimum hope of success.
				”
			</h1>
		</div>
		<div class="row" style="text-align: right;">
		<h5>Günther Anders</h5>
		
		</div>
	</div>
</section>
<!--parallax end-->
<!-- END catchy phrase -->


<!-- START donation & sponsorship -->


<div class="container">
	<!--clients start-->
	<div class="clients">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<ul class="list-unstyled">
						<li>
							<a href="http://www.un.org/en/documents/udhr/index.shtml#a12"
								target="_blank">
							<img
								src="/images/logo_justice_sm.png"
								alt="Universal Decalration of Human Rights" 
								title="Universal Decalration of Human Rights"  />
							</a>
						</li>
						<li>
							<a href="https://www.amnesty.org/"
								target="_blank">
							<img
								src="/images/logo_amnesty_international.gif"
								alt="Amnesty International" 
								title="Amnesty International" />
							</a>
						</li>
						<li>
							<a href="https://www.1984hosting.com/"
								target="_blank">
							<img
								src="/images/logo_1984_hosting.png"
								alt="1984 hosting" 
								title="1984 hosting" />
							</a>
						</li>
						
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!--clients end-->
</div>

<!-- END Content -->

<!-- footer START -->
@include('footer');
<!-- footer END -->
<!-- html footer START -->
@include('html_footer');
@include('html_footer_news');
<!-- html footer END -->

    </body>
</html>