<!DOCTYPE html>
<html lang="en">

@include('html_header');

<body>
@include('header');
<!-- metropia_the_main_idea.blade.php -->

<!-- START Content -->

<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-sm-4">
				<h1>
					Artworks
				</h1>
				<p style="color: #BFBFEF">
				
					An imaginary about telepathic technology.
          
				</p>
			</div>
			<div class="col-lg-8 col-sm-8 navigation">
				<a href="/en/welcome">MindHolocaust</a> &nbsp; &gt; &nbsp; 
        <a href="/artworks">
					artworks
				</a> &nbsp; &gt; &nbsp; metropia: the main idea
      </div>
		</div>
	</div>
</div>

<div class="property gray-bg">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-sm-12">

			<video class="video-clip" 
				controls="controls" 
				autoplay="autoplay">

				<source 
					src="/video/metropia_-_the_main_idea.webm" 
					type="video/webm" 
					media="all" />
				<source 
					src="/video/metropia_-_the_main_idea.mp4" 
					type="video/mp4" 
					media="all" />
			</video>
			
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<h3>Metropia: The main idea.</h3>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">	
			
				<p>
					Metropia is a movie about a world's corporate-driven
					dystopia in which corporations can read people's
					thoughts and
					deliver informations to the people's mind...
				</p>
				<p>
					This videoclip is an extract from a Metropia's review:
					the movie's main idea is explained.
				</p>
				<p style="font-style: italic;">
					
					Copyright © 2009 by Atmo. All Rights Reserved.
									<br />
					
					It is believed that the use of partial scaled-down and
					low-resolution videoclip is qualified as fair use.
								</p>
				
			</div>	
		</div>
		
	</div>
</div>
<!-- END Content -->

<!-- footer START -->
@include('footer');
<!-- footer END -->
<!-- html footer START -->
@include('html_footer');
<!-- html footer END -->

    </body>
</html>