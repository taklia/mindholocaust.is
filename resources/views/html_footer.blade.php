<script
		type="text/javascript"
		src="/flatlab/js/jquery-1.8.3.min.js">
</script>
<script
		type="text/javascript"
		src="/flatlab/js/bootstrap.min.js">
</script>
<script
		type="text/javascript"
		src="/flatlab/js/hover-dropdown.js">
</script>
<script
		defer
		type="text/javascript"
		src="/flatlab/js/jquery.flexslider.js">
</script>
<script
		type="text/javascript"
		src="/flatlab/assets/bxslider/jquery.bxslider.js">
</script>

<script
		type="text/javascript"
		src="/flatlab/js/jquery.parallax-1.1.3.js">
</script>

<script
		type="text/javascript"
		src="/flatlab/js/jquery.easing.min.js">
</script>
<script
		type="text/javascript"
		src="/flatlab/js/link-hover.js">
</script>

<script
		type="text/javascript"
		src="/flatlab/assets/fancybox/source/jquery.fancybox.pack.js">
</script>

<script
		type="text/javascript"
		src="/flatlab/assets/revolution_slider/rs-plugin/js/jquery.themepunch.plugins.min.js">
</script>
<script
		type="text/javascript"
		src="/flatlab/assets/revolution_slider/rs-plugin/js/jquery.themepunch.revolution.min.js">
</script>

<!--common script for all pages-->
<script
		type="text/javascript"
		src="/flatlab/js/common-scripts.js">
</script>

<script
		type="text/javascript"
		src="/flatlab/js/revulation-slide.js">
</script>


<!-- INIT objects -->
	<script
			type="text/javascript">

		RevSlide.initRevolutionSlider();

		$(window).load(function()
		{
				$('[data-zlname = reverse-effect]').mateHover(
						{
								position: 'y-reverse',
								overlayStyle: 'rolling',
								overlayBg: '#fff',
								overlayOpacity: 0.7,
								overlayEasing: 'easeOutCirc',
								rollingPosition: 'top',
								popupEasing: 'easeOutBack',
								popup2Easing: 'easeOutBack'
							}
					);
		});

		$(window).load(function()
		{
				$('.flexslider').flexslider(
				{
						animation: "slide",
						start: function(slider)
						{
								$('body').removeClass('loading');
							}
					});
			});
			// fancybox
		jQuery(".fancybox").fancybox();

</script>

<script
		type="text/javascript"
		src="/javascript/default.js">
</script>




<!-- Piwik Analitycs -->
<script type="text/javascript">
	var _paq = _paq || [];
	_paq.push(['trackPageView']);
	_paq.push(['enableLinkTracking']);
	(function() {
		var u="//analytics.nextholocaust.com/";
		_paq.push(['setTrackerUrl', u+'piwik.php']);
		_paq.push(['setSiteId', 1]);
		var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
		g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
	})();
</script>
<noscript><p><img src="//analytics.nextholocaust.com/piwik.php?idsite=1" style="border:0;" alt="" /></p></noscript>
<!-- End Piwik Code -->