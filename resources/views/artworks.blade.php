<!DOCTYPE html>
<html lang="en">

@include('html_header');

<body>
@include('header');
<!-- artworks.blade.php -->

<!-- START Content -->

<!-- breadcrumbs -->
<div class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-sm-4">
				<h1>
					Artworks
				</h1>
				<p style="color: #BFBFEF ">
					An imaginary about telepathic technology.
				</p>
			</div>
			<div class="col-lg-8 col-sm-8 navigation">
				<a href="/">MindHolocaust</a> &nbsp; &gt; &nbsp; 
					artworks
			</div>
		</div>
	</div>
</div>


<div class="container">

	
	<div class="row" style="margin-bottom: 60px;">
		<div class="col-lg-3 col-md-6 col-sm-6  text-right">
			<a href="/artworks/video/v/metropia_-_the_main_idea">
		
			<img style="width: 263px; margin-bottom: 10px; margin-top: 60px;"
				alt="" src="/images/thumbnail-metropia-video-01-review.jpg" />
				
			</a>
		</div>
		<div class="col-lg-9 col-md-6 col-sm-6">
			<h3>Metropia: The main idea.</h3>
			<p>
					Metropia is a movie about a world's corporate-driven
					dystopia in which corporations can read people's
					thoughts and
					deliver informations to the people's mind...
				</p>
			<p>
					This videoclip is an extract from a Metropia's review:
					the movie's main idea is explained.
				</p>

			<a class="btn btn-purchase"
				href="/artworks/video/v/metropia_-_the_main_idea">
				
					Watch it
								</a>
		</div>
	</div>
	
	
	<div class="row" style="margin-bottom: 60px;">
		<div class="col-lg-3 col-md-6 col-sm-6  text-right">
			<a href="/artworks/video/v/metropia_-_am_i_hearing_things">
		
			<img style="width: 263px; margin-bottom: 10px; margin-top: 60px;"
				alt="" src="/images/thumbnail-metropia-video-02-inneryou.jpg" />
				
			</a>
		</div>
		<div class="col-lg-9 col-md-6 col-sm-6">
			<h3>Metropia: Am I hearing things?</h3>
			<p>
					Metropia is a movie about a world's corporate-driven
					dystopia in which corporations can read people's
					thoughts and
					deliver informations to the people's mind...
				</p>
			<p>
					This videoclip is about a man that is experiencing the
					hearing of a stranger's voice inside his own head.
					The stranger
					listen to the man's inner voice's thoughts...
				</p>

			<a class="btn btn-purchase"
				href="/artworks/video/v/metropia_-_am_i_hearing_things">
				
					Watch it
								</a>
		</div>
	</div>
	
	
	<div class="row" style="margin-bottom: 60px;">
		<div class="col-lg-3 col-md-6 col-sm-6  text-right">
			<a href="/artworks/video/v/metropia_-_hard_to_talk_about">
		
			<img style="width: 263px; margin-bottom: 10px; margin-top: 60px;"
				alt="" src="/images/thumbnail-metropia-video-02-hardtotalk.jpg" />
				
			</a>
		</div>
		<div class="col-lg-9 col-md-6 col-sm-6">
			<h3>Metropia: It is hard to talk about it.</h3>
			<p>
					Metropia is a movie about a world's corporate-driven
					dystopia in which corporations can read people's
					thoughts and
					deliver informations to the people's mind...
				</p>
			<p>
					This videoclip shows how embarrassing can be to
					talk
					about this phenomenon.
				</p>

			<a class="btn btn-purchase"
				href="/artworks/video/v/metropia_-_hard_to_talk_about">
				
					Watch it
								</a>
		</div>
	</div>
	
		
</div>

<!-- END Content -->

<!-- footer START -->
@include('footer');
<!-- footer END -->
<!-- html footer START -->
@include('html_footer');
<!-- html footer END -->

    </body>
</html>