<!-- es_footer.blade.php -->
<!--footer start-->
<footer class="footer">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-sm-3">
				<h1>
					datos de contacto
				</h1>
				<address>
 					<!-- <p>Matteo Mazzanti</p> -->
 					<p>NextHolocaust</p>
					<p>
					Dirección
				:No. 9, Mariedalsvagen, #1302</p>
					<p>21754, Malmö, Sweden </p>
					<p>
					Mapa
				: <a href="https://www.google.se/maps/place/Mariedalsv%C3%A4gen+9,+217+54+Malm%C3%B6/@55.6017824,12.9800826,17z/data=!3m1!4b1!4m5!3m4!1s0x4653a6abb4e10b91:0x9e71085088afc7f8!8m2!3d55.6017794!4d12.9822766"
					 	target="_blank">
						Open Google Map</a>
					</p>
					<p>
					Teléfono
				: +46 72681516</p>
					<p>
						Email : <a href="/contact-form">info@nextholocaust.com</a>
					</p>
				</address>
			</div>
			<div class="col-lg-5 col-sm-5">
				<h1>
					política de privacidad
				</h1>
				<div class="tweet-box">
					<span style="font-weight: bold;">
					
					Nos preocupamos por tu privacidad en línea también
									</span>
					<br />
					<ul>
						<li>
							
					No permitimos que
           		
				<a href="http://en.wikipedia.org/wiki/Criticism_of_Google#Tracking"
					target="_blank"
					title="Criticism of Google"> 
				
					Google haga seguimiento de tu navegación
				
				</a> 
				
					a través de Google Analytics ni con otros softwares de Google.
										</li>
						<li>
							
					No permitimos que
            	
				<a href="http://en.wikipedia.org/wiki/Criticism_of_Facebook#Tracking_cookies"
					target="_blank"
					title="Criticism of Facebook">
				
					Facebook/Google+ (y similares)
				
				</a> 
				
					hagan seguimiento de tu navegación a través del uso
					de sus iconos de
					seguimiento en nuestras páginas.
										</li>
						<li>
							
					En general, 
				 <span style="text-decoration: underline;"> 
					no damos datos sobre ninungo de nuestros usuarios
					a terceros.</span>.
										</li>
						<li>
							
					Este sitio web se ejecuta en un servicio de alojamiento
					ofrecido por entusiastas del free-software y de
					los derechos
					civiles.
										</li>
					</ul>
					<p>
						
					Lee más acerca de nuestra 
				 <a href="/privacy_policy"> 
					política de privacidad aquí</a>.
									</p>
				</div>
			</div>
			<div class="col-lg-3 col-sm-3 col-lg-offset-1 copyrights">
				<h1>2021 - MindHolocaust</h1>
				
					
				<div style="margin: 0px 0px 10px 4px;">
					<a 
						rel="license" 
						href="http://creativecommons.org/licenses/by-sa/4.0/"
						target="_blank">
						<img 
							alt="Creative Commons License" 
							style="border-width:0" 
							src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" />
					</a>
				</div>

				<p>	
					Este obra está bajo una 
					<a 
						rel="license" 
						href="http://creativecommons.org/licenses/by-sa/4.0/"
						target="_blank">
						Creative Commons Reconocimiento  
							-Compartir Igual			4.0
							Internacional</a>.
					<br />
				</p>
				<p>
						Puede hallar permisos más allá de los
								concedidos con esta
								licencia en 
					<a 
						href="http://nextholocaust.com/contact" 
						target="_blank">
						http://nextholocaust.com/contact		</a>.
				</p>
				<div style="margin-top:20px;">
					
				producido por
									<a href="http://taklia.com"
						target="_blank"
						style="font-weight: bold;">
					taklia.com
					</a>
				</div>
			</div>
		</div>
	</div>
</footer>
<!--footer end-->
