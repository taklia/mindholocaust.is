<!-- html_footer_news.blade.php -->
<script>
$(document).ready(function () {
	$.ajax({
  	type: 'GET',
    url: 'http://news.mindholocaust.is/wp-json/wp/v2/posts?per_page=10&_embed',
    success: function (data) {
      var posts_html = '';
      $.each(data, function (index, post) {
        let initialDate = post.date
        let formattedDate = new Date(initialDate).toLocaleDateString('en-US', {
        	day: '2-digit',
        	month: 'short',
					year: 'numeric',
          hour12: false
        })

				posts_html += '<article class="media"> ';
				posts_html += '  <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12"> ';
				posts_html += '	   <a ';
				posts_html += '	     class="pull-left thumb"  ';
				posts_html += '		   href="' + post.link + '">  ';
				posts_html += '		   <img  ';
				posts_html += '			   style="width: 100%; border-radius: 25px; margin-bottom: 12px;" ';
				posts_html += '			   src="' + post._embedded['wp:featuredmedia']['0'].source_url + '" ';
				posts_html += '			   alt="' + post.title.rendered + '"  ';
				posts_html += '		   /> ';
				posts_html += '	   </a> ';
				posts_html += '  </div> ';
				posts_html += '  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12"> ';
				posts_html += '    <p style="color: #797979; font-style: italic;"> ';
				posts_html += '	     Article published on ';
				posts_html += '      <time datetime="' + formattedDate + '" pubdate="pubdate"> ';
				posts_html +=			     formattedDate
				posts_html += '		   </time> ';
				posts_html += '	   </p> ';
				posts_html += '     <div class="media-body"> ';
				posts_html += '	      <a  ';
				posts_html += '		      href="' + post.link + '" class=" p-head">  ';
				posts_html += 			    post.title.rendered
				posts_html += '		    </a> ';
				posts_html += '		    <p> ';
				posts_html += 		       post.excerpt.rendered
				posts_html += '		    </p> ';
				posts_html += '		    <p> ';
				posts_html += '			    <a href="' + post.link + '"> ';
				posts_html += '				<i class="fa fa-link"></i> ';
				posts_html += '				Read more</a> ';
				posts_html += '		</p> ';
				posts_html += '	</div> ';
				posts_html += '</div> ';
				posts_html += '</article> ';
      });
      $('#news_list').html(posts_html);
    },
    error: function (request, status, error) {
    	// alert(error);
  	}
  });
});
</script>