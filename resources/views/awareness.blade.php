<!DOCTYPE html>
<html lang="en">

@include('html_header');

<body>
@include('header');
<!-- awarness.blade.php -->

<!-- START Content -->

<!-- breadcrumbs -->
<div class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-sm-4">
				<h1>awareness</h1>
				<p style="color: #BFBFEF ">
					acknowledge the situation				</p>
			</div>
			<div class="col-lg-8 col-sm-8 navigation">
				<a href="/">MindHolocaust</a> &nbsp; &gt; &nbsp; 
					awareness
			</div>
		</div>
	</div>
</div>

<div class="container">
	<!-- Dossier Mind-Reading START -->
	<div class="row" style="margin-bottom: 60px;">
		<div class="col-lg-3 col-md-6 col-sm-6  text-right">
			<img style="width: 263px; margin-bottom: 10px; margin-top: 60px;"
				alt="" src="/images/dossier.png">
		</div>
		<div class="col-lg-9 col-md-6 col-sm-6">
			<h3>
					History of Thought Identification 
					<br /><span class='reduxtext'>
					A dossier about Mind
					Reading, Part I (2006-2015)
					</span>
							</h3>
			<p class="f-text">
					MindHolocaust is proud to present and to publish the
					"Mind
					Reading" dossier: a short but exhaustive chronological history
					about mind-reading
					technology.
							</p>
			<p class="f-text">
					The dossier answers to the question "Does mind-reading
					technology
					exist?" with extreme accuracy.
							</p>
			<p class="f-text">
					Each statement of the dossier is well documented and well
					proved.
							</p>	
			<p class="f-text">
					All the documentation is provided with the dossier as web
					links
					and PDF documents.
			</p>
			<p class="f-text">
					All the dossier's documents are official documents: scientific
					publications, press releases published by universities or
					institutions or federal agencies.
			</p>
			<a class="btn btn-purchase"
				href="/awareness/dossier/d/mind-reading">
					Read and enjoy the dossier
			</a>
		</div>
	</div>
	<!-- Dossier Mind-Reading END -->

	<!-- Some videos START -->
	<div class="row" style="margin-bottom: 60px;">
		<div class="col-lg-3 col-md-6 col-sm-6  text-right">
			<a href="/videos/watch/v/Science_Bytes_-_Decoding_Our_Senses">
				<img style="width: 263px; margin-bottom: 10px; margin-top: 60px;"
						alt="" src="/images/thumbnail-Science_Bytes_-_Decoding_Our_Senses.png" />
			</a>
		</div>
		<div class="col-lg-9 col-md-6 col-sm-6">
			<h3>
					Decoding Our Senses
			</h3>
			<p>
					Our senses, our audio-visual experiences, can be decoded and 
					reconstructed using fMRI and computer algorithms scientists claimed 
					at the end of 2011 beginning 2012.
			</p>
			<p>
					A short documentary about decoding audio-visual experiences 
					reserches has been produced in the series Science Bytes, 
					funded by Alfred P. Sloan Foundation.
			</p>
			<a class="btn btn-purchase"
					href="/videos/watch/v/Science_Bytes_-_Decoding_Our_Senses">
					Watch the video
			</a>
		</div>
	</div>
	
	<div class="row" style="margin-bottom: 60px;">
		<div class="col-lg-3 col-md-6 col-sm-6  text-right">
			<a href="/videos/watch/v/CBS_Reading_Your_Mind">
				<img style="width: 263px; margin-bottom: 10px; margin-top: 60px;"
						alt="" src="/images/thumbnail-CBS_Reading_Your_Mind.png" />
			</a>
		</div>
		<div class="col-lg-9 col-md-6 col-sm-6">
			<h3>
					Reading your mind
				</h3>
			<p>
					the CBS documentary about
					the nuroscience technique called
					&#8220;thought
					identification&#8221;
			</p>
			<p>
					Neuroscience has learned much about the brain's
					activity
					and its link
					to certain thoughts. 
					 <br /> 
					As Lesley Stahl reports,
					it may
					now be possible, on a basic level, to
					read a person's mind.
			</p>
			<a class="btn btn-purchase"
					href="/videos/watch/v/CBS_Reading_Your_Mind">
					Watch the video
			</a>
		</div>
	</div>

	<div class="row" style="margin-bottom: 60px;">
		<div class="col-lg-3 col-md-6 col-sm-6  text-right">
			<a href="/videos/watch/v/John-Dylan_Haynes_-_Mind_reading_with_brain_scanners">
				<img style="width: 263px; margin-bottom: 10px; margin-top: 60px;"
					alt="" src="/images/thumbnail-John-Dylan_Haynes_-_Mind_reading_with_brain_scanners.png" />
			</a>
		</div>
		<div class="col-lg-9 col-md-6 col-sm-6">
			<h3>
					John-Dylan Haynes: Mind reading with brain scanners
				</h3>
			<p>
					A TEDx Talk about Mind Reading
				</p>
			<p>
					The research of John-Dylan Haynes focuses on the neural
					mechanisms underlying human cognitive processes. His special
					interests are technical and ethical foundations of mental state
					decoding, as well as the neuroscience of consciousness, intentions
					and free will.
			</p>
			<a class="btn btn-purchase"
					href="/videos/watch/v/John-Dylan_Haynes_-_Mind_reading_with_brain_scanners">
					Watch the video
			</a>
		</div>
	</div>

</div>
<!-- Some videos END -->

<!-- END Content -->

<!-- footer START -->
@include('footer');
<!-- footer END -->
<!-- html footer START -->
@include('html_footer');
<!-- html footer END -->

    </body>
</html>