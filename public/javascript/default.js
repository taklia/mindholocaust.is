/**
 * Default javascipt
 */



$( document ).ready(function() 
	{
	    attachSubscribeToButton();
	    initHTMLPopover();
	}
);


/**
 * Show Error message in contact form
 */
var formErrorShow = function( formName , formMessages )
{
	for( var elName in formMessages )
	{

		for( var errorType in formMessages[ elName ] )
		{
			var errorMessage = formMessages[ elName ][ errorType ]; 
			var selectorForm = " FORM[ name='" + formName + "' ] ";
			
			var selectorField = selectorForm + " INPUT[ name='" + elName + "' ]" ;
			$( selectorField ).addClass( 'formError' );
			$( selectorField ).next( '.formError-text' ).text( errorMessage );
			
			var selectorField = selectorForm + " SELECT[ name='" + elName + "' ]" ;
			$( selectorField ).addClass( 'formError' );
			$( selectorField ).next( '.formError-text' ).text( errorMessage );
			
			var selectorField = selectorForm + " TEXTAREA[ name='" + elName + "' ]" ;
			$( selectorField ).addClass( 'formError' );
			$( selectorField ).next( '.formError-text' ).text( errorMessage );
		}
	}
}




function attachSubscribeToButton()
{
	$( "A[ data-tag='subscribeButton' ]" ).click( 
		function( event )
		{
			event.preventDefault();
			subscribeNewsletter();
		}	
	);
}

/**
 * Subscribe Mindholocaust newsletter
 */
var subscribeNewsletter = function()
{
	var formNewsletterEmail = $( 'INPUT[ data-tag="subscribeNewsletterEmail" ]' ).val();
	
	$.ajax(
		{
			type: "POST",
			url: "/index/jnewsletter",
			data: 
				{ 
					'subscribeNewsletterEmail' : formNewsletterEmail
				}
		}
	).done(function( msg ) 
		{
			if( "undefined" != typeof msg 
					&& "undefined" != typeof msg.success )
			{
				if( ! msg.success )
				{
					if( msg.formError )
					{
						// show msg
						$( "SPAN[ data-tag='subscribeMessageError' ]" )
							.show();
						// hide msg
						$( "SPAN[ data-tag='subscribeMessageError' ]" )
							.delay(10000)
							.fadeOut(10000);
					}
				}
				else if( msg.success  )
				{
					// show msg
					$( "SPAN[ data-tag='subscribeMessageSuccess' ]" )
						.show();
					// hide msg
					$( "SPAN[ data-tag='subscribeMessageSuccess' ]" )
						.delay(10000)
						.fadeOut(10000);
					// clean form
					$( "INPUT[ data-tag='subscribeNewsletterEmail' ]" )
						.val( '' );
				}
			}
		}
	);
}



/**
 * Enable custom made bootstrap popover with HTML content
 */
var initHTMLPopover = function()
{
	var triggers = $("[ data-tag='HTMLPopoverTrigger' ]") ;
	for( var i = 0 ; i < triggers.length ; i++ )
	{
		var el = triggers[ i ] ;
		var popoverId = $( el ).attr( 'data-targetPopoverId' );
		var selector = "DIV[ data-popoverId='" + popoverId + "' ] " ;
		var title = $( selector ).find( "DIV[ data-tag='HTMLPopoverTitle' ]" ).html();
		var content = $( selector ).find( "DIV[ data-tag='HTMLPopoverContent' ]" ).html();
		$( el ).popover(
			{
				html : true , 
				content: content ,
				title , title
			}
		);
	}
}

/**
 * Universal Popupover Outside Click Close Method :)
 */
jQuery(document).mouseup( function(e)
	{ 
		var popocontainer = jQuery(".popover");
		if (popocontainer.has(e.target).length === 0)
		{
			jQuery('.popover').toggleClass('in').remove();
		}
	}
);


/*
$(".htmlPopover").popover(
	{
		html : true, 
		content: function() {
			return $('#popoverExampleTwoHiddenContent').html();
		},
		title: function() {
			return $('#popoverExampleTwoHiddenTitle').html();
		}
	}
);
*/



